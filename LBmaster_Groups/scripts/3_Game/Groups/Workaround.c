class LB_GUI_Workaround {

	static bool CreateWidgetSafe(string layout, Widget parent, string ident, out Widget wid, int maxTries = 50) {
		string layoutCreate = "" + layout.Substring(0, layout.Length());
		Widget empty = GetGame().GetWorkspace().CreateWidgets("LBmaster_Groups/gui/layouts/empty.layout");
		if (empty) {
			Widget child2 = empty.GetChildren();
			while (child2 != null) {
				Print("Child Empty: " + child2.GetName());
				empty.RemoveChild(child2);
				child2 = empty.GetChildren();
			}
			empty.Unlink();
		}
		for (int t = 0; t < maxTries; t++) {
			wid = GetGame().GetWorkspace().CreateWidgets(layoutCreate, null);
			Print("Widget for " + layoutCreate + ": " + wid);
			if (!wid) {
				Print("Workaround could not create Layout " + layoutCreate + " T:" + t);
				continue;
			}
			Widget check = wid.FindAnyWidget("ident_" + ident);
			if (check) {
				Print("Workaround worked for " + layoutCreate + " With " + (t + 1) + " tries");
				if (parent)
					parent.AddChild(wid);
				return true;
			}
			Widget child = wid.GetChildren();
			while (child != null) {
				Print("Child: " + child.GetName());
				wid.RemoveChild(child);
				child = wid.GetChildren();
			}
			wid.Unlink();
			Print("Next Workaround try for " + layoutCreate + " T:" + t);
		}
		Print("Workaround Failed for " + layoutCreate + " Ident: ident_" + ident);
		return false;
	}

}