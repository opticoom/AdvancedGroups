class LB_ATM_Constants {

	static const string MAIN_FOLDER = "$profile:LBGroup/";
	static const string MAIN_PLAYER_FOLDER = "$profile:LBGroup/ATMPlayers/";
	
	static const string ATM_MESSAGE_ICON = "LBmaster_Groups_ATM/gui/icons/Ruble.edds";
	static const string ATM_POSITIONS_FILE = "ATMPositions.json";
	static const string ATM_ADMINS_FILE = "ATMAdmins.json";
}