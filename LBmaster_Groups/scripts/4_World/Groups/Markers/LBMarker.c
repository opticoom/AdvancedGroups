class LBMarker {
	
	static ref array<LBMarker> allMarkers = new array<LBMarker>();
	static bool hideAllMarkers = false;

	LBMarkerType type;
	int uid;
	string name;
	string icon;
	vector position = vector.Zero;
	int currentSubgroup;
	int colorA = 255, colorR = 255, colorG = 255, colorB = 255;
	
	[NonSerialized()]
	bool visibleOnScreen = false;
	[NonSerialized()]
	Widget mainWidget;
	[NonSerialized()]
	Widget bottomWidget;
	[NonSerialized()]
	ImageWidget iconWidget;
	[NonSerialized()]
	TextWidget nameWidget;
	[NonSerialized()]
	TextWidget distanceWidget;
	[NonSerialized()]
	LBGroup parentGroup;
	[NonSerialized()]
	ref MarkerConfigEntry cachedMarkerConfig = null;
	[NonSerialized()]
	float dist;
	[NonSerialized()]
	bool show = true;
	[NonSerialized()]
	bool disable3dDifferentSubgroup = false;
	[NonSerialized()]
	int state = 0;
	[NonSerialized()]
	int lastcolor = 0;
	
	[NonSerialized()]
	Widget compassWidget;
	[NonSerialized()]
	ImageWidget compassIconWidget;
	[NonSerialized()]
	TextWidget compassNameWidget;
	
	static bool compassInit = false;
	static float currentCameraAngle = 0;
	static Widget compassWidgetGlobal;
	
	static void InitCompassWidgets() {
		compassInit = true;
		foreach (LBMarker marker : allMarkers) {
			if (marker) {
				marker.InitCompassWidget();
			}
		}
	}
	
	static void UpdateAllMarkers() {
		foreach (LBMarker marker : allMarkers) {
			if (marker) {
				if (!hideAllMarkers && marker.UpdateMarkerClient())
					marker.SetWidgetPosition();
				else {
					marker.SetVisibleOnScreen(false);
					if (marker.compassWidget)
						marker.compassWidget.Show(false);
				}
			}
		}
	}
	
	static void UpdateAllMarkersSlow() {
		foreach (LBMarker marker : allMarkers) {
			if (marker) {
				marker.UpdateMarkerSlow();
			}
		}
	}
	
	void SetupMarker(LBMarkerType type, string name, string icon, vector pos) {
		this.type = type;
		this.name = name;
		this.icon = icon;
		this.position = pos;
		this.uid = Math.RandomInt(200, int.MAX - 1);
	}
	
	void InitMarker() {
		if (GetMarkerConfig() && !GetMarkerConfig().display3d)
			return;
		if (mainWidget || GetGame().IsServer())
			return;
		mainWidget = GetGame().GetWorkspace().CreateWidgets("LBmaster_Groups/gui/layouts/3dmarker.layout", null);
		if (mainWidget) {
			iconWidget = ImageWidget.Cast(mainWidget.FindAnyWidget("icon"));
			nameWidget = TextWidget.Cast(mainWidget.FindAnyWidget("name"));
			distanceWidget = TextWidget.Cast(mainWidget.FindAnyWidget("distance"));
			bottomWidget = mainWidget.FindAnyWidget("bottom");
			
			if (GetIcon().Length() > 0) {
				Print("Loading Image: " + GetIcon());
				iconWidget.LoadImageFile(0, GetIcon());
			} else {
				iconWidget.Show(false);
			}
			if (type != LBMarkerType.GROUP_PING) {
				nameWidget.SetText(name);
			} else {
				nameWidget.SetText("");
				nameWidget.Show(false);
			}
			mainWidget.Update();
			UpdateDistance();
			
			SetColor(true);
		}
		SetVisibleOnScreen(false);
		string printname = name + "";
		printname.Replace("%", "");
		Print("Init Marker " + printname + ". Created Layout: " + (mainWidget != null));
		UpdateMarkerSlow();
		if (LBGroupMainConfig.Get().enableCompassHud)
			InitCompassWidget();
	}
	
	void InitCompassWidget() {
		if (GetMarkerConfig() && !GetMarkerConfig().displayCompass)
			return;
		if (compassWidget || GetGame().IsServer())
			return;
		if (!compassInit)
			return;
		compassWidget = GetGame().GetWorkspace().CreateWidgets(GetCompassMarkerLayout(), compassWidgetGlobal);
		if (compassWidget) {
			compassIconWidget = ImageWidget.Cast(compassWidget.FindAnyWidget("icon"));
			compassNameWidget = TextWidget.Cast(compassWidget.FindAnyWidget("name"));
			if (GetIcon().Length() > 0) {
				Print("Loading Image: " + GetIcon());
				compassIconWidget.LoadImageFile(0, GetIcon());
			} else {
				compassIconWidget.Show(false);
			}
			compassNameWidget.SetText(name);
			compassWidget.Show(false);
			SetColor(true);
		}
	
	}
	
	string GetIcon() {
		if (type == LBMarkerType.GROUP_PING)
			return LBMarkerVisibilityManager.Get().GetPingMarkerIcon();
		return icon;
	}
	
	string GetCompassMarkerLayout() {
		return LBLayoutConfig.Get().GetCurrentLayout("Compass Marker");
	}
	
	void UpdateMarkerSlow() {
		SetColor();
		if (LBGroupMainConfig.Get().enableSubGroups && (type == LBMarkerType.GROUP_PING || type == LBMarkerType.GROUP_PLAYER_MARKER)) {
			PlayerBase pb = PlayerBase.Cast(GetGame().GetPlayer());
			if (!pb)
				return;
			int mySubgroup = pb.GetMySubGroup();
			disable3dDifferentSubgroup = mySubgroup != currentSubgroup;
			if (disable3dDifferentSubgroup)
				return;
		}
		disable3dDifferentSubgroup = false;
		show = ShowMarker();
		//Print("Show Marker: " + name + ": " + show);
	}
	
	void OnMarkerRPCClient(int type, ParamsReadContext ctx) {
		if (type == LBGroupRPCs.POSITION) {
			float x,y,z;
			if (!ctx.Read(x) || !ctx.Read(y) || !ctx.Read(z))
				return;
			vector vec = Vector(x,y,z);
			SetPosition(vec);
		} else if (type == LBGroupRPCs.SUBGRUOP) {
			int grp = 0;
			if (!ctx.Read(grp))
				return;
			SetSubGroup(grp);
		} else if (type == LBGroupRPCs.NAME) {
			string name;
			if (!ctx.Read(name))
				return;
			SetName(name);
		}
	}
	
	void AddToAllList() {
		allMarkers.Insert(this);
	}
	
	void LBMarker() {
		AddToAllList();
	}
	
	void SetSubGroup(int grp) {
		currentSubgroup = grp;
	}
	
	void SetPosition(vector pos) {
		position = pos;
	}
	
	void SendPositionToServer() {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.POSITION);
		rpc.Write(position[0]);
		rpc.Write(position[1]);
		rpc.Write(position[2]);
		SendMarkerRPC(rpc);
	}
	
	void SetName(string name) {
		this.name = name;
	}
	
	void ~LBMarker() {
		Print("Removed Marker: " + name);
		allMarkers.RemoveItem(this);
		if (mainWidget) {
			mainWidget.Unlink();
			mainWidget = null;
		}
		if (compassWidget) {
			compassWidget.Unlink();
			compassWidget = null;
		}
	}
	
	void SetColor(bool force = false) {
		int rgb = Get3DColorARGB();
		if (force || lastcolor != rgb) {
			if (iconWidget)
				iconWidget.SetColor(rgb);
			if (nameWidget)
				nameWidget.SetColor(rgb);
			if (compassNameWidget && compassIconWidget) {
				compassNameWidget.SetColor(rgb);
				compassIconWidget.SetColor(rgb);
			}
			lastcolor = rgb;
		}
	}
	
	MarkerConfigEntry GetMarkerConfig() {
		if (cachedMarkerConfig)
			return cachedMarkerConfig;
		cachedMarkerConfig = LBGroupMainConfig.Get().GetMarkerConfigEntry(type);
		return cachedMarkerConfig;
	}
	
	bool ShowDistance() {
		if (!GetMarkerConfig())
			return true;
		return GetMarkerConfig().displayDistance;
	}
	
	bool ShouldCenterWidget() {
		return type == LBMarkerType.GROUP_PING;
	}
	
	float GetCompassPosY() {
		if (type != LBMarkerType.GROUP_PING)
			return 0;
		return 0.5;
	}
	
	void SetDistance() {
		if (!GetGame() || !GetGame().GetPlayer() || !ShowDistance())
			return;
		vector pos = GetGame().GetCurrentCameraPosition();
		dist = vector.Distance(position, pos);
		if (dist < 1000) {
			distanceWidget.SetText("" + ((int) dist) + "m");
		} else {
			float km = ((float) ((int) (dist / 100))) / 10;
			distanceWidget.SetText("" + km + "km");
		}
	}
	
	bool ShowMarker() {
		MarkerConfigEntry cfg = GetMarkerConfig();
	//	Print("MarkerEntry: " + cfg);
		if (!cfg)
			return false;
		vector pos = GetGame().GetCurrentCameraPosition();
		dist = vector.Distance(position, pos);
	//	Print("Dist: " + dist + " from: " + pos + " To: " + position);
		if (GetMarkerConfig().maxDistance < 0 || GetMarkerConfig().maxDistance > dist) {
			return LBMarkerVisibilityManager.Get().Is3DVisiblie(uid, type);
		}
		return false;
	}
	
	void UpdateDistance() {
		if (ShowDistance()) {
			SetDistance();
			bottomWidget.Show(true);
		} else {
			bottomWidget.Show(false);
		}
	}
	
	int GetColorARGB() {
		if (type == LBMarkerType.GROUP_PING) {
			return LBColorManager.Get().GetColor("Ping 3D Marker");
		}
		return ARGB(colorA, colorR, colorG, colorB);
	}
	
	int Get3DColorARGB() {
		return GetColorARGB();
	}
	
	bool UpdateMarkerClient() {
		if (!mainWidget || !show || !GetGame() || !GetGame().GetPlayer() || !GetGame().GetPlayer().IsAlive() || GetGame().GetPlayer().IsUnconscious())
			return false;
		UpdateDistance();
		return true;
	}
	
	bool SetWidgetPosition() {
		if (!mainWidget || !position)
			return false;
		if (compassWidget) {
			float angle = GetMarkerAngle();
			float posX = (angle / 180.0) - 0.5;
			if (posX < -1)
				posX += 2;
			compassWidget.SetPos(posX, GetCompassPosY());
		}
		vector screenPos = GetGame().GetScreenPos(position);
		int screenWidth, screenHeight;
		GetScreenSize(screenWidth,screenHeight);
		if (screenPos[0] <= 0 || screenPos[0] >= screenWidth || screenPos[1] <= 0 || screenPos[1] >= screenHeight || screenPos[2] <= 0) {
			SetVisibleOnScreen(false);
			return false;
		}
		if (ShouldCenterWidget()) {
			float width, height;
			mainWidget.GetScreenSize(width, height);
			screenPos[0] = screenPos[0] - width / 2;
			screenPos[1] = screenPos[1] - height / 2;
		}
		SetVisibleOnScreen(true);
		mainWidget.SetPos(screenPos[0], screenPos[1]);
		return true;
	}
	
	float GetMarkerAngle() {
		vector camPos = GetGame().GetCurrentCameraPosition();
		vector dir = camPos - position;
		dir = dir.Normalized();
		vector angles = dir.VectorToAngles();
		float angle = angles[0] - currentCameraAngle + 360;
		while (angle > 180)
			angle -= 360;
		return angle;
	}
	
	void SetVisibleOnScreen(bool b) {
		visibleOnScreen = b;
		if (mainWidget)
			mainWidget.Show(b && !disable3dDifferentSubgroup && show);
		if (compassWidget) {
			compassWidget.Show(!disable3dDifferentSubgroup && show);
		}
	}
	
	bool ReadFromCtx(ParamsReadContext ctx) {
		if (!ctx.Read(type))
			return false;
		if (!ctx.Read(uid))
			return false;
		if (!ctx.Read(name))
			return false;
		if (!ctx.Read(icon))
			return false;
		if (!ctx.Read(position))
			return false;
		if (!ctx.Read(colorA))
			return false;
		if (!ctx.Read(colorR))
			return false;
		if (!ctx.Read(colorG))
			return false;
		if (!ctx.Read(colorB))
			return false;
		if (!ctx.Read(currentSubgroup))
			return false;
		return true;
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(type);
		ctx.Write(uid);
		ctx.Write(name);
		ctx.Write(icon);
		ctx.Write(position);
		ctx.Write(colorA);
		ctx.Write(colorR);
		ctx.Write(colorG);
		ctx.Write(colorB);
		ctx.Write(currentSubgroup);
	}
	
	ScriptRPC CreateRPCCall(int type) {
		ScriptRPC rpc = new ScriptRPC();
		rpc.Write(type);
		rpc.Write(uid);
		return rpc;
	}
	
	void SendMarkerRPC(ScriptRPC rpc) {
		if (!parentGroup && GetGame().IsClient() && GetGame().IsMultiplayer()) {
			PlayerBase pb = PlayerBase.Cast(GetGame().GetPlayer());
			parentGroup = pb.GetLBGroup();
		}
		if (parentGroup) {
			if (GetGame().IsServer()) {
				parentGroup.SendRPCToGroupMembers(rpc);
			} else {
				parentGroup.SendRPCToServer(rpc);
			}
		} else if (type == LBMarkerType.SERVER_STATIC || type == LBMarkerType.SERVER_DYNAMIC) {
			rpc.Send(null, LBGroupRPCs.MARKER_RPC, true);
		} else {
			if (!parentGroup)
				Print("Failed to send Marker RPC for Marker: " + type + " No Parent Group ! ");
			else
				Print("Failed to send Marker RPC for Marker: " + type + " Parent Group: " + parentGroup.shortname);
		}
	}
	
}