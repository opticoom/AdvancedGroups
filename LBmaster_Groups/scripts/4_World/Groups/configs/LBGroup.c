class LBGroup {

	static int CONFIG_VERSION = 1;
	int configVersion = 0;
	string name, shortname;
	int level;
	int maxPlayers;
	int subGroupSize;
	int subGroupCount;
	int lastActivity = -1;
	int creationDate = -1;
	int markerLimit = 0;
	int plotpoleLimit = 0;
	bool hasATMAccount = false;
	bool showTagInChat = true;
	int ATMBalance = 0;
	ref array<ref LBGroupMember> members = new array<ref LBGroupMember>();
	ref array<ref LBMarker> markers = new array<ref LBMarker>();
	[NonSerialized()]
	ref array<ref LBMarker> pings = new array<ref LBMarker>();
	
	void OnLoadServer() {
		if (configVersion != CONFIG_VERSION) {
			if (configVersion < 1) {
				showTagInChat = true;
			}
		}
		configVersion = CONFIG_VERSION;
	}
	
	void ~LBGroup() {
		if (!GetGame() || GetGame().IsServer())
			return;
		while (members.Get(0)) {
			delete members.Get(0);
			members.Remove(0);
		}
		while (markers.Get(0)) {
			delete markers.Get(0);
			markers.Remove(0);
		}
		while (pings.Get(0)) {
			delete pings.Get(0);
			pings.Remove(0);
		}
	}
	
	void InitMarkers() {
		foreach (LBMarker marker : markers) {
			if (marker)
				marker.InitMarker();
		}
		foreach (LBGroupMember member : members) {
			if (member)
				member.InitMarker();
		}
		foreach (LBMarker ping : pings) {
			if (ping)
				ping.InitMarker();
		}
	}
	
	void OnRPCClient(int type, ParamsReadContext ctx) {
		if (type == LBGroupRPCs.ADD) {
			LBMarker marker = new LBMarker();
			if (!marker.ReadFromCtx(ctx)) {
				Print("Failed to receive new Marker from Server !");
				return;
			}
			AddMarkerLocal(marker);
		} else if (type == LBGroupRPCs.REMOVE) {
			int uid;
			if (!ctx.Read(uid)) {
				Print("Failed to receive new Marker ID from Server !");
				return;
			}
			LBMarker mark = FindAnyMarkerByUID(uid);
			if (mark) {
				RemoveMarkerLocal(mark);
			}
		} else if (type == LBGroupRPCs.CHANGE_TAG_VISIBILITY) {
			bool enabled;
			if (!ctx.Read(enabled)) {
				Print("Failed to receive Tag Visibility from Server !");
				return;
			}
			showTagInChat = enabled;
		} else if (type == LBGroupRPCs.ADD_CLIENT) {
			LBGroupMember member = new LBGroupMember();
			if (!member.ReadFromCtx(ctx)) {
				Print("Failed to receive new Member from Server !");
				return;
			}
			AddMarkerLocal(member);
		} else if (type == LBGroupRPCs.UPGRADE) {
			int level1, maxPlayers1, subGroupCount1, subGroupSize1, maxMarkers1, plotpoleLimit1;
			if (!ctx.Read(level1) || !ctx.Read(maxPlayers1) || !ctx.Read(subGroupCount1) || !ctx.Read(subGroupSize1) || !ctx.Read(maxMarkers1) || !ctx.Read(plotpoleLimit1)) {
				Print("Failed to Read Upgrade Info");
				return;
			}
			level = level1;
			maxPlayers = maxPlayers1;
			subGroupCount = subGroupCount1;
			subGroupSize = subGroupSize1;
			markerLimit = maxMarkers1;
			plotpoleLimit = plotpoleLimit1;
			MissionBaseWorld mission = MissionBaseWorld.Cast(GetGame().GetMission());
			if (mission)
				mission.OnGroupChanged();
		} else if (type == LBGroupRPCs.ATM_ACCOUNT_CHANGED) {
			bool hasAcc;
			if (!ctx.Read(hasAcc)) {
				Print("Failed to get HasGroupATMAccount info !");
				return;
			}
			Print("Successfully read HasGroupATMAccount. Now: " + hasAcc);
			this.hasATMAccount = hasAcc;
			mission = MissionBaseWorld.Cast(GetGame().GetMission());
			if (mission)
				mission.OnGroupChanged();
		} else if (type == LBGroupRPCs.ATM_BALANCE_CHANGED) {
			int money;
			if (!ctx.Read(money)) {
				Print("Failed to get money info !");
				return;
			}
			Print("Successfully read money. Now: " + money);
			this.ATMBalance = money;
			mission = MissionBaseWorld.Cast(GetGame().GetMission());
			if (mission)
				mission.OnGroupChanged();
		} else if (type > LBGroupRPCs.START_MARKER_RPC) {
			int uid2;
			if (!ctx.Read(uid2))
				return;
			LBMarker marker2 = FindAnyMarkerByUID(uid2);
			if (marker2)
				marker2.OnMarkerRPCClient(type, ctx);
		}
	}
	
	void KickPlayerClient(string steamid) {
		PromoteDemoteKickHelpter(steamid, LBGroupRPCs.KICK);
	}
	
	void DemotePlayerClient(string steamid) {
		PromoteDemoteKickHelpter(steamid, LBGroupRPCs.DEMOTE);
	}
	
	void PromotePlayerClient(string steamid) {
		PromoteDemoteKickHelpter(steamid, LBGroupRPCs.PROMOTE);
	}
	
	void PromoteDemoteKickHelpter(string steamid, int type) {
		ScriptRPC rpc = CreateRPCCall(type);
		rpc.Write(steamid);
		SendRPCToServer(rpc);
	}
	
	void LeaveGroupClient() {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.LEAVE);
		SendRPCToServer(rpc);
	}
	
	void UpgradeGroupClient() {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.UPGRADE);
		SendRPCToServer(rpc);
	}
	
	LBGroupMember GetMemberBySteamid(string steamid) {
		foreach (LBGroupMember member : members) {
			if (member.steamid == steamid)
				return member;
		}
		return null;
	}
	
	bool IsMember(string steamid) {
		return GetMemberBySteamid(steamid) != null;
	}
	
	void RemoveMember(LBGroupMember member) {
		int uidd = member.uid;
		for (int i = 0; i < members.Count(); i++) {
			LBGroupMember mem = members.Get(i);
			if (!mem || mem.uid == uidd) {
				members.Remove(i);
				i--;
				if (mem)
					delete mem;
			}
		}
	}
	
	int GetSubgroupMemberCount(int subGroup) {
		return GetSubgroupMembers(subGroup).Count();
	}
	
	void JoinSubgroupRequest(int grp) {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.JOIN_SUBGROUP);
		rpc.Write(grp);
		SendRPCToServer(rpc);
	}
	
	array<ref LBGroupMember> GetSubgroupMembers(int subGroup) {
		if (!LBGroupMainConfig.Get().enableSubGroups)
			return members;
		array<ref LBGroupMember> memb = new array<ref LBGroupMember>();
		foreach (LBGroupMember member : members) {
			if (member.currentSubgroup == subGroup)
				memb.Insert(member);
		}
		return memb;
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(name);
		ctx.Write(shortname);
		ctx.Write(members.Count());
		foreach (LBGroupMember member : members) {
			member.WriteToCtx(ctx);
		}
		ctx.Write(markers.Count());
		foreach (LBMarker marker : markers) {
			marker.WriteToCtx(ctx);
		}
		ctx.Write(level);
		ctx.Write(maxPlayers);
		ctx.Write(subGroupSize);
		ctx.Write(subGroupCount);
		ctx.Write(markerLimit);
		ctx.Write(plotpoleLimit);
		ctx.Write(showTagInChat);
		ctx.Write(creationDate);
		ctx.Write(lastActivity);
		ctx.Write(hasATMAccount);
		ctx.Write(ATMBalance);
	}
	
	bool ReadFromCtx(ParamsReadContext ctx) {
		if (!ctx.Read(name))
			return false;
		if (!ctx.Read(shortname))
			return false;
		int count = 0;
		if (!ctx.Read(count))
			return false;
		for (int i = 0; i < count; i++) {
			LBGroupMember member = new LBGroupMember();
			if (!member.ReadFromCtx(ctx))
				return false;
			members.Insert(member);
		}
		count = 0;
		if (!ctx.Read(count))
			return false;
		for (i = 0; i < count; i++) {
			LBMarker marker = new LBMarker();
			if (!marker.ReadFromCtx(ctx))
				return false;
			markers.Insert(marker);
		}
		if (!ctx.Read(level))
			return false;
		if (!ctx.Read(maxPlayers))
			return false;
		if (!ctx.Read(subGroupSize))
			return false;
		if (!ctx.Read(subGroupCount))
			return false;
		if (!ctx.Read(markerLimit))
			return false;
		if (!ctx.Read(plotpoleLimit))
			return false;
		if (!ctx.Read(showTagInChat))
			return false;
		if (!ctx.Read(creationDate))
			return false;
		if (!ctx.Read(lastActivity))
			return false;
		if (!ctx.Read(hasATMAccount))
			return false;
		if (!ctx.Read(ATMBalance))
			return false;
		return true;
	}
	
	void AddMarker(LBMarker marker) {
		if (markers.Count() >= markerLimit && marker.type != LBMarkerType.GROUP_PING) {
			SendErrorNotificationLOCAL("#lb_message_markerLimitReached");
			return;
		}
		Print("Sending Add Marker request to Server ...");
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.ADD);
		marker.WriteToCtx(rpc);
		SendRPCToServer(rpc);
	}
	
	void SendErrorNotificationLOCAL(string message, float show_time = 4) {
		NotificationSystem.AddNotificationExtended(show_time, "#lb_message_groupSystem", message, "set:ccgui_enforce image:MapDestroyed");
	}
	
	void SendInfoNotificationLOCAL(string message, float show_time = 4) {
		NotificationSystem.AddNotificationExtended(show_time, "#lb_message_groupSystem", message, "set:ccgui_enforce image:HudUserMarker");
	}
	
	void RemoveMarker(LBMarker marker) {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.REMOVE);
		rpc.Write(marker.uid);
		SendRPCToServer(rpc);
	}
	
	void SendRPCToServer(ScriptRPC rpc) {
		PlayerBase pb = PlayerBase.Cast(GetGame().GetPlayer());
		if (!pb)
			return;
		rpc.Send(pb, LBGroupRPCs.GROUP_RPC, true);
	}
	
	ScriptRPC CreateRPCCall(int type) {
		ScriptRPC rpc = new ScriptRPC();
		rpc.Write(type);
		return rpc;
	}
	
	void SendChatTagVisibilityRequest(bool visible) {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.CHANGE_TAG_VISIBILITY);
		rpc.Write(visible);
		SendRPCToServer(rpc);
	}
	
	void SendPlayerInviteClient(string steamid) {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.INVITE);
		rpc.Write(steamid);
		SendRPCToServer(rpc);
	}
	
	void AddMarkerLocal(LBMarker marker) {
		if (marker.type == LBMarkerType.GROUP_PING) {
			pings.Insert(marker);
			marker.InitMarker();
		} else if (marker.type == LBMarkerType.GROUP_MARKER) {
			markers.Insert(marker);
			marker.InitMarker();
		} else if (marker.type == LBMarkerType.GROUP_PLAYER_MARKER) {
			LBGroupMember member = LBGroupMember.Cast(marker);
			if (!member)
				return;
			members.Insert(member);
			member.parentGroup = this;
			member.InitMarker();
		} else {
			Print("Trying to add Marker to Group which is no Group Marker type. Type Got: " + marker.type);
		}
		
	}
	
	void RemoveMarkerLocal(LBMarker marker) {
		if (marker.type == LBMarkerType.GROUP_PING) {
			for (int i = 0; i < pings.Count(); i++) {
				LBMarker mark = pings.Get(i);
				if (mark && mark.uid == marker.uid) {
					pings.Remove(i);
					i--;
					delete mark;
					if (!marker)
						break;
				}
			}
		} else if (marker.type == LBMarkerType.GROUP_MARKER) {
			for (i = 0; i < markers.Count(); i++) {
				mark = markers.Get(i);
				if (mark && mark.uid == marker.uid) {
					markers.Remove(i);
					i--;
					delete mark;
					if (!marker)
						break;
				}
			}
		} else if (marker.type == LBMarkerType.GROUP_PLAYER_MARKER) {
			LBGroupMember member = LBGroupMember.Cast(marker);
			if (!member)
				return;
			RemoveMember(member);
		} else {
			Print("Trying to remove Marker form Group which is no Group Marker type. Type Got: " + marker.type);
		}
	}
	
	LBMarker FindMarkerByUID(int uid) {
		foreach (LBMarker marker : markers) {
			if (marker.uid == uid)
				return marker;
		}
		return null;
	}
	
	LBMarker FindPingMarkerByUID(int uid) {
		foreach (LBMarker marker : pings) {
			if (marker.uid == uid)
				return marker;
		}
		return null;
	}
	
	LBGroupMember FindMemberByUID(int uid) {
		foreach (LBGroupMember member : members) {
			if (member.uid == uid)
				return member;
		}
		return null;
	}
	
	LBMarker FindAnyMarkerByUID(int uid) {
		Print("Finding any Marker with UID: " + uid + " Markers: " + markers.Count() + " Members: " + members.Count() + " Pings: " + pings.Count());
		LBMarker marker = FindMarkerByUID(uid);
		if (!marker)
			marker = FindMemberByUID(uid);
		if (!marker)
			marker = FindPingMarkerByUID(uid);
		Print("Found Marker: " + marker);
		return marker;
	}
	
	bool FindNearestMarker(vector position, out LBMarker markero, out float distance) {
		if (markers.Count() == 0)
			return false;
		float bestDist = 0;
		LBMarker bestMarker = null;
		foreach (LBMarker marker : markers) {
			if (!bestMarker || vector.Distance(position, marker.position) < bestDist) {
				bestMarker = marker;
				bestDist = vector.Distance(position, marker.position) < bestDist;
			}
		}
		if (bestMarker) {
			markero = bestMarker;
			distance = bestDist;
			return true;
		}
		return false;
	}
	
	void SendATMBalanceChanged() {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.ATM_BALANCE_CHANGED);
		rpc.Write(this.ATMBalance);
		SendRPCToGroupMembers(rpc);
	}
	
	void SendATMAccountChanged() {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.ATM_ACCOUNT_CHANGED);
		rpc.Write(this.hasATMAccount);
		SendRPCToGroupMembers(rpc);
	}
	
	void SendRPCToGroupMembers(ScriptRPC rpc, int otherRPCType = -1) {}
	void InitNumbers() {}
}