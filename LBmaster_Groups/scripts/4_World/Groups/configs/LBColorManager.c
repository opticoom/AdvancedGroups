class LBColorManager {

	static ref LBColorManager g_LBColorManager;
	
	ref array<ref Param2<string, int>> colors = new array<ref Param2<string, int>>();
	string changedColor = "";
	
	static ref ScriptInvoker Event_OnColorChange = new ScriptInvoker();
	
	static LBColorManager Get() {
		if (!g_LBColorManager) {
			g_LBColorManager = Load();
		}
		return g_LBColorManager;
	}
	
	static LBColorManager Load() {
		if (!FileExist(LBGroupConstants.SAVE_PREFIX))
			MakeDirectory(LBGroupConstants.SAVE_PREFIX);
		LBColorManager mgr = new LBColorManager();
		ref array<ref Param2<string, int>> colorss = new array<ref Param2<string, int>>();
		if (FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_COLOR_MANAGER)) {
			JsonFileLoader<array<ref Param2<string, int>>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_COLOR_MANAGER, colorss);
		}
		mgr.SetDefaultColors();
		mgr.ReplaceColors(colorss);
		mgr.Save();
		return mgr;
	}
	
	static void Reload() {
		g_LBColorManager = Load();
		InvokeOnChanged();
	}
	
	void Save() {
		ref array<ref Param2<string, int>> colorssave = new array<ref Param2<string, int>>();
		ref array<ref Param2<string, int>> colorss = new array<ref Param2<string, int>>();
		if (FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_COLOR_MANAGER)) {
			JsonFileLoader<array<ref Param2<string, int>>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_COLOR_MANAGER, colorss);
		}
		// Add all Current Colors to the Array
		foreach (Param2<string, int> color : colors) {
			colorssave.Insert(color);
		}
		// Go through all Colors that were saved in the config to not delete entries from other servers.
		foreach (Param2<string, int> oldColor : colorss) {
			bool found = false;
			// Check if the Color is already present and ignore them
			foreach (Param2<string, int> setColor : colorssave) {
				if (oldColor.param1 == setColor.param1) {
					found = true;
					break;
				}
			}
			// if color was not found, add it to the list
			if (!found)
				colorssave.Insert(oldColor);
		}
		JsonFileLoader<array<ref Param2<string, int>>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_COLOR_MANAGER, colorssave);
	}
	
	private void ReplaceColors(array<ref Param2<string, int>> colors) {
		foreach (Param2<string, int> color : colors) {
			SetColor(color.param1, color.param2, false);
		}
	}
	
	void ResetAll() {
		colors.Clear();
		SetDefaultColors();
		InvokeOnChanged();
	}
	
	static void InvokeOnChanged() {
		Event_OnColorChange.Invoke();
	}
	
	int GetColor(string colorStr, int defaultColor = -1) {
		if (changedColor == colorStr) {
			SetColor(colorStr, defaultColor);
			changedColor = "";
		}
		foreach (Param2<string, int> color : colors) {
			if (color && color.param1 == colorStr)
				return color.param2;
		}
		colors.Insert(new Param2<string, int>(colorStr, defaultColor));
		return defaultColor;
	}
	
	void SetColor(string colorStr, int colorARGB, bool add = true) {
		foreach (Param2<string, int> color : colors) {
			if (color && color.param1 == colorStr) {
				color.param2 = colorARGB;
				return;
			}
		}
		if (add)
			colors.Insert(new Param2<string, int>(colorStr, colorARGB));
	}
	
	void SetDefaultColors() {
		GetColor("Player 3D Marker", ARGB(255, 255, 255, 255));
		GetColor("Own Player Map Marker", ARGB(255, 255, 51, 51));
		GetColor("Player Online", ARGB(255, 0, 255, 0));
		GetColor("Player Offline", ARGB(255, 255, 0, 0));
		GetColor("Ping 3D Marker", ARGB(255, 255, 255, 0));
		GetColor("Compass", ARGB(255, 255, 255, 255));
		GetColor("Compass Line", ARGB(255, 255, 0, 0));
		GetColor("Playerlist entry full health", ARGB(255, 255, 255, 255));
		GetColor("Playerlist entry zero health", ARGB(255, 255, 0, 0));
		GetColor("Playerlist entry border", ARGB(255, 255, 255, 255));
	}
	
	void ResetColorToDefault(string colorStr) {
		changedColor = colorStr;
		SetDefaultColors();
	}
	
	TStringArray GetColorStrings() {
		ref TStringArray arr = new TStringArray();
		foreach (Param2<string, int> color : colors) {
			arr.Insert(color.param1);
		}
		return arr;
	}

}