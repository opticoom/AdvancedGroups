typedef Param3<string, ref vector, int> LBWidgetPosition;

class LBPositionManager {

	static ref LBPositionManager g_LBPositionManager;
	
	ref array<ref LBWidgetPosition> positions = new array<ref LBWidgetPosition>();
	string changedPosition = "";
	
	static ref ScriptInvoker Event_OnPositionChange = new ScriptInvoker();
	
	static LBPositionManager Get() {
		if (!g_LBPositionManager) {
			g_LBPositionManager = Load();
		}
		return g_LBPositionManager;
	}
	
	static LBPositionManager Load() {
		if (!FileExist(LBGroupConstants.SAVE_PREFIX))
			MakeDirectory(LBGroupConstants.SAVE_PREFIX);
		LBPositionManager mgr = new LBPositionManager();
		ref array<ref LBWidgetPosition> positionss = new array<ref LBWidgetPosition>();
		if (FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_POSITION_MANAGER)) {
			JsonFileLoader<array<ref LBWidgetPosition>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_POSITION_MANAGER, positionss);
		}
		Print("Loaded Positions: " + positionss.Count());
		mgr.SetDefaultPositions();
		mgr.ReplacePositions(positionss);
		mgr.Save();
		return mgr;
	}
	
	static void Reload() {
		g_LBPositionManager = Load();
		InvokeOnChanged();
	}
	
	void Save() {
		ref array<ref LBWidgetPosition> positionssave = new array<ref LBWidgetPosition>();
		ref array<ref LBWidgetPosition> positionss = new array<ref LBWidgetPosition>();
		if (FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_POSITION_MANAGER)) {
			JsonFileLoader<array<ref LBWidgetPosition>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_POSITION_MANAGER, positionss);
		}
		// Add all Current Position to the Array
		foreach (LBWidgetPosition pos : positions) {
			if (pos)
				positionssave.Insert(pos);
		}
		// Go through all Positions that were saved in the config to not delete entries from other servers.
		foreach (LBWidgetPosition oldPos : positionss) {
			bool found = false;
			// Check if the Position is already present and ignore them
			foreach (LBWidgetPosition setPos : positionssave) {
				if (oldPos.param1 == setPos.param1) {
					found = true;
					break;
				}
			}
			// if pos was not found, add it to the list
			if (!found && oldPos)
				positionssave.Insert(oldPos);
		}
		JsonFileLoader<array<ref LBWidgetPosition>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_POSITION_MANAGER, positionssave);
	}
	
	private void ReplacePositions(array<ref LBWidgetPosition> positionss) {
		foreach (LBWidgetPosition pos : positionss) {
			SetPosition(pos.param1, pos.param2, false);
			SetIndex(pos.param1, pos.param3, false);
		}
	}
	
	void ResetAll() {
		positions.Clear();
		SetDefaultPositions();
		InvokeOnChanged();
	}
	
	static void InvokeOnChanged() {
		Event_OnPositionChange.Invoke();
	}
	
	vector GetPosition(string posStr, vector defaultPos = vector.Zero, int defaultIndex = 0, bool insert = true) {
		if (!insert)
			return vector.Zero;
		if (changedPosition == posStr) {
			SetPosition(posStr, defaultPos);
			SetIndex(posStr, defaultIndex);
			changedPosition = "";
		}
		foreach (LBWidgetPosition pos : positions) {
			if (pos && pos.param1 == posStr)
				return pos.param2;
		}
		positions.Insert(new LBWidgetPosition(posStr, defaultPos, defaultIndex));
		return defaultPos;
	}
	
	int GetIndex(string posStr) {
		foreach (LBWidgetPosition pos : positions) {
			if (pos && pos.param1 == posStr)
				return pos.param3;
		}
		return 0;
	}
	
	void SetPosition(string posStr, vector pos, bool add = true) {
		foreach (LBWidgetPosition poss : positions) {
			if (poss && poss.param1 == posStr) {
				poss.param2 = pos;
				return;
			}
		}
		if (add)
			positions.Insert(new LBWidgetPosition(posStr, pos, 0));
	}
	
	void SetIndex(string posStr, int index, bool add = true) {
		foreach (LBWidgetPosition poss : positions) {
			if (poss && poss.param1 == posStr) {
				poss.param3 = index;
				return;
			}
		}
		if (add)
			positions.Insert(new LBWidgetPosition(posStr, vector.Zero, index));
	}
	
	void SetDefaultPositions() {
		GetPosition("PlayerList", Vector(0.02, 0.02, 0), 0, LBGroupMainConfig.Get().enablePlayerList);
		#ifndef LB_DISABLE_CHAT
		GetPosition("Chat", Vector(0.05, 0.85, 0), 6);
		#endif
	}
	
	void ResetPositionToDefault(string posStr) {
		changedPosition = posStr;
		SetDefaultPositions();
	}
	
	TStringArray GetPositionStrings() {
		ref TStringArray arr = new TStringArray();
		foreach (LBWidgetPosition pos : positions) {
			arr.Insert(pos.param1);
		}
		return arr;
	}

}