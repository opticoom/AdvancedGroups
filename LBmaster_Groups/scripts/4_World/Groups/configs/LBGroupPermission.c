class LBGroupPermission {

	int UID, nextGroupUID, previousGroupUID, inheritGroupUID;
	string permName;
	ref array<ref Param2<int, bool>> markerPermissions = new array<ref Param2<int, bool>>();
	bool canUpgrade;
	bool canPromote;
	bool canDemote;
	bool canInvite;
	int promotePower;
	int demotePower;
	int promoteNeedPower;
	int demoteNeedPower;
	bool canDoBasebuilding = false;
	bool canPackPlotpole = false;
	bool canCreateSeeATMAccount = false;
	bool canCreateGroupATMAccount = false;
	bool canWithdrawGroupMoney = false;
	bool canDepositGroupMoney = false;
	bool tempGroup = false;
	
	[NonSerialized()]
	bool filledInheritedPermissions = false;
	
	
	bool CanPromote(LBGroupPermission target) {
		if (!target || target.nextGroupUID == -1)
			return false;
		LBGroupPermission next = target.GetNextGroup();
		if (!next)
			return false;
		return promotePower >= next.promoteNeedPower;
	}
	
	bool CanDemote(LBGroupPermission target) {
		if (!target || target.previousGroupUID == -1)
			return false;
		return demotePower >= target.demoteNeedPower;
	}
	
	bool CanKick(LBGroupPermission target) {
		if (!target)
			return false;
		return demotePower >= target.demoteNeedPower;
	}
	
	bool CanSeeMarkerType(LBMarkerType type) {
		foreach (Param2<int, bool> allowed : markerPermissions) {
			if (allowed.param1 == type)
				return allowed.param2;
		}
		return false;
	}
	
	void FillInherited(LBGroupPermission inherited) {
		canInvite = canInvite || inherited.canInvite;
		canUpgrade = canUpgrade || inherited.canUpgrade;
		canPromote = canPromote || inherited.canPromote;
		canDoBasebuilding = canDoBasebuilding || inherited.canDoBasebuilding;
		canPackPlotpole = canPackPlotpole || inherited.canPackPlotpole;
		canCreateGroupATMAccount = canCreateGroupATMAccount || inherited.canCreateGroupATMAccount;
		canCreateSeeATMAccount = canCreateSeeATMAccount || inherited.canCreateSeeATMAccount;
		canWithdrawGroupMoney = canWithdrawGroupMoney || inherited.canWithdrawGroupMoney;
		canDepositGroupMoney = canDepositGroupMoney || inherited.canDepositGroupMoney;
		canDemote = canDemote || inherited.canDemote;
		promotePower = Math.Max(promotePower, inherited.promotePower);
		demotePower = Math.Max(demotePower, inherited.demotePower);
		promoteNeedPower = Math.Max(promoteNeedPower, inherited.promoteNeedPower);
		demoteNeedPower = Math.Max(demoteNeedPower, inherited.demoteNeedPower);
		foreach (Param2<int, bool> othermarkerPerms : inherited.markerPermissions) {
			bool found = false;
			foreach (Param2<int, bool> mymarkerPerms : markerPermissions) {
				if (othermarkerPerms.param1 == mymarkerPerms.param1) {
					mymarkerPerms.param2 = mymarkerPerms.param2 || othermarkerPerms.param2;
					found = true;
				}
			}
			if (!found) {
				markerPermissions.Insert(new Param2<int, bool>(othermarkerPerms.param1, othermarkerPerms.param2));
			}
		}
		filledInheritedPermissions = true;
	}
	
	void PrintPermission() {
		Print(permName + " Temp ? " + tempGroup + " (" + UID + ") < " + previousGroupUID + " > " + nextGroupUID + " : " + inheritGroupUID);
		Print("canUpgrade: " + canUpgrade + " canPromote: " + canPromote + " canDemote: " + canDemote + " canInvite: " + canInvite + " canPackPlotpole: " + canPackPlotpole + " canDoBasebuilding: " + canDoBasebuilding);
		Print("canCreateSeeATMAccount: " + canCreateSeeATMAccount + " canCreateGroupATMAccount: " + canCreateGroupATMAccount + " canWithdrawGroupMoney: " + canWithdrawGroupMoney + " canDepositGroupMoney: " + canDepositGroupMoney);
		Print("promotePower: " + promotePower + " demotePower: " + demotePower + " promoteNeedPower: " + promoteNeedPower + " demoteNeedPower: " + demoteNeedPower);
		foreach (Param2<int, bool> markerPerms : markerPermissions) {
			Print("Marker Perm: " + markerPerms.param1 + " : " + markerPerms.param2);
		}
	}
	
	void FillInheritedPermissions() {
		if (filledInheritedPermissions)
			return;
		LBGroupPermission inherited = LBGroupPermissions.Get().FindPermissionGroupByUID(inheritGroupUID);
		if (inherited) {
			inherited.FillInheritedPermissions();
			FillInherited(inherited);
		}
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(UID);
		ctx.Write(nextGroupUID);
		ctx.Write(previousGroupUID);
		ctx.Write(inheritGroupUID);
		ctx.Write(permName);
		ctx.Write(markerPermissions.Count());
		foreach (Param2<int, bool> perm : markerPermissions) {
			ctx.Write(perm.param1);
			ctx.Write(perm.param2);
		}
		ctx.Write(canUpgrade);
		ctx.Write(canPromote);
		ctx.Write(canDemote);
		ctx.Write(canInvite);
		ctx.Write(canDoBasebuilding);		
		ctx.Write(canCreateSeeATMAccount);
		ctx.Write(canCreateGroupATMAccount);
		ctx.Write(canWithdrawGroupMoney);
		ctx.Write(canDepositGroupMoney);
		ctx.Write(canPackPlotpole);
		ctx.Write(promotePower);
		ctx.Write(demotePower);
		ctx.Write(promoteNeedPower);
		ctx.Write(demoteNeedPower);
	}
	
	bool ReadFromCtx(ParamsReadContext ctx) {
		if (!ctx.Read(UID))
			return false;
		if (!ctx.Read(nextGroupUID))
			return false;
		if (!ctx.Read(previousGroupUID))
			return false;
		if (!ctx.Read(inheritGroupUID))
			return false;
		if (!ctx.Read(permName))
			return false;
		int count = 0;
		if (!ctx.Read(count))
			return false;
		for (int i = 0; i < count; i++) {
			int type;
			bool canSee = false;
			if (!ctx.Read(type))
				return false;
			if (!ctx.Read(canSee))
				return false;
			markerPermissions.Insert(new Param2<int, bool>(type, canSee));
		}
		if (!ctx.Read(canUpgrade))
			return false;
		if (!ctx.Read(canPromote))
			return false;
		if (!ctx.Read(canDemote))
			return false;
		if (!ctx.Read(canInvite))
			return false;
		if (!ctx.Read(canDoBasebuilding))
			return false;
		if (!ctx.Read(canCreateSeeATMAccount))
			return false;
		if (!ctx.Read(canCreateGroupATMAccount))
			return false;
		if (!ctx.Read(canWithdrawGroupMoney))
			return false;
		if (!ctx.Read(canDepositGroupMoney))
			return false;
		if (!ctx.Read(canPackPlotpole))
			return false;
		if (!ctx.Read(promotePower))
			return false;
		if (!ctx.Read(demotePower))
			return false;
		if (!ctx.Read(promoteNeedPower))
			return false;
		if (!ctx.Read(demoteNeedPower))
			return false;
		return true;
	}
	
	LBGroupPermission GetPreviousGroup() {
		return LBGroupPermissions.Get().FindPermissionGroupByUID(previousGroupUID);
	}
	
	LBGroupPermission GetNextGroup() {
		return LBGroupPermissions.Get().FindPermissionGroupByUID(nextGroupUID);
	}
	
}

class LBGroupPermissions {

	ref array<ref LBGroupPermission> allGroups = new array<ref LBGroupPermission>();
	ref TIntArray tempGroups = new TIntArray;
	
	static ref LBGroupPermissions g_LBGroupPermissions;
	
	static void Delete() {
		if (g_LBGroupPermissions)
			delete g_LBGroupPermissions;
	}
	
	static LBGroupPermissions Get() {
		if (!g_LBGroupPermissions) {
			if (GetGame().IsServer()) {
				g_LBGroupPermissions = Load();
				g_LBGroupPermissions.LoadInheritence();
				g_LBGroupPermissions.LoadTempGroups();
				g_LBGroupPermissions.PrintAllPermissionGroups();
			} else {
				g_LBGroupPermissions = new LBGroupPermissions();
				GetGame().RPCSingleParam(null, LBGroupRPCs.CONFIG_SYNC_PERMISSIONS, new Param1<bool>(true), true);
			}
		}
		return g_LBGroupPermissions;
	}
	
	static LBGroupPermissions Load() {
		LBGroupPermissions perms;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUP_PERMISSIONS)) {
			perms = LoadDefault();
			JsonFileLoader<array<ref LBGroupPermission>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUP_PERMISSIONS, perms.allGroups);
			return perms;
		}
		perms = new LBGroupPermissions();
		JsonFileLoader<array<ref LBGroupPermission>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUP_PERMISSIONS, perms.allGroups);
		return perms;
	}
	
	static LBGroupPermissions LoadDefault() {
		LBGroupPermissions perms = new LBGroupPermissions();
		LBGroupPermission temp = new LBGroupPermission();
		temp.UID = 1;
		temp.nextGroupUID = 3;
		temp.previousGroupUID = -1;
		temp.inheritGroupUID = -1;
		temp.permName = "Temp";
		temp.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.SERVER_STATIC, true)); // 0
		temp.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.SERVER_DYNAMIC, true)); // 1
		temp.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.GROUP_PING, true)); // 2
		temp.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.GROUP_MARKER, false)); // 3
		temp.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.PRIVATE_MARKER, true)); // 4
		temp.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.GROUP_PLAYER_MARKER, true)); // 5
		temp.canPromote = false;
		temp.canDemote = false;
		temp.promotePower = 0;
		temp.demotePower = 0;
		temp.promoteNeedPower = 20;
		temp.demoteNeedPower = 20;
		temp.tempGroup = true;
		temp.canPackPlotpole = false;
		temp.canDoBasebuilding = false;
		temp.canCreateGroupATMAccount = false;
		temp.canCreateSeeATMAccount = false;
		temp.canWithdrawGroupMoney = false;
		temp.canDepositGroupMoney = false;
		perms.allGroups.Insert(temp);
		LBGroupPermission trial = new LBGroupPermission();
		trial.UID = 3;
		trial.nextGroupUID = 5;
		trial.previousGroupUID = 1;
		trial.inheritGroupUID = 1;
		trial.permName = "Trial";
		trial.tempGroup = false;
		perms.allGroups.Insert(trial);
		LBGroupPermission member = new LBGroupPermission();
		member.UID = 5;
		member.nextGroupUID = 7;
		member.previousGroupUID = 3;
		member.inheritGroupUID = 3;
		member.canCreateSeeATMAccount = true;
		member.canDepositGroupMoney = true;
		member.permName = "Member";
		member.tempGroup = false;
		member.canDoBasebuilding = true;
		member.markerPermissions.Insert(new Param2<int, bool>(LBMarkerType.GROUP_MARKER, true));
		perms.allGroups.Insert(member);
		LBGroupPermission admin = new LBGroupPermission();
		admin.UID = 7;
		admin.nextGroupUID = 9;
		admin.previousGroupUID = 5;
		admin.inheritGroupUID = 5;
		admin.permName = "Admin";
		admin.tempGroup = false;
		admin.canPromote = true;
		admin.canDemote = true;
		admin.canUpgrade = true;
		admin.canInvite = true;
		admin.canPackPlotpole = true;
		member.canCreateGroupATMAccount = true;
		member.canWithdrawGroupMoney = true;
		admin.promotePower = 30;
		admin.demotePower = 30;
		admin.promoteNeedPower = 40;
		admin.demoteNeedPower = 40;
		perms.allGroups.Insert(admin);
		LBGroupPermission leader = new LBGroupPermission();
		leader.UID = 9;
		leader.nextGroupUID = -1;
		leader.previousGroupUID = 7;
		leader.inheritGroupUID = 7;
		leader.permName = "Leader";
		leader.tempGroup = false;
		leader.promotePower = 40;
		leader.demotePower = 40;
		leader.promoteNeedPower = 50;
		leader.demoteNeedPower = 50;
		perms.allGroups.Insert(leader);
		return perms;
	}
	
	void PrintAllPermissionGroups() {
		foreach (LBGroupPermission perm : allGroups) {
			perm.PrintPermission();
		}
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(allGroups.Count());
		foreach (LBGroupPermission perm : allGroups) {
			perm.WriteToCtx(ctx);
		}
	}
	
	bool ReadFromCtx(ParamsReadContext ctx) {
		int count = 0;
		if (!ctx.Read(count))
			return false;
		allGroups.Clear();
		Print("Reading " + count + " Permission Groups");
		for (int i = 0; i < count; i++) {
			LBGroupPermission perm = new LBGroupPermission();
			if (!perm.ReadFromCtx(ctx))
				return false;
			allGroups.Insert(perm);
		}
		return true;
	}
	
	void RPC_LB(PlayerIdentity sender, ParamsReadContext ctx) {
		if (GetGame().IsServer()) {
			ScriptRPC rpc = new ScriptRPC();
			WriteToCtx(rpc);
			rpc.Send(null, LBGroupRPCs.CONFIG_SYNC_PERMISSIONS, true, sender);
		} else {
			if (!ReadFromCtx(ctx)) {
				Print("Unable to read Permissions Config from Server !");
				Print("Groups received: ");
				PrintAllPermissionGroups();
				return;
			}
			Print("Successfully Received Permissions Config from Server Entries: " + allGroups.Count());
			PrintAllPermissionGroups();
		}
	}
	
	void LoadTempGroups() {
		foreach (LBGroupPermission perm : allGroups) {
			if (perm.tempGroup)
				tempGroups.Insert(perm.UID);
		}
	}
	
	void LoadInheritence() {
		foreach (LBGroupPermission perm : allGroups) {
			perm.FillInheritedPermissions();
		}
	}
	
	LBGroupPermission FindHighestGroup() {
		foreach (LBGroupPermission perm : allGroups) {
			if (perm.nextGroupUID == -1)
				return perm;
		}
		return null;
	}
	
	LBGroupPermission FindLowestGroup() {
		foreach (LBGroupPermission perm : allGroups) {
			if (perm.previousGroupUID == -1)
				return perm;
		}
		return null;
	}
	
	LBGroupPermission FindPermissionGroupByUID(int uid) {
		if (uid == -1)
			return null;
		foreach (LBGroupPermission perm : allGroups) {
			if (perm.UID == uid)
				return perm;
		}
		return null;
	}
	
	LBGroupPermission GetNextPermission(LBGroupPermission perm) {
		if (!perm)
			return null;
		return FindPermissionGroupByUID(perm.nextGroupUID);
	}
		
	LBGroupPermission GetNextPermission(int uid) {
		return GetNextPermission(FindPermissionGroupByUID(uid));
	}

	LBGroupPermission GetPreviousPermission(LBGroupPermission perm) {
		if (!perm)
			return null;
		return FindPermissionGroupByUID(perm.nextGroupUID);
	}
		
	LBGroupPermission GetPreviousPermission(int uid) {
		return GetPreviousPermission(FindPermissionGroupByUID(uid));
	}

}