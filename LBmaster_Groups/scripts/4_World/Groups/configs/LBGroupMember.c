class LBGroupMember : LBMarker {

	string steamid;
	int permissionGroup;
	bool online = false;
	float health;
	[NonSerialized()]
	int lastPBCheck = 0;
	[NonSerialized()]
	PlayerBase clientPBFound;
	[NonSerialized()]
	string hashedId = "";
	
	void FindPlayerBase() {
		//ref array<Man> players = ClientData.m_PlayerBaseList;
		ref array<PlayerBase> players = PlayerBase.lb_player_list;
		Print("Check PlayerBase: " + name + ". List Size: " + PlayerBase.lb_player_list.Count());
		int hashidhash = hashedId.Hash(); // Second Identifier as a backup plan if the Identity gets deleted from Helicopter Mod
		foreach (PlayerBase player : players) {
			if (player && player.IsAlive() && player.GetIdentity()) {
				string hashid = player.GetIdentity().GetId();
				if (hashedId.Length() > 0 && (hashid == hashedId || hashidhash == player.identityIdHash)) {
					clientPBFound = player;
					return;
				}
			}
		}
	}
	
	LBGroupPermission GetPermission() {
		return LBGroupPermissions.Get().FindPermissionGroupByUID(permissionGroup);
	}
	
	override void SetPosition(vector pos) {
		if (!IsValidPlayerBase())
			super.SetPosition(pos);
	}
	
	vector GetHeadPos() {
		vector vec = "0 0 0";
		MiscGameplayFunctions.GetHeadBonePos(clientPBFound, vec);
		vec = vec + "0 0.2 0";
		return vec;
	}
	
	override bool ShouldCenterWidget() {
		return true;
	}
	
	override bool ShowMarker() {
		if (!mainWidget || !GetGame() || !GetGame().GetPlayer() || !GetGame().GetPlayer().IsAlive() || GetGame().GetPlayer().IsUnconscious())
			return false;
		//Print("Marker Show was ok. My Steamid: " + MissionBaseWorld.mySteamid + " This Steamid: " + steamid + " Name: " + name + " Online: " + online);
		if (MissionBaseWorld.mySteamid.Length() == 17 && MissionBaseWorld.mySteamid == steamid)
			return false;
		if (online)
			return true;
		return LBGroupMainConfig.Get().offlinePlayer3dMarkerDistance < 0 || LBGroupMainConfig.Get().offlinePlayer3dMarkerDistance > dist;
	}
	
	override bool UpdateMarkerClient() {
		if (!super.UpdateMarkerClient())
			return false;
		int now = GetGame().GetTime();
		if (now - lastPBCheck > 1000) { // Always look for new Playerbase to see if that fixes invisible Tags related to Cars and Helicopters
			FindPlayerBase();
			lastPBCheck = now;
		}
		if (IsValidPlayerBase()) {
			position = GetHeadPos();
		}
		return true;
	}
	
	override void OnMarkerRPCClient(int type, ParamsReadContext ctx) {
		super.OnMarkerRPCClient(type, ctx);
		if (type == LBGroupRPCs.HEALTH) {
			float health = 0;
			if (!ctx.Read(health))
				return;
			SetHealth(health);
		} else if (type == LBGroupRPCs.PERMISSION) {
			int perm = 0;
			if (!ctx.Read(perm))
				return;
			SetPermission(perm);
		} else if (type == LBGroupRPCs.ONLINE) {
			bool online = 0;
			string hashedId;
			if (!ctx.Read(online))
				return;
			if (!ctx.Read(hashedId))
				return;
			SetOnline(online, hashedId);
		}
	}
	
	void SetPermission(int perm) {
		permissionGroup = perm;
	}
	
	void SetOnline(bool on, string hashedId) {
		online = on;
		this.hashedId = hashedId;
		if (GetGame().IsClient())
			SetColor();
	}
	
	void SetHealth(float health) {
		this.health = health;
	}
	
	bool IsValidPlayerBase() {
		if (!clientPBFound || !clientPBFound.IsAlive())
			return false;
		return true;
	}
	
	static LBGroupMember CreateMember(PlayerBase pb) {
		if (!pb)
			return null;
		PlayerIdentity ident = pb.GetIdentity();
		if (!ident)
			return null;
		LBGroupMember member = new LBGroupMember();
		member.steamid = ident.GetPlainId();
		member.hashedId = ident.GetId();
		member.SetupMarker(LBMarkerType.GROUP_PLAYER_MARKER, ident.GetName(), "", pb.GetPosition());
		return member;
	}
	
	override int GetColorARGB() {
		if (online) {
			return LBColorManager.Get().GetColor("Player Online");
		} else {
			return LBColorManager.Get().GetColor("Player Offline");
		}
	}
	
	override int Get3DColorARGB() {
		if (online) {
			return LBColorManager.Get().GetColor("Player 3D Marker");
		} else {
			return super.Get3DColorARGB();
		}
	}
	
	override bool ReadFromCtx(ParamsReadContext ctx) {
		if (!super.ReadFromCtx(ctx))
			return false;
		if (!ctx.Read(steamid))
			return false;
		if (!ctx.Read(hashedId))
			return false;
		if (!ctx.Read(permissionGroup))
			return false;
		if (!ctx.Read(currentSubgroup))
			return false;
		if (!ctx.Read(online))
			return false;
		if (!ctx.Read(health))
			return false;
		icon = LBGroupMainConfig.Get().otherPlayerIconsPath;
		return true;
	}
	
	override void WriteToCtx(ParamsWriteContext ctx) {
		super.WriteToCtx(ctx);
		ctx.Write(steamid);
		ctx.Write(hashedId);
		ctx.Write(permissionGroup);
		ctx.Write(currentSubgroup);
		ctx.Write(online);
		ctx.Write(health);
	}

}