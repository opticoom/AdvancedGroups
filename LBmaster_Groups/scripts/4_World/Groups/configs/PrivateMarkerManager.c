class LBPrivateMarkerManager {

	ref array<ref Param2<string, ref array<ref LBMarker>>> markers = new array<ref Param2<string, ref array<ref LBMarker>>>();
	ref array<ref LBMarker> privateMarkers = new array<ref LBMarker>();
	string currentServer;
	
	static ref LBPrivateMarkerManager g_LBPrivateMarkerManager;
	
	static void Delete() {
		if (g_LBPrivateMarkerManager)
			delete g_LBPrivateMarkerManager;
	}
	
	void ~LBPrivateMarkerManager() {
		Save();
		foreach (LBMarker marker : privateMarkers)
			delete marker;
		privateMarkers.Clear();
	}
	
	static LBPrivateMarkerManager Get(string server = "") {
		if (!g_LBPrivateMarkerManager) {
			g_LBPrivateMarkerManager = Load(server);
			g_LBPrivateMarkerManager.InitMarkers();
		}
		return g_LBPrivateMarkerManager;
	}
	
	static LBPrivateMarkerManager Load(string server) {
		Print("Loading Private Markers for Server " + server);
		LBPrivateMarkerManager mgr;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX))
			MakeDirectory(LBGroupConstants.SAVE_PREFIX);
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER)) {
			mgr = LoadDefault();
			mgr.currentServer = server;
			JsonFileLoader<ref array<ref Param2<string, ref array<ref LBMarker>>>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER, mgr.markers);
			return mgr;
		}
		mgr = new LBPrivateMarkerManager();
		mgr.currentServer = server;
		JsonFileLoader<ref array<ref Param2<string, ref array<ref LBMarker>>>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER, mgr.markers);
		return mgr;
	}
	
	static LBPrivateMarkerManager LoadDefault() {
		LBPrivateMarkerManager def = new LBPrivateMarkerManager;
		return def;
	}
	
	void Save() {
		JsonFileLoader<ref array<ref Param2<string, ref array<ref LBMarker>>>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER, markers);
	}
	
	void InitMarkers() {
		privateMarkers = GetPrivateMarkersFromList();
		foreach (LBMarker mark : privateMarkers) {
			mark.InitMarker();
			mark.AddToAllList();
		}
	}
	
	private array<ref LBMarker> GetPrivateMarkersFromList() {
		foreach (Param2<string, ref array<ref LBMarker>> param : markers) {
			if (param.param1 == currentServer) {
				return param.param2;
			}
		}
		privateMarkers = new array<ref LBMarker>();
		markers.Insert(new Param2<string, ref array<ref LBMarker>>(currentServer, privateMarkers));
		return privateMarkers;
	}
	
	void AddMarker(LBMarker marker) {
		privateMarkers.Insert(marker);
		marker.InitMarker();
		Save();
	}
	
	void RemoveMarker(LBMarker marker) {
		privateMarkers.RemoveItem(marker);
		delete marker;
		Save();
	}
	
	LBMarker FindMarkerByUID(int uid) {
		foreach (LBMarker marker : privateMarkers) {
			if (marker.uid == uid)
				return marker;
		}
		return null;
	}
	
	bool FindMarkerByIcon(string icon, out LBMarker mark) {
		foreach (LBMarker marker : privateMarkers) {
			if (marker.icon == icon) {
				mark = marker;
				return true;
			}
		}
		return false;
	}
	
	bool FindNearestMarker(vector position, out LBMarker markero, out float distance) {
		if (privateMarkers.Count() == 0)
			return false;
		float bestDist = 0;
		LBMarker bestMarker = null;
		foreach (LBMarker marker : privateMarkers) {
			if (!bestMarker || vector.Distance(position, marker.position) < bestDist) {
				bestMarker = marker;
				bestDist = vector.Distance(position, marker.position) < bestDist;
			}
		}
		if (bestMarker) {
			markero = bestMarker;
			distance = bestDist;
			return true;
		}
		return false;
	}
	
}