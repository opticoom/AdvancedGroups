class LBStaticMarkerManager {

	ref array<ref LBServerMarker> staticMarkers = new array<ref LBServerMarker>();
	ref array<ref LBServerMarker> tempStaticMarkers = new array<ref LBServerMarker>();
	
	static ref LBStaticMarkerManager g_LBStaticMarkerManager;
	
	static LBStaticMarkerManager Get() {
		if (!g_LBStaticMarkerManager) {
			g_LBStaticMarkerManager = Load();
		}
		return g_LBStaticMarkerManager;
	}
	
	static void Delete() {
		if (g_LBStaticMarkerManager)
			delete g_LBStaticMarkerManager;
	}
	
	static LBStaticMarkerManager Load() {
		LBStaticMarkerManager markerMgr;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_STATIC_MARKER)) {
			markerMgr = LoadDefault();
			JsonFileLoader<array<ref LBServerMarker>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_STATIC_MARKER, markerMgr.staticMarkers);
			return markerMgr;
		}
		markerMgr = new LBStaticMarkerManager();
		JsonFileLoader<array<ref LBServerMarker>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_STATIC_MARKER, markerMgr.staticMarkers);
		return markerMgr;
	}
	
	static LBStaticMarkerManager LoadDefault() {
		LBStaticMarkerManager mgr = new LBStaticMarkerManager();
		LBServerMarker marker = new LBServerMarker();
		marker.Init("Green Mountain Trader", "3714 0 5998", "DZ\\gear\\navigation\\data\\map_tree_ca.paa", 0, 255, 0);
		mgr.staticMarkers.Insert(marker);
		marker = new LBServerMarker();
		marker.Init("Toxic Zone", "1590 0 14123", "DZ\\gear\\navigation\\data\\map_tree_ca.paa", 255, 0, 0);
		mgr.staticMarkers.Insert(marker);
		return mgr;
	}
	
	void SendMarkerRefreshRPC() {
		RPC_LB(null, null, LBGroupRPCs.CONFIG_SYNC_STATIC_MARKERS);
	}
	
	void RPC_LB(PlayerIdentity sender, ParamsReadContext ctx, int type) {}
}

class LBStaticMarkerManagerClient {

	ref array<ref LBServerMarker> staticMarkers = new array<ref LBServerMarker>();
	
	static ref LBStaticMarkerManagerClient g_LBGroupGroups;
	
	static void Delete() {
		if (g_LBGroupGroups)
			delete g_LBGroupGroups;
	}
	
	void ~LBStaticMarkerManagerClient() {
		foreach (LBServerMarker marker : staticMarkers)
			delete marker;
		staticMarkers.Clear();
	}
	
	static LBStaticMarkerManagerClient Get() {
		if (!g_LBGroupGroups) {
			g_LBGroupGroups = new LBStaticMarkerManagerClient();
			GetGame().RPCSingleParam(null, LBGroupRPCs.CONFIG_SYNC_STATIC_MARKERS, new Param1<bool>(true), true);
		}
		return g_LBGroupGroups;
	}
	
	void RequestGlobalMarkerAdd(LBMarker marker) {
		ScriptRPC rpc = new ScriptRPC();
		marker.WriteToCtx(rpc);
		rpc.Send(null, LBGroupRPCs.CONFIG_GLOBAL_MARKER_ADD, true);
	}
	
	void RequestGlobalMarkerRemove(int uid) {
		ScriptRPC rpc = new ScriptRPC();
		rpc.Write(uid);
		rpc.Send(null, LBGroupRPCs.CONFIG_GLOBAL_MARKER_REMOVE, true);
	}
	
	void RPC_LB(PlayerIdentity sender, ParamsReadContext ctx) {
		int count;
		if (!ctx.Read(count)) {
			Print("Unable to receive Static Markers from Server !");
			return;
		}
		DeleteStaticMarkers();
		for (int i = 0; i < count; i++) {
			LBServerMarker mark = new LBServerMarker;
			if (mark.ReadFromCtx(ctx)) {
				staticMarkers.Insert(mark);
			} else {
				Print("Failed to read received Markers from Server. Index: " + i);
				return;
			}
		}
		Print("Received Static Markers from Server: " + staticMarkers.Count());
		InitAllMarkers();
	}
	
	void InitAllMarkers() {
		foreach (LBServerMarker marker : staticMarkers) {
			marker.InitMarker();
		}
	}
	
	void DeleteStaticMarkers() {
		foreach (LBServerMarker marker : staticMarkers) {
			delete marker;
		}
		staticMarkers.Clear();
	}
	
	void AddMarker(LBServerMarker marker) {
		staticMarkers.Insert(marker);
	}
	
	void RemoveMarker(LBServerMarker marker) {
		staticMarkers.RemoveItem(marker);
	}
	
	LBServerMarker FindMarkerByUID(int uid) {
		foreach (LBServerMarker marker : staticMarkers) {
			if (marker.uid == uid)
				return marker;
		}
		return null;
	}
	
	bool FindNearestMarker(vector position, out LBMarker markero, out float distance) {
		if (staticMarkers.Count() == 0)
			return false;
		float bestDist = 0;
		LBMarker bestMarker = null;
		foreach (LBMarker marker : staticMarkers) {
			if (!bestMarker || vector.Distance(position, marker.position) < bestDist) {
				bestMarker = marker;
				bestDist = vector.Distance(position, marker.position) < bestDist;
			}
		}
		if (bestMarker) {
			markero = bestMarker;
			distance = bestDist;
			return true;
		}
		return false;
	}
	
}