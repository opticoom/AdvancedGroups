class LBGroupMainConfig {
	const static int CURRENT_VERSION = 5;
	int configVersion = CURRENT_VERSION;
	string serverLogoPath = "";
	bool canSeeOwnPlayerOnMap = true;
	string ownPlayerIconPath = "";
	string otherPlayerIconsPath = "";
	int inactiveGroupLifetimeDays = 30;
	int tacticalPingLifetimeSeconds = 8;
	int inviteCooldownSeconds = 120;
	float inviteMaxDistance = -1;
	bool inviteActionEnabled = false;
	bool inviteActionShowName = false;
	ref array<ref MarkerConfigEntry> markerConfig = new array<ref MarkerConfigEntry>();
	ref TStringArray availableIcons = new TStringArray();
	string customMapLayoutFolder = "";
	string customPlayerlistLayoutFolder = "";
	string customCompassLayoutFolder = "";
	ref array<ref LayoutStyleEntry> layoutStyles = new array<ref LayoutStyleEntry>();
	bool enableCompassHud = false;
	bool enablePlayerList = true;
	bool enablePlayerListDistance = true;
	bool enableSubGroups = true;
	bool enableSubGroupSharedPlayerMapMarker = false;
	bool enableSubGroupSharedPingMapMarker = false;
	bool enableInfoPanelSurvivorCount = true;
	bool enableInfoPanelCursorCoordinates = true;
	bool enableInfoPanelIngameTime = true;
	bool enableInfoPanelRealTime = true;
	bool disableInfoPanelModCreatorMention = false;
	int groupMarkerLimit = 20;
	ref TStringArray adminSteamids = new TStringArray();
	float offlinePlayer3dMarkerDistance = 20.0;
	ref TStringArray subGroupNames = new TStringArray();
	ref array<ref LBButtonConfig> buttonConfig = new array<ref LBButtonConfig>();
	[NonSerialized()]
	int groupCreationCost = 0;
	
	static ref LBGroupMainConfig g_LBGroupMainConfig;
	
	static void Delete() {
		if (g_LBGroupMainConfig)
			delete g_LBGroupMainConfig;
	}
	
	static LBGroupMainConfig Get() {
		if (!g_LBGroupMainConfig) {
			if (GetGame().IsServer() || !GetGame().IsMultiplayer()) {
				g_LBGroupMainConfig = Load();
			} else if (GetGame().IsClient()) {
				g_LBGroupMainConfig = new LBGroupMainConfig();
				GetGame().RPCSingleParam(null, LBGroupRPCs.CONFIG_SYNC_MAIN, new Param1<bool>(true), true);
			}
		}
		return g_LBGroupMainConfig;
	}
	
	static LBGroupMainConfig Load() {
		LBGroupMainConfig cfg;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_MAIN_CONFIG)) {
			cfg = LoadDefault();
			JsonFileLoader<LBGroupMainConfig>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_MAIN_CONFIG, cfg);
			return cfg;
		}
		cfg = new LBGroupMainConfig();
		JsonFileLoader<LBGroupMainConfig>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_MAIN_CONFIG, cfg);
		if (cfg.configVersion != CURRENT_VERSION) {
			UpgradeConfigFile(cfg);
			JsonFileLoader<LBGroupMainConfig>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_MAIN_CONFIG, cfg);
		}
		return cfg;
	}
	
	static void UpgradeConfigFile(LBGroupMainConfig cfg) {
		Print("Upgrading MainConfig from Version " + cfg.configVersion + " to Version " + CURRENT_VERSION);
		if (cfg.configVersion < 3) {
			cfg.layoutStyles = new array<ref LayoutStyleEntry>();
			LayoutStyleEntry lEntry = new LayoutStyleEntry();
			lEntry.styles.Insert("default");
			lEntry.entryName = "Map Layout";
			cfg.layoutStyles.Insert(lEntry);
			
			lEntry = new LayoutStyleEntry();
			lEntry.styles.Insert("default");
			lEntry.entryName = "Compass Layout";
			cfg.layoutStyles.Insert(lEntry);
			
			lEntry = new LayoutStyleEntry();
			lEntry.styles.Insert("default");
			lEntry.styles.Insert("small");
			lEntry.styles.Insert("tiny");
			lEntry.entryName = "Player List";
			cfg.layoutStyles.Insert(lEntry);
		}
		if (cfg.configVersion < 4) {
			cfg.enablePlayerListDistance = true;
		}
		if (cfg.configVersion < 5) {
			cfg.inviteMaxDistance = -1;
			cfg.inviteActionEnabled = false;
			cfg.inviteActionShowName = false;
		}
		cfg.configVersion = CURRENT_VERSION;
		
	}
	string LB_SSL() {
		return "no";
	}
	
	static LBGroupMainConfig LoadDefault() {
		LBGroupMainConfig def = new LBGroupMainConfig();
		def.configVersion = CURRENT_VERSION;
		def.serverLogoPath = "";
		def.canSeeOwnPlayerOnMap = true;
		def.inactiveGroupLifetimeDays = 30;
		def.ownPlayerIconPath = "DZ\\gear\\navigation\\data\\map_tree_ca.paa";
		def.otherPlayerIconsPath = "DZ\\gear\\navigation\\data\\map_tree_ca.paa";
		def.customMapLayoutFolder = "";
		def.customPlayerlistLayoutFolder = "";
		def.customCompassLayoutFolder = "";
		
		def.layoutStyles = new array<ref LayoutStyleEntry>();
		LayoutStyleEntry lEntry = new LayoutStyleEntry();
		lEntry.styles.Insert("default");
		lEntry.entryName = "Map Layout";
		def.layoutStyles.Insert(lEntry);
		
		lEntry = new LayoutStyleEntry();
		lEntry.styles.Insert("default");
		lEntry.entryName = "Compass Layout";
		def.layoutStyles.Insert(lEntry);
		
		lEntry = new LayoutStyleEntry();
		lEntry.styles.Insert("default");
		lEntry.styles.Insert("small");
		lEntry.styles.Insert("tiny");
		lEntry.entryName = "Player List";
		def.layoutStyles.Insert(lEntry);
		
		def.enableCompassHud = true;
		def.enablePlayerList = true;
		def.enablePlayerListDistance = true;
		def.enableSubGroups = true;
		def.enableSubGroupSharedPlayerMapMarker = false;
		def.enableSubGroupSharedPingMapMarker = false;
		def.enableInfoPanelSurvivorCount = true;
		def.enableInfoPanelCursorCoordinates = true;
		def.enableInfoPanelIngameTime = true;
		def.enableInfoPanelRealTime = true;
		def.disableInfoPanelModCreatorMention = false;
		def.offlinePlayer3dMarkerDistance = 20.0;
		def.adminSteamids.Insert("76561198141097113");
		def.subGroupNames.Insert("Alpha");
		def.subGroupNames.Insert("Bravo");
		def.subGroupNames.Insert("Charlie");
		def.subGroupNames.Insert("Delta");
		def.subGroupNames.Insert("Echo");
		def.subGroupNames.Insert("Foxtrot");
		def.subGroupNames.Insert("Golf");
		def.subGroupNames.Insert("Hotel");
		def.subGroupNames.Insert("India");
		def.subGroupNames.Insert("Juliett");
		def.markerConfig.Insert(MarkerConfigEntry.Init(LBMarkerType.SERVER_STATIC, -1, true, true, true, false));
		def.markerConfig.Insert(MarkerConfigEntry.Init(LBMarkerType.SERVER_DYNAMIC, -1, true, true, true, false));
		def.markerConfig.Insert(MarkerConfigEntry.Init(LBMarkerType.GROUP_PING, -1, true, false, false, true));
		def.markerConfig.Insert(MarkerConfigEntry.Init(LBMarkerType.GROUP_MARKER, -1, true, true, true, false));
		def.markerConfig.Insert(MarkerConfigEntry.Init(LBMarkerType.PRIVATE_MARKER, -1, true, true, true, false));
		def.markerConfig.Insert(MarkerConfigEntry.Init(LBMarkerType.GROUP_PLAYER_MARKER, 2000, true, false, true, true));
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\marker.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\marker-stroked.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\cross.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\home.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\camp.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\hospital.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\flag.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\star.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\car.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\parking.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\heli.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\rail.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\ship.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\scooter.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\bank.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\restaurant.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\post.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\castle.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\ranger-station.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\water.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\triangle.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\cow.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\bear.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\car-repair.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\communications.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\roadblock.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\stadium.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\skull.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\rocket.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\bbq.paa");
		def.availableIcons.Insert("LBmaster_Groups\\gui\\icons\\ping.paa");
		def.buttonConfig.Insert(LBButtonConfig.InitButton("Website", "musterseite.de", "google.com"));
		def.buttonConfig.Insert(LBButtonConfig.InitButton("Teamspeak", "teamspeak.com", "ts3server://teamspeak.com"));
		def.buttonConfig.Insert(LBButtonConfig.InitButton("Donate", "Donate via PayPal", "paypal.me/lbmaster"));
		return def;
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(serverLogoPath);
		ctx.Write(canSeeOwnPlayerOnMap);
		ctx.Write(ownPlayerIconPath);
		ctx.Write(otherPlayerIconsPath);
		ctx.Write(customMapLayoutFolder);
		ctx.Write(customPlayerlistLayoutFolder);
		ctx.Write(customCompassLayoutFolder);
		ctx.Write(layoutStyles.Count());
		foreach (LayoutStyleEntry style : layoutStyles) {
			ctx.Write(style.entryName);
			ctx.Write(style.styles.Count());
			foreach (string stl : style.styles)
				ctx.Write(stl);
		}
		ctx.Write(enableCompassHud);
		ctx.Write(enablePlayerList);
		ctx.Write(enablePlayerListDistance);
		ctx.Write(enableSubGroups);
		ctx.Write(enableSubGroupSharedPlayerMapMarker);
		ctx.Write(enableSubGroupSharedPingMapMarker);
		ctx.Write(enableInfoPanelSurvivorCount);
		ctx.Write(enableInfoPanelCursorCoordinates);
		ctx.Write(enableInfoPanelIngameTime);
		ctx.Write(enableInfoPanelRealTime);
		ctx.Write(disableInfoPanelModCreatorMention);
		ctx.Write(offlinePlayer3dMarkerDistance);
		ctx.Write(subGroupNames.Count());
		foreach (string str : subGroupNames) {
			ctx.Write(str);
		}
		ctx.Write(markerConfig.Count());
		foreach (MarkerConfigEntry entry : markerConfig) {
			entry.WriteToCtx(ctx);
		}
		ctx.Write(availableIcons.Count());
		foreach (string s : availableIcons) {
			ctx.Write(s);
		}
		ctx.Write(buttonConfig.Count());
		foreach (LBButtonConfig btnCfg : buttonConfig) {
			btnCfg.WriteToCtx(ctx);
		}
		ctx.Write(groupCreationCost);
	}
	
	bool ReadFromCtx(ParamsReadContext ctx) {
		if (!ctx.Read(serverLogoPath))
			return false;
		if (!ctx.Read(canSeeOwnPlayerOnMap))
			return false;
		if (!ctx.Read(ownPlayerIconPath))
			return false;
		if (!ctx.Read(otherPlayerIconsPath))
			return false;
		if (!ctx.Read(customMapLayoutFolder))
			return false;
		if (!ctx.Read(customPlayerlistLayoutFolder))
			return false;
		if (!ctx.Read(customCompassLayoutFolder))
			return false;
		int count1 = 0;
		if (!ctx.Read(count1))
			return false;
		layoutStyles.Clear();
		for (int i = 0; i < count1; i++) {
			string name;
			if (!ctx.Read(name))
				return false;
			int count2 = 0;
			if (!ctx.Read(count2))
				return false;
			LayoutStyleEntry lEntry = new LayoutStyleEntry();
			lEntry.entryName = name;
			for (int a = 0; a < count2; a++) {
				string stl;
				if (!ctx.Read(stl))
					return false;
				lEntry.styles.Insert(stl);
			}
			layoutStyles.Insert(lEntry);
		}
		if (!ctx.Read(enableCompassHud))
			return false;
		if (!ctx.Read(enablePlayerList))
			return false;
		if (!ctx.Read(enablePlayerListDistance))
			return false;
		if (!ctx.Read(enableSubGroups))
			return false;
		if (!ctx.Read(enableSubGroupSharedPlayerMapMarker))
			return false;
		if (!ctx.Read(enableSubGroupSharedPingMapMarker))
			return false;
		if (!ctx.Read(enableInfoPanelSurvivorCount))
			return false;
		if (!ctx.Read(enableInfoPanelCursorCoordinates))
			return false;
		if (!ctx.Read(enableInfoPanelIngameTime))
			return false;
		if (!ctx.Read(enableInfoPanelRealTime))
			return false;
		if (!ctx.Read(disableInfoPanelModCreatorMention))
			return false;
		if (!ctx.Read(offlinePlayer3dMarkerDistance))
			return false;
		int count = 0;
		if (!ctx.Read(count))
			return false;
		for (i = 0; i < count; i++) {
			string str;
			if (!ctx.Read(str))
				return false;
			subGroupNames.Insert(str);
		}
		count = 0;
		if (!ctx.Read(count))
			return false;
		for (i = 0; i < count; i++) {
			MarkerConfigEntry entry = new MarkerConfigEntry();
			if (!entry.ReadFromCtx(ctx))
				return false;
			markerConfig.Insert(entry);
		}
		if (!ctx.Read(count))
			return false;
		for (i = 0; i < count; i++) {
			string path;
			if (!ctx.Read(path))
				return false;
			availableIcons.Insert(path);
		}
		if (!ctx.Read(count))
			return false;
		for (i = 0; i < count; i++) {
			LBButtonConfig cfg2 = new LBButtonConfig();
			if (!cfg2.ReadFromCtx(ctx))
				return false;
			buttonConfig.Insert(cfg2);
		}
		if (!ctx.Read(groupCreationCost))
			return false;
		return true;
	}
	
	void RPC_LB(PlayerIdentity sender, ParamsReadContext ctx) {
		if (GetGame().IsServer()) {
			ScriptRPC rpc = new ScriptRPC();
			WriteToCtx(rpc);
			rpc.Send(null, LBGroupRPCs.CONFIG_SYNC_MAIN, true, sender);
		} else {
			if (!ReadFromCtx(ctx)) {
				Print("Unable to read Main Config from Server !");
				return;
			}
			Print("Successfully Received Main Config from Server");
		}
	}
	
	MarkerConfigEntry GetMarkerConfigEntry(LBMarkerType type) {
		foreach (MarkerConfigEntry entry : markerConfig) {
			if (entry.type == type)
				return entry;
		}
		return null;
	}
	
	bool IsAdmin(string steamid) {
		return adminSteamids.Find(steamid) != -1;
	}
	
	bool IsAdmin(PlayerIdentity ident) {
		if (!ident)
			return false;
		return IsAdmin(ident.GetPlainId());
	}
	
	bool IsAdmin(PlayerBase player) {
		if (!player)
			return false;
		return IsAdmin(player.GetIdentity());
	}
	
	bool IsCompassVisible(LBMarkerType type) {
		MarkerConfigEntry cfgEntry = GetMarkerConfigEntry(type);
		if (!cfgEntry)
			return false;
		return cfgEntry.displayCompass;
	}
	
	void PrintMarkerConfigEntries() {
		Print("Marker config Entires: " + markerConfig.Count());
		foreach (MarkerConfigEntry entry : markerConfig) {
			Print("" + entry.type + " " + entry.maxDistance + " " + entry.display3d + " "  + entry.displayDistance + " "  + entry.displayMap);
		}
	}
}
class MarkerConfigEntry {
	
	LBMarkerType type;
	string typeString;
	int maxDistance;
	bool display3d;
	bool displayDistance;
	bool displayMap;
	bool displayCompass;
	
	static MarkerConfigEntry Init(LBMarkerType type2, int maxDist, bool disp3d, bool dispDist, bool dispMap, bool dispComp) {
		MarkerConfigEntry ent = new MarkerConfigEntry;
		ent.type = type2;
		ent.typeString = typename.EnumToString(LBMarkerType, type2);
		ent.maxDistance = maxDist;
		ent.display3d = disp3d;
		ent.displayDistance = dispDist;
		ent.displayMap = dispMap;
		ent.displayCompass = dispComp;
		return ent;
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(type);
		ctx.Write(maxDistance);
		ctx.Write(display3d);
		ctx.Write(displayDistance);
		ctx.Write(displayMap);
		ctx.Write(displayCompass);
	}
	
	bool ReadFromCtx(ParamsReadContext ctx) {
		if (!ctx.Read(type))
			return false;
		if (!ctx.Read(maxDistance))
			return false;
		if (!ctx.Read(display3d))
			return false;
		if (!ctx.Read(displayDistance))
			return false;
		if (!ctx.Read(displayMap))
			return false;
		if (!ctx.Read(displayCompass))
			return false;
		return true;
	}
	
}
class LayoutStyleEntry {

	string entryName;
	ref TStringArray styles = new TStringArray();

}