class LBMarkerVisibilityManager {

	static int CURRENT_VERSION = 2;
	int version = 0;
	
	ref array<ref LBMarkerVisibilityEntry> entries = new array<ref LBMarkerVisibilityEntry>();
	ref array<ref LBGlobalVisibilityEntry> globalentries = new array<ref LBGlobalVisibilityEntry>();
	
	int globalVisiblityState;
	string pingMarkerIcon = "LBmaster_Groups\\gui\\icons\\ping.paa";
	bool compassEnabled = true;
	bool playerlistEnabled = true;
	bool disableShowClantextures = false;
	int chatSize = 15;
	
	static ref LBMarkerVisibilityManager g_LBMarkerVisibilityManager;
	
	static void Delete() {
		if (g_LBMarkerVisibilityManager)
			delete g_LBMarkerVisibilityManager;
	}
	
	void ~LBMarkerVisibilityManager() {
		Save();
	}
	
	static LBMarkerVisibilityManager Get() {
		if (!g_LBMarkerVisibilityManager) {
			g_LBMarkerVisibilityManager = Load();
		}
		return g_LBMarkerVisibilityManager;
	}
	
	static LBMarkerVisibilityManager Load() {
		LBMarkerVisibilityManager mgr;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX))
			MakeDirectory(LBGroupConstants.SAVE_PREFIX);
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER_STATES)) {
			mgr = LoadDefault();
			JsonFileLoader<LBMarkerVisibilityManager>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER_STATES, mgr);
			return mgr;
		}
		mgr = new LBMarkerVisibilityManager();
		JsonFileLoader<LBMarkerVisibilityManager>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER_STATES, mgr);
		if (mgr.version != CURRENT_VERSION) {
			Print("Upgrading Visibility Manager Version from " + mgr.version + " to " + CURRENT_VERSION);
			mgr.UpgradeVersion(mgr.version, CURRENT_VERSION);
			mgr.Save();
		}
		return mgr;
	}
	
	static LBMarkerVisibilityManager LoadDefault() {
		LBMarkerVisibilityManager def = new LBMarkerVisibilityManager;
		def.version = CURRENT_VERSION;
		def.globalVisiblityState = 0;
		def.pingMarkerIcon = "LBmaster_Groups\\gui\\icons\\ping.paa";
		def.compassEnabled = true;
		def.playerlistEnabled = true;
		return def;
	}
	
	void UpgradeVersion(int from, int to) {
		version = CURRENT_VERSION;
		if (from < 1) {
			pingMarkerIcon = "LBmaster_Groups\\gui\\icons\\ping.paa";
			compassEnabled = true;
			playerlistEnabled = true;
		}
		if (from < 2) {
			chatSize = 15;
		}
	}
	
	int GetChatSize() {
		return Math.Max(7, chatSize);
	}
	
	void Save() {
		for (int i = 0; i < entries.Count(); i++) {
			LBMarkerVisibilityEntry entry = entries.Get(i);
			if (entry.displaystate == 0) {
				entries.Remove(i--);
			}
		}
		JsonFileLoader<LBMarkerVisibilityManager>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER_STATES, this);
	}
	
	string GetPingMarkerIcon() {
		if (!FileExist(pingMarkerIcon) || pingMarkerIcon.Length() < 4)
			return "LBmaster_Groups\\gui\\icons\\ping.paa";
		return pingMarkerIcon;
	}
	
	void ResetPingToDefault() {
		SetPingMarkerIcon("LBmaster_Groups\\gui\\icons\\ping.paa");
		chatSize = 15;
	}
	
	void ResetPingToLast() {
		LBMarkerVisibilityManager mgr = new LBMarkerVisibilityManager();
		if (FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER_STATES)) {
			JsonFileLoader<LBMarkerVisibilityManager>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_PRIVATE_MARKER_STATES, mgr);
			SetPingMarkerIcon(mgr.pingMarkerIcon);
			chatSize = mgr.chatSize;
		} else {
			ResetPingToDefault();
		}
	}
	
	void SetPingMarkerIcon(string icon) {
		this.pingMarkerIcon = icon;
	}
	
	TStringArray GetPingMarkerIcons() {
		TStringArray arr = new TStringArray();
		foreach (string icon : LBGroupMainConfig.Get().availableIcons) {
			arr.Insert(icon);
		}
		if (arr.Find("LBmaster_Groups\\gui\\icons\\ping.paa") == -1 && arr.Find("LBmaster_Groups/gui/icons/ping.paa") == -1)
			arr.Insert("LBmaster_Groups\\gui\\icons\\ping.paa");
		return arr;
	}
	
	LBMarkerVisibilityEntry GetVisibilityOrAdd(int uid) {
		foreach (LBMarkerVisibilityEntry entry : entries) {
			if (entry.uid == uid)
				return entry;
		}
		LBMarkerVisibilityEntry ent = new LBMarkerVisibilityEntry();
		ent.uid = uid;
		ent.displaystate = 0;
		entries.Insert(ent);
		return ent;
	}
	
	LBGlobalVisibilityEntry GetGlobalVisibilityOrAdd(LBMarkerType type) {
		foreach (LBGlobalVisibilityEntry entry : globalentries) {
			if (entry.type == type)
				return entry;
		}
		LBGlobalVisibilityEntry ent = new LBGlobalVisibilityEntry();
		ent.type = type;
		ent.displaystate = 0;
		globalentries.Insert(ent);
		return ent;
	}
	
	bool Is3DVisiblie(int uid, LBMarkerType type, bool testType = true) {
		if (GetVisibilityOrAdd(uid).displaystate != 0)
			return false;
		return !testType || IsGlobal3DVisible(type);
	}
	
	bool IsMapVisible(int uid, LBMarkerType type, bool testType = true) {
		if (GetVisibilityOrAdd(uid).displaystate == 2)
			return false;
		return !testType || IsGlobal2DVisible(type);
	}
	
	bool IsGlobal3DVisible(LBMarkerType type) {
		return GetGlobalVisibilityOrAdd(type).displaystate == 0);
	}
	
	bool IsGlobal2DVisible(LBMarkerType type) {
		return GetGlobalVisibilityOrAdd(type).displaystate != 2);
	}
	
	int GetNextState() {
		globalVisiblityState++;
		globalVisiblityState = globalVisiblityState % 6;
		if (globalVisiblityState == 0) {
			SetAllGlobalStates(0);
		} else if (globalVisiblityState == 1) {
			SetAllGlobalStates(0);
			GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_STATIC).displaystate = 1;
			GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_DYNAMIC).displaystate = 1;
		} else if (globalVisiblityState == 2) {
			SetAllGlobalStates(0);
			GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_STATIC).displaystate = 1;
			GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_DYNAMIC).displaystate = 1;
			GetGlobalVisibilityOrAdd(LBMarkerType.PRIVATE_MARKER).displaystate = 1;
		} else if (globalVisiblityState == 3) {
			SetAllGlobalStates(1);
			GetGlobalVisibilityOrAdd(LBMarkerType.GROUP_PLAYER_MARKER).displaystate = 0;
			GetGlobalVisibilityOrAdd(LBMarkerType.GROUP_PING).displaystate = 0;
		} else if (globalVisiblityState == 4) {
			SetAllGlobalStates(1);
			GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_STATIC).displaystate = 0;
			GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_DYNAMIC).displaystate = 0;
		} else {
			SetAllGlobalStates(1);
		}
		Save();
		return globalVisiblityState;
	}
	
	void SetAllGlobalStates(int state) {
		GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_STATIC).displaystate = state;
		GetGlobalVisibilityOrAdd(LBMarkerType.SERVER_DYNAMIC).displaystate = state;
		GetGlobalVisibilityOrAdd(LBMarkerType.GROUP_PING).displaystate = state;
		GetGlobalVisibilityOrAdd(LBMarkerType.GROUP_MARKER).displaystate = state;
		GetGlobalVisibilityOrAdd(LBMarkerType.GROUP_PLAYER_MARKER).displaystate = state;
		GetGlobalVisibilityOrAdd(LBMarkerType.PRIVATE_MARKER).displaystate = state;
	}
	
	string GetCurrentStateName() {
		if (globalVisiblityState == 0) {
			return "All Visible";
		} else if (globalVisiblityState == 1) {
			return "No Server Marker";
		} else if (globalVisiblityState == 2) {
			return "Only Group Marker";
		} else if (globalVisiblityState == 3) {
			return "Only Player Marker";
		} else if (globalVisiblityState == 4) {
			return "Only Server Marker";
		}
		return "All Hidden";
	}

}
class LBMarkerVisibilityEntry {

	int uid;
	int displaystate; // 0 3D+2D // 1 2D // 2 None
	
	int GetNextState() {
		displaystate++;
		displaystate = displaystate % 3;
		LBMarkerVisibilityManager.Get().Save();
		return displaystate;
	}

}
class LBGlobalVisibilityEntry {

	LBMarkerType type;
	int displaystate; // 0 3D+2D // 1 2D // 2 None
	
	int GetNextState() {
		displaystate++;
		displaystate = displaystate % 3;
		return displaystate;
	}

}