class LBLayoutConfig {
	
	ref array<ref LBLayoutConfigEntry> entries = new array<ref LBLayoutConfigEntry>();
	
	static ref LBLayoutConfig g_LBLayoutConfig;
	static ref ScriptInvoker Event_OnLayoutChanged = new ScriptInvoker();
	
	static LBLayoutConfig Get() {
		if (!g_LBLayoutConfig) {
			if (GetGame().IsClient()) {
				g_LBLayoutConfig = Load();
			} else {
				g_LBLayoutConfig = new LBLayoutConfig();
				g_LBLayoutConfig.LoadDefault();
			}
		}
		return g_LBLayoutConfig;
	}
	
	static LBLayoutConfig Load() {
		LBLayoutConfig mgr = new LBLayoutConfig();
		mgr.LoadDefault();
		if (!FileExist(LBGroupConstants.SAVE_PREFIX))
			MakeDirectory(LBGroupConstants.SAVE_PREFIX);
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_LAYOUT_MANAGER)) {
			mgr.Save();
			return mgr;
		}
		mgr.LoadFromFile();
		return mgr;
	}
	
	void LoadFromFile() {
		array<ref Param2<string, string>> layoutEntries = new array<ref Param2<string, string>>();
		JsonFileLoader<array<ref Param2<string, string>>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_LAYOUT_MANAGER, layoutEntries);
		foreach (Param2<string, string> entry : layoutEntries) {
			LBLayoutConfigEntry ent = GetEntryByEntryName(entry.param1);
			if (ent)
				ent.current = entry.param2;
		}
	}
	
	void Save() {
		array<ref Param2<string, string>> layoutEntries = new array<ref Param2<string, string>>();
		if (FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_LAYOUT_MANAGER)) {
			JsonFileLoader<array<ref Param2<string, string>>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_LAYOUT_MANAGER, layoutEntries);
		}
		foreach (LBLayoutConfigEntry entry : entries) {
			bool found = false;
			foreach (Param2<string, string> param : layoutEntries) {
				if (param.param1 == entry.entryName) {
					found = true;
					param.param2 = entry.current;
					break;
				}
			}
			if (!found) {
				layoutEntries.Insert(new Param2<string, string>(entry.entryName, entry.current));
			}
		}
		JsonFileLoader<array<ref Param2<string, string>>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_LAYOUT_MANAGER, layoutEntries);
	}
	
	static void Reload() {
		g_LBLayoutConfig = Load();
		InvokeOnLayoutChanged();
	}
	
	bool OnStylesReceived(ParamsReadContext ctx) {
		int count1 = 0;
		if (!ctx.Read(count1))
			return false;
		Print("OnStylesReceived. Layouts: " + count1);
		for (int i = 0; i < count1; i++) {
			string name = "";
			if (!ctx.Read(name))
				return false;
			int count2 = 0;
			if (!ctx.Read(count2))
				return false;
			ref TStringArray arr = new TStringArray();
			Print("Styles of " + name + ": " + count2);
			for (int a = 0; a < count2; a++) {
				string str = "";
				if (!ctx.Read(str))
					return false;
				arr.Insert(str);
				Print("Style: " + str);
			}
			LBLayoutConfigEntry cfgEntry = GetEntryByName(name);
			if (cfgEntry)
				cfgEntry.allLayoutsCache = arr;
		}
		int check = 0;
		if (!ctx.Read(check))
			return false;
		return check == 55555;
	}
	
	void ResetAll() {
		entries.Clear();
		LoadDefault();
		InvokeOnLayoutChanged();
	}
	
	void LoadDefault() {
		TStringArray names = new TStringArray();
		names.Insert("Map");
		names.Insert("Map Top Button");
		names.Insert("Map Marker Add Popup");
		names.Insert("Map Marker List Entry");
		names.Insert("Map Page 0 0");
		names.Insert("Map Page 1 0");
		names.Insert("Map Page 1 1");
		names.Insert("Map Page 2 0");
		names.Insert("Map Page 3 0");
		names.Insert("Map Page 5 0");
		TStringArray prefixes = new TStringArray();
		prefixes.Insert("mapmenu_");
		prefixes.Insert("topButton_");
		prefixes.Insert("markerpopup_");
		prefixes.Insert("markerlist/markerlistentry_");
		prefixes.Insert("pages/page_0_0_");
		prefixes.Insert("pages/page_1_0_");
		prefixes.Insert("pages/page_1_1_");
		prefixes.Insert("pages/page_2_0_");
		prefixes.Insert("pages/page_3_0_");
		prefixes.Insert("pages/page_5_0_");
		LBLayoutConfigEntry entry = LBLayoutConfigEntry.InitEntry("Map Layout", names, "default", LBGroupConstants.LAYOUT_MAP_PATH, LBGroupMainConfig.Get().customMapLayoutFolder, prefixes, "default");
		entries.Insert(entry);
		
		names = new TStringArray();
		names.Insert("Compass");
		names.Insert("Compass Marker");
		prefixes = new TStringArray();
		prefixes.Insert("compass_");
		prefixes.Insert("compassMarker_");
		entry = LBLayoutConfigEntry.InitEntry("Compass Layout", names, "default", LBGroupConstants.LAYOUT_COMPASS_PATH, LBGroupMainConfig.Get().customCompassLayoutFolder, prefixes, "default");
		if (LBGroupMainConfig.Get().enableCompassHud)
			entries.Insert(entry);
		
		names = new TStringArray();
		names.Insert("Player List");
		prefixes = new TStringArray();
		prefixes.Insert("playerlistentry_");
		entry = LBLayoutConfigEntry.InitEntry("Player List", names, "default", LBGroupConstants.LAYOUT_PLAYERLIST_PATH, LBGroupMainConfig.Get().customPlayerlistLayoutFolder, prefixes, "default");
		if (LBGroupMainConfig.Get().enablePlayerList)
			entries.Insert(entry);
	}
	
	static void InvokeOnLayoutChanged() {
		Event_OnLayoutChanged.Invoke();
	}
	
	string GetCurrentPageLayout(int pageId, int subId = 0) {
		return GetCurrentLayout("Map Page " + pageId + " " + subId);
	}
	
	string GetCurrentLayout(string name) {
		LBLayoutConfigEntry entry = GetEntryByName(name);
		Print("GetCurrentLayout of " + name + " Entry Found: " + (entry != null));
		if (!entry)
			return "";
		string current = entry.GetCurrentLayout(name);
		Print("Current: " + current);
		return current;
	}
	
	LBLayoutConfigEntry GetEntryByName(string name) {
		foreach (LBLayoutConfigEntry entry : entries) {
			if (entry.HasName(name))
				return entry;
		}
		return null;
	}
	
	LBLayoutConfigEntry GetEntryByEntryName(string entryName) {
		foreach (LBLayoutConfigEntry entry : entries) {
			if (entry.entryName == entryName)
				return entry;
		}
		return null;
	}
	
	TStringArray GetAvailableLayouts() {
		TStringArray avail = new TStringArray();
		foreach (LBLayoutConfigEntry entry : entries)
			avail.Insert(entry.entryName);
		return avail;
	}
	
	TStringArray GetLayoutStyles(string entryName) {
		LBLayoutConfigEntry entry = GetEntryByEntryName(entryName);
		if (!entry)
			return new TStringArray();
		return entry.FindAllLayoutStyles();
	}
	
}

class LBLayoutConfigEntry {
	
	string entryName;
	string current;
	string path, customPath, defaultSelection;
	ref TStringArray prefixes = new TStringArray();
	ref TStringArray names = new TStringArray();
	
	[NonSerialized()]
	ref TStringArray allLayoutsCache = null;
	
	static LBLayoutConfigEntry InitEntry(string entryName1, TStringArray names1, string current1, string path1, string customPath1, TStringArray prefixes1, string defaultSelection1) {
		LBLayoutConfigEntry entry = new LBLayoutConfigEntry();
		entry.Init(entryName1, names1, current1, path1, customPath1, prefixes1, defaultSelection1);
		return entry;
	}
	
	void Init(string entryName, TStringArray names, string current, string path, string customPath, TStringArray prefixes, string defaultSelection) {
		this.entryName = entryName;
		this.names = names;
		this.current = current;
		this.path = path;
		this.customPath = customPath;
		this.prefixes = prefixes;
		this.defaultSelection = defaultSelection;
	}
	
	bool HasName(string name) {
		return names.Find(name) != -1;
	}
	
	string GetPrefix(string name) {
		int index = names.Find(name);
		if (index < 0)
			return "";
		return prefixes.Get(index);
	}
	
	TStringArray FindAllLayoutStyles() {
		if (allLayoutsCache)
			return allLayoutsCache;
		TStringArray arr = new TStringArray();
		foreach (LayoutStyleEntry styles : LBGroupMainConfig.Get().layoutStyles) {
			if (styles.entryName == entryName) {
				arr = styles.styles;
			}
		}
		allLayoutsCache = arr;
		return arr;
	}
	
	string GetCurrentLayout(string name) {
		string prefix = GetPrefix(name);
		string path2 = path + "\\" + prefix + current + ".layout";
		if (FileExist(path2))
			return path2;
		if (customPath.Length() > 0) {
			if (customPath[customPath.Length() - 1] != "/" || customPath[customPath.Length() - 1] != "\\")
				customPath = customPath + "\\";
			path2 = customPath + prefix + current + ".layout";
			if (FileExist(path2))
				return path2;
		}
		return path + prefix + defaultSelection + ".layout";
	}
}