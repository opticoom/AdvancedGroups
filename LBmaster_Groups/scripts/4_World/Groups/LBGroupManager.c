class LBGroupManager {
	
	static ref TStringArray illegalFilenames = new TStringArray();
	static ref TStringArray illegalFilenamesNum = new TStringArray();

	static ref LBGroupManager g_LBGroupManager;
	
	static LBGroupManager Get() {
		if (!g_LBGroupManager) {
			g_LBGroupManager = Load();
		}
		return g_LBGroupManager;
	}
	
	static void Delete() {
		if (g_LBGroupManager)
			delete g_LBGroupManager;
	}
	
	static LBGroupManager Load() {
		LBGroupManager groupMgr = new LBGroupManager;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER)) {
			MakeDirectory(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER);
		}
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPSDELETED_FOLDER)) {
			MakeDirectory(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPSDELETED_FOLDER);
		}
		groupMgr.LoadAllGroups();
		Print("Loaded LBGroupManager");
		illegalFilenames.Clear();
		illegalFilenames.Insert("CON");
		illegalFilenames.Insert("PRN");
		illegalFilenames.Insert("AUX");
		illegalFilenames.Insert("NUL");
		illegalFilenamesNum.Clear();
		illegalFilenamesNum.Insert("COM");
		illegalFilenamesNum.Insert("LPT");
		illegalFilenames.Insert("LST");
		return groupMgr;
	}
	
	LBGroup GetGroupByHash(int hash) {
		return null;
	}
	
	void LoadAllGroups() {}
	
	bool GroupTagTaken(string tag, LBGroup exception = null) { return false; };
	
	void SaveGroup(LBGroup grp) {}
	void DeleteGroup(LBGroup group) {}
}