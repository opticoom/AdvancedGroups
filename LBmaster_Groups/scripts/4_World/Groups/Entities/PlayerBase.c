modded class PlayerBase {
	
	static ref array<PlayerBase> lb_player_list = new array<PlayerBase>();
	int identityIdHash = 0;
	
	void PlayerBase() {
		if (GetGame() && GetGame().IsClient()) {
			lb_player_list.Insert(this);
		}
		RegisterNetSyncVariableInt("identityIdHash");
	}
	
	void ~PlayerBase() {
		if (GetGame() && GetGame().IsClient()) {
			lb_player_list.RemoveItem(this);
		}
	}

	ref LBGroup lbgroup;
	ref LBGroupMember memberCache;
	
	LBGroup GetLBGroup() {
		return lbgroup;
	}
	
	LBGroupPermission GetPermission() {
		LBGroupMember member = GetMyGroupMarker();
		if (!member)
			return null;
		return LBGroupPermissions.Get().FindPermissionGroupByUID(member.permissionGroup);
	}
	
	LBGroupMember GetMyGroupMarker() {
		if (!GetLBGroup())
			return null;
		if (memberCache)
			return memberCache;
		string steamid = GetMySteamId();
		LBGroupMember member = GetLBGroup().GetMemberBySteamid(steamid);
		memberCache = member;
		return member;
	}
	
	static PlayerBase GetPlayerByIdentity(PlayerIdentity ident) {
		if (!ident)
			return null;
		int low, high;
		int id = ident.GetPlayerId();
		GetGame().GetPlayerNetworkIDByIdentityID(id, low, high);
		Object obj = GetGame().GetObjectByNetworkId(low, high);
		return PlayerBase.Cast(obj);
	}
	
	int GetMySubGroup() {
		LBGroupMember memb = GetMyGroupMarker();
		if (memb)
			return memb.currentSubgroup;
		return -1;
	}
	
	string GetMySteamId() {
		if (GetGame().IsServer()) {
			if (!GetIdentity())
				return "";
			return GetIdentity().GetPlainId();
		} else {
			MissionBaseWorld mission = MissionBaseWorld.Cast(GetGame().GetMission());
			if (!mission)
				return "";
			return mission.mySteamid;
		}
	}
	
	void SetLBGroup(LBGroup grp) {
		if (GetGame().IsClient()) {
			if (lbgroup)
				delete lbgroup;
		}
		lbgroup = grp;
		OnGroupChanged();
	}
	
	void OnGroupChanged() {
		memberCache = null;
		MissionBaseWorld mission = MissionBaseWorld.Cast(GetGame().GetMission());
		if (mission)
			mission.OnGroupChanged();
	}
	
	override void OnRPC(PlayerIdentity sender, int rpc_type, ParamsReadContext ctx) {
		super.OnRPC(sender, rpc_type, ctx);
		if (rpc_type == LBGroupRPCs.GROUP_SYNC) {
			bool has = false;
			if (!ctx.Read(has))
				return;
			if (!has) {
				SetLBGroup(null);
				return;
			}
			LBGroup grp = new LBGroup();
			if (!grp.ReadFromCtx(ctx)) {
				Print("Failed to receive Group from Server !");
				return;
			}
			Print("Successfully received Group from Server");
			SetLBGroup(grp);
			grp.InitMarkers();
		} else if (rpc_type == LBGroupRPCs.GROUP_INVITE) {
			Param1<string> shortnameParam;
			if (!ctx.Read(shortnameParam)) {
				Print("Failed to receive Shortname of Group Invite !");
				return;
			}
			MissionBaseWorld mission = MissionBaseWorld.Cast(GetGame().GetMission());
			mission.lastInvite = shortnameParam.param1;
			Print("Recevied Invite to Group " + mission.lastInvite);
		}
	}
	
	override void SetActionsRemoteTarget( out TInputActionMap InputActionMap) {
		super.SetActionsRemoteTarget(InputActionMap);
		AddAction(ActionInvitePlayerToGroup, InputActionMap);
	}

}