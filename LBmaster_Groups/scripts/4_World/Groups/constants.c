class LBGroupConstants {
	const string SAVE_PREFIX = "$profile:LBGroup/";
	const string SAVE_SUFFIX_GROUP_PERMISSIONS = "Permissions.json";
	const string SAVE_SUFFIX_MAIN_CONFIG = "MainConfig.json";
	const string SAVE_SUFFIX_GROUP_LEVELS = "Levels.json";
	const string SAVE_SUFFIX_STATIC_MARKER = "StaticMarkers.json";
	const string SAVE_SUFFIX_PRIVATE_MARKER = "PrivateMarkers.json";
	const string SAVE_SUFFIX_PRIVATE_MARKER_STATES = "PrivateMarkerDisplaystates.json";
	const string SAVE_SUFFIX_LAYOUT_MANAGER = "LayoutManager.json";
	const string SAVE_SUFFIX_COLOR_MANAGER = "ColorManager.json";
	const string SAVE_SUFFIX_POSITION_MANAGER = "PositionManager.json";
	const string SAVE_SUFFIX_MONEY_CONFIG = "ATMConfig.json";
	const string SAVE_SUFFIX_PLOTPOLE_CONFIG = "PlotpoleConfig.json";
	const string SAVE_SUFFIX_CLANCLOTHING_CONFIG = "ClanClothing.json";
	const string SAVE_SUFFIX_GROUPS_FOLDER = "Groups/";
	const string SAVE_SUFFIX_GROUPSDELETED_FOLDER = "Groups_Deleted/";
	
	const string LAYOUT_MAP_PATH = "LBmaster_Groups\\gui\\layouts\\mapmenu";
	const string LAYOUT_COMPASS_PATH = "LBmaster_Groups\\gui\\layouts\\compass";
	const string LAYOUT_PLAYERLIST_PATH = "LBmaster_Groups\\gui\\layouts\\playerlist";
}