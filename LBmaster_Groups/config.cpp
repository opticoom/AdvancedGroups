class CfgPatches
{
	class LBmaster_Groups
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data",
			"RPC_Scripts"
		};
	};
};
class CfgMods
{
	class LBmaster_Groups
	{
		dir="LBmaster_Groups";
		picture="";
		action="";
		hideName=0;
		hidePicture=1;
		name="Groups";
		author="LBmaster";
		credits="LBmaster";
		authorID="0";
		version="1.0";
		type="mod";
		inputs="LBmaster_Groups/inputsLBmaster.xml";
		dependencies[]=
		{
			"Game",
			"World",
			"Mission"
		};
		class defs
		{
			class widgetStyles
			{
				value="";
				files[]=
				{
					"LBmaster_Groups/gui/styles/customStyles.styles"
				};
			};
			class gameScriptModule
			{
				value="";
				files[]=
				{
					"LBmaster_Groups/scripts/3_Game"
				};
			};
			class worldScriptModule
			{
				value="";
				files[]=
				{
					"LBmaster_Groups/scripts/4_World"
				};
			};
			class missionScriptModule
			{
				value="";
				files[]=
				{
					"LBmaster_Groups/scripts/5_Mission"
				};
			};
		};
	};
};
class RscMapControl
{
	scaleMin=0.050000001;
	scaleMax=0.94999999;
	scaleDefault=0.94;
	ptsPerSquareSea=8;
	ptsPerSquareTxt=10;
	ptsPerSquareCLn=10;
	ptsPerSquareExp=10;
	ptsPerSquareCost=10;
	ptsPerSquareFor=9;
	ptsPerSquareForEdge=15;
	ptsPerSquareRoad=4;
	ptsPerSquareObj=15;
	maxSatelliteAlpha=1;
	alphaFadeStartScale=1;
	alphaFadeEndScale=1;
	userMapPath="\dz\gear\navigation\data\usermap";
	maxUserMapAlpha=0.1;
	alphaUserMapFadeStartScale=0.0099999998;
	alphaUserMapFadeEndScale=0.0099999998;
	showCountourInterval=1;
	colorLevels[]={0.64999998,0.60000002,0.44999999,0.94999999};
	colorSea[]={0.60000002,0.80000001,0.94999999,0.80000001};
	colorForest[]={0.36000001,0.77999997,0.079999998,0};
	colorRocks[]={0.5,0.5,0.5,0.40000001};
	colorCountlines[]={0.85000002,0.80000001,0.64999998,0.1};
	colorMainCountlines[]={0.44999999,0.40000001,0.25,0.2};
	colorCountlinesWater[]={0.25,0.40000001,0.5,0};
	colorMainCountlinesWater[]={0.25,0.40000001,0.5,0};
	colorPowerLines[]={0.1,0.1,0.1,1};
	colorRailWay[]={0.80000001,0.2,0,1};
	colorForestBorder[]={0.40000001,0.80000001,0,0.1};
	colorRocksBorder[]={0.5,0.5,0.5,0.1};
	colorOutside[]={1,1,1,1};
	colorTracks[]={0.77999997,0.66000003,0.34,1};
	colorRoads[]={0.69,0.43000001,0.23,1};
	colorMainRoads[]={0.52999997,0.34999999,0,1};
	colorTracksFill[]={0.95999998,0.91000003,0.60000002,1};
	colorRoadsFill[]={0.92000002,0.73000002,0.41,1};
	colorMainRoadsFill[]={0.83999997,0.61000001,0.20999999,1};
	colorGrid[]={0.80000001,0.69999999,0,0.5};
	colorGridMap[]={1,1,1,0.69999999};
	fontNames="gui\fonts\sdf_MetronBook24";
	sizeExNames=0.050000001;
	colorNames[]={1,1,1,1};
	fontGrid="gui\fonts\sdf_MetronBook24";
	sizeExGrid=0.02;
	fontLevel="gui\fonts\sdf_MetronBook24";
	sizeExLevel=0.0099999998;
	colorMountPoint[]={0.44999999,0.40000001,0.25,1};
	mapPointDensity=0.1;
	text="";
	fontLabel="gui\fonts\sdf_MetronBook24";
	fontInfo="gui\fonts\sdf_MetronBook24";
	class Legend
	{
		x=0.75;
		y=0.90000004;
		w=0.2;
		h=0.050000001;
		font="gui\fonts\sdf_MetronBook24";
		sizeEx=0.02;
		colorBackground[]={1,1,1,0};
		color[]={0,0,0,0};
	};
	class Bush
	{
		icon="\dz\gear\navigation\data\map_bush_ca.paa";
		color[]={0.40000001,0.80000001,0,0};
		size=14;
		importance="0.2 * 14 * 0.05";
		coefMin=0.25;
		coefMax=4;
	};
	class SmallTree
	{
		icon="\dz\gear\navigation\data\map_smalltree_ca.paa";
		color[]={0.40000001,0.80000001,0,0};
		size=12;
		importance="0.6 * 12 * 0.05";
		coefMin=0.25;
		coefMax=4;
	};
	class Tree
	{
		icon="\dz\gear\navigation\data\map_tree_ca.paa";
		color[]={0.40000001,0.80000001,0,0};
		size=12;
		importance="0.9 * 16 * 0.05";
		coefMin=0.25;
		coefMax=4;
	};
};
class CfgLocationTypes
{
	class Name
	{
		name="keypoint";
		drawStyle="name";
		texture="#(argb,1,1,1)color(1,1,1,1)";
		color[]={0.5,0.5,0,5,1};
		size=0;
		font="gui\fonts\sdf_MetronBook24";
		textSize=0.029999999;
		shadow=0;
		importance=1;
	};
	class NameIcon
	{
		name="keypoint";
		drawStyle="icon";
		texture="#(argb,1,1,1)color(1,1,1,1)";
		color[]={0.5,0.5,0,5,1};
		size=0;
		font="gui\fonts\sdf_MetronBook24";
		textSize=0.029999999;
		shadow=0;
		importance=1;
	};
	class Capital: Name
	{
		textSize=0.050000001;
		color[]={0.89999998,0.89999998,0.89999998,1};
		importance=6;
	};
	class City: Name
	{
		textSize=0.045000002;
		color[]={0.80000001,0.80000001,0.80000001,1};
		importance=5;
	};
	class Village: Name
	{
		textSize=0.035;
		color[]={0.69999999,0.69999999,0.69999999,1};
		importance=4;
	};
	class Local: Name
	{
		color[]={0.5,0.5,0.5,1};
		textSize=0.027000001;
		importance=2;
	};
	class Marine: Name
	{
		color[]={0.5,0.5,0.5,1};
		textSize=0.027000001;
		importance=2;
	};
	class Ruin: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_ruin_ca.paa";
		textSize=0.027000001;
		importance=2;
		size=7;
	};
	class Camp: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_camp_ca.paa";
		textSize=0.035;
		size=9;
	};
	class Hill: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_hill_ca.paa";
		size=9;
	};
	class ViewPoint: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_viewpoint_ca.paa";
		size=9;
	};
	class RockArea: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_rock_ca.paa";
		size=9;
	};
	class RailroadStation: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_station_ca.paa";
		size=9;
	};
	class IndustrialSite: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_factory_ca.paa";
		size=9;
	};
	class LocalOffice: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_govoffice_ca.paa";
		size=10;
	};
	class BorderCrossing: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_border_cross_ca.paa";
		size=9;
	};
	class VegetationBroadleaf: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_broadleaf_ca.paa";
		size=9;
	};
	class VegetationFir: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_fir_ca.paa";
		size=9;
	};
	class VegetationPalm: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_palm_ca.paa";
		size=9;
	};
	class VegetationVineyard: NameIcon
	{
		color[]={0.5,0.5,0.5,1};
		texture="\dz\gear\navigation\data\map_vineyard_ca.paa";
		size=9;
	};
};
class CfgWorlds
{
	class CAWorld;
	class ChernarusPlus: CAWorld
	{
		class Names
		{
			class Settlement_Chernogorsk
			{
				name="Chernogorsk";
				position[]={6778.6191,2321.7739};
				type="Capital";
			};
			class Settlement_Novodmitrovsk
			{
				name="Novodmitrovsk";
				position[]={11339.77,14391.05};
				type="Capital";
			};
			class Settlement_Novoselki
			{
				name="Novoselki";
				position[]={6115.9102,3256.3601};
				type="City";
			};
			class Settlement_Dubovo
			{
				name="Dubovo";
				position[]={6754.5098,3595.2571};
				type="City";
			};
			class Settlement_Vysotovo
			{
				name="Vysotovo";
				position[]={5701.2402,2556.1101};
				type="City";
			};
			class Settlement_Zelenogorsk
			{
				name="Zelenogorsk";
				position[]={2796,5166};
				type="City";
			};
			class Settlement_Berezino
			{
				name="Berezino";
				position[]={12372.383,9760.9814};
				type="City";
			};
			class Settlement_Elektrozavodsk
			{
				name="Elektrozavodsk";
				position[]={10286.68,1992.77};
				type="City";
			};
			class Settlement_NovayaPetrovka
			{
				name="NovayaPetrovka";
				position[]={3477.1689,12931.243};
				type="City";
			};
			class Settlement_Gorka
			{
				name="Gorka";
				position[]={9524,8945};
				type="City";
			};
			class Settlement_Solnechny
			{
				name="Solnechny";
				position[]={13487.724,6225.8838};
				type="City";
			};
			class Settlement_StarySobor
			{
				name="StarySobor";
				position[]={6114,7849};
				type="City";
			};
			class Settlement_Vybor
			{
				name="Vybor";
				position[]={3909.8311,9019.6143};
				type="City";
			};
			class Settlement_Severograd
			{
				name="Severograd";
				position[]={8168.4839,12573.602};
				type="City";
			};
			class Settlement_Bor
			{
				name="Bor";
				position[]={3316.406,4070.9661};
				type="Village";
			};
			class Settlement_Svetloyarsk
			{
				name="Svetloyarsk";
				position[]={13989.74,13263.78};
				type="City";
			};
			class Settlement_Krasnostav
			{
				name="Krasnostav";
				position[]={11203.167,12340.808};
				type="City";
			};
			class Settlement_ChernayaPolyana
			{
				name="ChernayaPolyana";
				position[]={12158.54,13770.22};
				type="City";
			};
			class Settlement_Polyana
			{
				name="Polyana";
				position[]={10794,8160};
				type="City";
			};
			class Settlement_Tulga
			{
				name="Tulga";
				position[]={12768,4384};
				type="Village";
			};
			class Settlement_Msta
			{
				name="Msta";
				position[]={11333.79,5421.3999};
				type="Village";
			};
			class Settlement_Staroye
			{
				name="Staroye";
				position[]={10212.52,5385.0498};
				type="Village";
			};
			class Settlement_Shakhovka
			{
				name="Shakhovka";
				position[]={9728.6396,6567.2998};
				type="Village";
			};
			class Settlement_Dolina
			{
				name="Dolina";
				position[]={11285.17,6633.5801};
				type="Village";
			};
			class Settlement_Orlovets
			{
				name="Orlovets";
				position[]={12250,7253};
				type="Village";
			};
			class Settlement_NovySobor
			{
				name="NovySobor";
				position[]={7123.6299,7774.6299};
				type="Village";
			};
			class Settlement_Kabanino
			{
				name="Kabanino";
				position[]={5300.71,8645.9004};
				type="Village";
			};
			class Settlement_Mogilevka
			{
				name="Mogilevka";
				position[]={7650.4102,5096.46};
				type="Village";
			};
			class Settlement_Nadezhdino
			{
				name="Nadezhdino";
				position[]={5894,4780};
				type="Village";
			};
			class Settlement_Guglovo
			{
				name="Guglovo";
				position[]={8445,6579};
				type="Village";
			};
			class Settlement_Kamyshovo
			{
				name="Kamyshovo";
				position[]={12170,3458};
				type="Village";
			};
			class Settlement_Pusta
			{
				name="Pusta";
				position[]={9197,3873};
				type="Village";
			};
			class Settlement_Dubrovka
			{
				name="Dubrovka";
				position[]={10324.299,9888.2373};
				type="Village";
			};
			class Settlement_VyshnayaDubrovka
			{
				name="VyshnayaDubrovka";
				position[]={9926.8232,10468.919};
				type="Village";
			};
			class Settlement_Khelm
			{
				name="Khelm";
				position[]={12329.27,10771.01};
				type="Village";
			};
			class Settlement_Olsha
			{
				name="Olsha";
				position[]={13402,12898};
				type="Village";
			};
			class Settlement_Gvozdno
			{
				name="Gvozdno";
				position[]={8644.751,11934.115};
				type="Village";
			};
			class Settlement_Grishino
			{
				name="Grishino";
				position[]={5980,10257};
				type="Village";
			};
			class Settlement_Rogovo
			{
				name="Rogovo";
				position[]={4802,6748};
				type="Village";
			};
			class Settlement_Pogorevka
			{
				name="Pogorevka";
				position[]={4445,6322};
				type="Village";
			};
			class Settlement_Pustoshka
			{
				name="Pustoshka";
				position[]={3137.9441,7890.9248};
				type="Village";
			};
			class Settlement_Kozlovka
			{
				name="Kozlovka";
				position[]={4402,4736};
				type="Village";
			};
			class Settlement_Karmanovka
			{
				name="Karmanovka";
				position[]={12700.628,14691.312};
				type="Village";
			};
			class Settlement_Balota
			{
				name="Balota";
				position[]={4527.5801,2465.3201};
				type="Village";
			};
			class Settlement_Komarovo
			{
				name="Komarovo";
				position[]={3702,2440};
				type="Village";
			};
			class Settlement_Kamenka
			{
				name="Kamenka";
				position[]={1981,2207};
				type="Village";
			};
			class Settlement_Myshkino
			{
				name="Myshkino";
				position[]={2048,7258};
				type="Village";
			};
			class Settlement_Pavlovo
			{
				name="Pavlovo";
				position[]={1693.98,3858.6499};
				type="Village";
			};
			class Settlement_Lopatino
			{
				name="Lopatino";
				position[]={2801,10029};
				type="Village";
			};
			class Settlement_Vyshnoye
			{
				name="Vyshnoye";
				position[]={6629,6051};
				type="Village";
			};
			class Settlement_Prigorodki
			{
				name="Prigorodki";
				position[]={7760,3336};
				type="Village";
			};
			class Settlement_Drozhino
			{
				name="Drozhino";
				position[]={3374,4861};
				type="Village";
			};
			class Settlement_Sosnovka
			{
				name="Sosnovka";
				position[]={2556,6307};
				type="Village";
			};
			class Settlement_Nizhneye
			{
				name="Nizhneye";
				position[]={12824.88,8097.7002};
				type="Village";
			};
			class Settlement_Pulkovo
			{
				name="Pulkovo";
				position[]={5015,5607};
				type="Village";
			};
			class Settlement_Berezhki
			{
				name="Berezhki";
				position[]={15020,13905};
				type="Village";
			};
			class Settlement_Turovo
			{
				name="Turovo";
				position[]={13630,14146};
				type="Village";
			};
			class Settlement_BelayaPolyana
			{
				name="BelayaPolyana";
				position[]={14166,14968};
				type="Village";
			};
			class Settlement_Dobroye
			{
				name="Dobroye";
				position[]={12996,15066};
				type="Village";
			};
			class Settlement_Nagornoye
			{
				name="Nagornoye";
				position[]={9286,14677};
				type="Village";
			};
			class Settlement_Svergino
			{
				name="Svergino";
				position[]={9493.5195,13875.27};
				type="Village";
			};
			class Settlement_Ratnoye
			{
				name="Ratnoye";
				position[]={6195.8779,12761.802};
				type="Village";
			};
			class Settlement_Kamensk
			{
				name="Kamensk";
				position[]={6748.7412,14430.046};
				type="Village";
			};
			class Settlement_Krasnoye
			{
				name="Krasnoye";
				position[]={6411.374,15029.961};
				type="Village";
			};
			class Settlement_StaryYar
			{
				name="StaryYar";
				position[]={4987.8599,14992.976};
				type="Village";
			};
			class Settlement_Polesovo
			{
				name="Polesovo";
				position[]={5955.7368,13558.046};
				type="Village";
			};
			class Settlement_Tisy
			{
				name="Tisy";
				position[]={3441.6499,14799.55};
				type="Village";
			};
			class Settlement_Topolniki
			{
				name="Topolniki";
				position[]={2913.7451,12366.357};
				type="Village";
			};
			class Settlement_Zaprudnoye
			{
				name="Zaprudnoye";
				position[]={5214,12792};
				type="Village";
			};
			class Settlement_Sinystok
			{
				name="Sinystok";
				position[]={1538.278,11904.578};
				type="Village";
			};
			class Settlement_Vavilovo
			{
				name="Vavilovo";
				position[]={2263,11103};
				type="Village";
			};
			class Settlement_Kumyrna
			{
				name="Kumyrna";
				position[]={8300,6054};
				type="Village";
			};
			class Settlement_Kalinovka
			{
				name="Kalinovka";
				position[]={7448,13406};
				type="Village";
			};
			class Settlement_Bogatyrka
			{
				name="Bogatyrka";
				position[]={1453.62,8870.9199};
				type="Village";
			};
			class Settlement_SvyatoyRomanSkiResort
			{
				name="SvyatoyRomanSkiResort";
				position[]={638,11667};
				type="Local";
			};
			class Settlement_SKVSChBiathlonArena
			{
				name="SKVSChBiathlonArena";
				position[]={357,11142};
				type="Local";
			};
			class Settlement_Zvir
			{
				name="Zvir";
				position[]={577.33002,5317.4199};
				type="Village";
			};
			class Settlement_Zabolotye
			{
				name="Zabolotye";
				position[]={1256.859,9993.2598};
				type="Village";
			};
			class Settlement_Galkino
			{
				name="Galkino";
				position[]={1102.6801,8798.2695};
				type="Local";
			};
			class Camp_Arsenovo
			{
				name="Arsenovoо";
				position[]={8509.6611,13918.628};
				type="Camp";
			};
			class Camp_Stroytel
			{
				name="Stroytel";
				position[]={7017.6299,4365.21};
				type="Camp";
			};
			class Camp_Romashka
			{
				name="Romashka";
				position[]={8177.6021,10980.289};
				type="Camp";
			};
			class Camp_Kometa
			{
				name="Kometa";
				position[]={10271.697,3558.228};
				type="Camp";
			};
			class Camp_Druzhba
			{
				name="Druzhba";
				position[]={11437,10695};
				type="Camp";
			};
			class Camp_Nadezhda
			{
				name="Nadezhda";
				position[]={7279.9678,7014.3501};
				type="Camp";
			};
			class Camp_YouthPioneer
			{
				name="YouthPioneer";
				position[]={11155.825,7074.1431};
				type="Camp";
			};
			class Camp_ProudChernarus
			{
				name="ProudChernarus";
				position[]={3199.6169,6174.9321};
				type="Camp";
			};
			class Camp_Shkolnik
			{
				name="Shkolnik";
				position[]={14856.503,14571.028};
				type="Camp";
			};
			class Camp_Pobeda
			{
				name="Pobeda";
				position[]={3703.3821,14876.384};
				type="Camp";
			};
			class Camp_Metalurg
			{
				name="Metalurg";
				position[]={1035.406,6677.3828};
				type="Camp";
			};
			class Hill_Zelenayagora
			{
				name="Zelenayagora";
				position[]={3767.1699,6010.54};
				type="Hill";
			};
			class Local_Dichina
			{
				name="Dichina";
				position[]={4618,7821};
				type="Local";
			};
			class Local_Novylug
			{
				name="Novylug";
				position[]={9251,11360};
				type="Local";
			};
			class Local_Staryeluga
			{
				name="Staryeluga";
				position[]={6919,8995};
				type="Local";
			};
			class Hill_Vysota
			{
				name="Vysota";
				position[]={6591.6299,3400};
				type="Hill";
			};
			class Hill_Kopyto
			{
				name="Kopyto";
				position[]={7871.479,3869.1021};
				type="Hill";
			};
			class Local_Grubyeskaly
			{
				name="Grubyeskaly";
				position[]={13115.06,11900};
				type="Local";
			};
			class Local_Chernyles
			{
				name="Chernyles";
				position[]={9031,7803};
				type="Local";
			};
			class Hill_Altar
			{
				name="Altar";
				position[]={8143.3999,9159.6797};
				type="Hill";
			};
			class Local_RadioZenit
			{
				name="RadioZenit";
				position[]={8080.0098,9341.6797};
				type="Local";
			};
			class Hill_PikKozlova
			{
				name="PikKozlova";
				position[]={8850.1699,2880.53};
				type="Hill";
			};
			class Local_Pustoykhrebet
			{
				name="Pustoykhrebet";
				position[]={10890,5665};
				type="Local";
			};
			class Hill_Bashnya
			{
				name="Hill_Bashnya";
				position[]={4178.27,11771.22};
				type="Hill";
			};
			class Hill_Veresnik
			{
				name="Veresnik";
				position[]={4440.1699,8070.54};
				type="Hill";
			};
			class Hill_Kurgan
			{
				name="Kurgan";
				position[]={3368.52,5296.8701};
				type="Hill";
			};
			class Hill_Kustryk
			{
				name="Kustryk";
				position[]={4912.8799,5063.4502};
				type="Hill";
			};
			class Hill_Vetryanayagora
			{
				name="Vetryanayagora";
				position[]={3892.74,4200.5898};
				type="Hill";
			};
			class Hill_Kalmyk
			{
				name="Kalmyk";
				position[]={6903.7798,4919.6602};
				type="Hill";
			};
			class Hill_PopIvan
			{
				name="PopIvan";
				position[]={6420.2598,6570.6602};
				type="Hill";
			};
			class Hill_Erbenka
			{
				name="Erbenka";
				position[]={2842.916,4014.811};
				type="Hill";
			};
			class Local_Lesnoykhrebet
			{
				name="Lesnoykhrebet";
				position[]={8122.77,7815.54};
				type="Local";
			};
			class Hill_Vysokiykamen
			{
				name="Vysokiykamen";
				position[]={8940.1904,4380.52};
				type="Hill";
			};
			class Hill_Dobry
			{
				name="Hill_Dobry";
				position[]={10552.9,3061.03};
				type="Hill";
			};
			class Hill_Baranchik
			{
				name="Baranchik";
				position[]={10153.147,6208.521};
				type="Hill";
			};
			class Hill_Malinovka
			{
				name="Malinovka";
				position[]={10897.7,7575.5898};
				type="Hill";
			};
			class Hill_Dubina
			{
				name="Dubina";
				position[]={11107.9,8474.8301};
				type="Hill";
			};
			class Hill_Klen
			{
				name="Klen";
				position[]={11473.511,11315.393};
				type="Hill";
			};
			class Hill_Chernayagora
			{
				name="Chernayagora";
				position[]={10280.816,12053.928};
				type="Hill";
			};
			class Ruin_Zolotar
			{
				name="Zolotar";
				position[]={10179,11998};
				type="Ruin";
			};
			class Hill_Ostry
			{
				name="Ostry";
				position[]={10792.747,12829.504};
				type="Hill";
			};
			class Hill_Olsha
			{
				name="Olsha";
				position[]={12975.7,12775.2};
				type="Hill";
			};
			class Marine_Tikhiyzaliv
			{
				name="Tikhiyzaliv";
				position[]={1221.49,2111.8899};
				type="Marine";
			};
			class Marine_Mutnyizaliv
			{
				name="Мутный залив";
				position[]={5735.479,1918.92};
				type="Marine";
			};
			class Marine_Chernyzaliv
			{
				name="Chernyzaliv";
				position[]={7599.0688,2486.5859};
				type="Marine";
			};
			class Marine_Zelenyzaliv
			{
				name="Zelenyzaliv";
				position[]={11227.019,2989.8379};
				type="Marine";
			};
			class Marine_Skalistyproliv
			{
				name="Skalistyproliv";
				position[]={13385.92,3613.9399};
				type="Marine";
			};
			class Marine_Nizhniyzaliv
			{
				name="Nizhniyzaliv";
				position[]={12989.3,8515.7598};
				type="Marine";
			};
			class Marine_ZalivGuba
			{
				name="ZalivGuba";
				position[]={14328.374,13136.732};
				type="Marine";
			};
			class Marine_Rify
			{
				name="Rify";
				position[]={13931.73,11288.61};
				type="Local";
			};
			class Marine_Ivovoyeozero
			{
				name="Ivovoyeozero";
				position[]={13248,11572};
				type="Marine";
			};
			class Marine_Chernoyeozero
			{
				name="Chernoyeozero";
				position[]={13381,12002};
				type="Marine";
			};
			class Marine_PlotinaTopolka
			{
				name="PlotinaTopolka";
				position[]={10231.33,3691.1499};
				type="Marine";
			};
			class Marine_PlotinaPobeda
			{
				name="PlotinaPobeda";
				position[]={9928.2939,13874.643};
				type="Marine";
			};
			class Marine_PlotinaTishina
			{
				name="PlotinaTishina";
				position[]={1150.8199,6432.4541};
				type="Marine";
			};
			class Marine_Ozerko
			{
				name="Ozerko";
				position[]={6777.6299,4492.4199};
				type="Marine";
			};
			class Marine_Prud
			{
				name="Prud";
				position[]={6610.98,9308.5703};
				type="Marine";
			};
			class Ruin_Chortovzamok
			{
				name="Chortovzamok";
				position[]={6883.2388,11501.288};
				type="Ruin";
			};
			class Ruin_Zub
			{
				name="Zub";
				position[]={6574.2798,5573.8501};
				type="Ruin";
			};
			class Ruin_Rog
			{
				name="Rog";
				position[]={11267.206,4293.0361};
				type="Ruin";
			};
			class Local_Grozovypereval
			{
				name="Grozovypereval";
				position[]={3318.1201,15250.55};
				type="Local";
			};
			class Local_Sosnovypereval
			{
				name="Sosnovypereval";
				position[]={2687.3999,6590.2798};
				type="Local";
			};
			class Local_PerevalOreshka
			{
				name="PerevalOreshka";
				position[]={9822.1914,6088.793};
				type="Local";
			};
			class Local_Turovskiypereval
			{
				name="Turovskiypereval";
				position[]={14674.241,14089.306};
				type="Local";
			};
			class Local_Tridoliny
			{
				name="Tridoliny";
				position[]={12764.47,5412.21};
				type="Local";
			};
			class Local_Grozovyedoly
			{
				name="Grozovyedoly";
				position[]={10403.982,14793.168};
				type="Local";
			};
			class Ruin_Klyuch
			{
				name="Klyuch";
				position[]={9282.5703,13476.67};
				type="Ruin";
			};
			class Hill_Lysayagora
			{
				name="Lysayagora";
				position[]={6722.459,14050.42};
				type="Hill";
			};
			class Marine_Glubokoyeozero
			{
				name="Glubokoyeozero";
				position[]={1692.5551,14821.565};
				type="Marine";
			};
			class Local_Skalka
			{
				name="Skalka";
				position[]={5758,14486};
				type="Local";
			};
			class Local_Vidy
			{
				name="Vidy";
				position[]={785.05402,14398.815};
				type="Local";
			};
			class Hill_Tumannyverkh
			{
				name="Tumannyverkh";
				position[]={331.224,12982.99};
				type="Hill";
			};
			class Local_Adamovka
			{
				name="Adamovka";
				position[]={5340.6602,11380.01};
				type="Local";
			};
			class Hill_Shishok
			{
				name="Hill_Shishok";
				position[]={3559.313,9422.2695};
				type="Hill";
			};
			class Settlement_Skalisty
			{
				name="Skalisty";
				position[]={13715.454,3118.78};
				type="Village";
			};
			class Ruin_Storozh
			{
				name="Storozh";
				position[]={2816.6011,1277.96};
				type="Ruin";
			};
			class Local_MysGolova
			{
				name="MysGolova";
				position[]={8286.0527,2405.5979};
				type="Local";
			};
			class Local_Drakon
			{
				name="Drakon";
				position[]={11191,2443};
				type="Local";
			};
			class Local_Otmel
			{
				name="Otmel";
				position[]={11581.25,3213.24};
				type="Local";
			};
			class Local_MysKrutoy
			{
				name="MysKrutoy";
				position[]={13578.19,3976.8201};
				type="Local";
			};
			class Hill_Tokarnya
			{
				name="Tokarnya";
				position[]={8890.4775,5672.5532};
				type="Hill";
			};
			class Hill_Ostrog
			{
				name="Ostrog";
				position[]={2655.7339,2264.6321};
				type="Hill";
			};
			class Local_Maryanka
			{
				name="Maryanka";
				position[]={2789,3386};
				type="Local";
			};
			class Local_Polonina
			{
				name="Polonina";
				position[]={1004.486,4242.8691};
				type="Local";
			};
			class Local_Kalinka
			{
				name="Kalinka";
				position[]={3346.5,11292.097};
				type="Local";
			};
			class Hill_Kikimora
			{
				name="Kikimora";
				position[]={1812,6200};
				type="Hill";
			};
			class Hill_BolshoyKotel
			{
				name="BolshoyKotel";
				position[]={714.64899,6520.4312};
				type="Hill";
			};
			class Hill_Simurg
			{
				name="Simurg";
				position[]={134,7569.9902};
				type="Hill";
			};
			class Hill_Volchiypik
			{
				name="Volchiypik";
				position[]={320.254,2719.269};
				type="Hill";
			};
			class Ruin_Krona
			{
				name="Krona";
				position[]={1493,9270.3398};
				type="Ruin";
			};
			class Local_TriKresta
			{
				name="TriKresta";
				position[]={301,9478};
				type="Local";
			};
			class Local_Rostoki
			{
				name="Rostoki";
				position[]={744.45001,8647.8604};
				type="Local";
			};
			class Marine_OrlyeOzero
			{
				name="OrlyeOzero";
				position[]={597,5990};
				type="Marine";
			};
			class Local_Makosh
			{
				name="Makosh";
				position[]={7849.7202,6480.1899};
				type="Local";
			};
			class Local_Klenovyipereval
			{
				name="Klenovyipereval";
				position[]={10862.702,11585.512};
				type="Local";
			};
			class Local_Zmeinykhrebet
			{
				name="Zmeinykhrebet";
				position[]={11446.17,13606.92};
				type="Local";
			};
			class Hill_Sokol
			{
				name="Sokol";
				position[]={12001,14813};
				type="Hill";
			};
			class Local_Krutyeskaly
			{
				name="Krutyeskaly";
				position[]={14880.63,13671.76};
				type="Local";
			};
			class Local_Bogat
			{
				name="Bogat";
				position[]={7058.0112,12023.571};
				type="Local";
			};
			class Local_Dubnik
			{
				name="Dubnik";
				position[]={3298.29,10330.89};
				type="Local";
			};
			class Hill_Viselitsa
			{
				name="Viselitsa";
				position[]={12722.157,7504.0498};
				type="Hill";
			};
			class Local_Dazhbog
			{
				name="Dazhbog";
				position[]={6581,8590};
				type="Hill";
			};
			class Marine_Verbnik
			{
				name="Verbnik";
				position[]={4414.8789,9103.002};
				type="Marine";
			};
			class Local_Medvezhilugi
			{
				name="Medvezhilugi";
				position[]={9641,13236};
				type="Local";
			};
			class Ruin_Voron
			{
				name="Voron";
				position[]={13517.93,3311.2451};
				type="Ruin";
			};
			class Ruin_Gnomovzamok
			{
				name="Gnomovzamok";
				position[]={7446.915,9097.1699};
				type="Ruin";
			};
			class Marine_Glaza
			{
				name="Glaza";
				position[]={7383,9320};
				type="Marine";
			};
			class Local_KarerKrasnayaZarya
			{
				name="KarerKrasnayaZarya";
				position[]={8565.3486,13418.684};
				type="Local";
			};
			class Local_Matveyevo
			{
				name="Matveyevo";
				position[]={4281,7380};
				type="Local";
			};
			class Local_Kotka
			{
				name="Kotka";
				position[]={5873.7002,6883.2402};
				type="Local";
			};
			class Local_Chernyeskaly
			{
				name="Chernyeskaly";
				position[]={3932.572,14599.496};
				type="Local";
			};
			class Hill_SvyatoyRoman
			{
				name="SvyatoyRoman";
				position[]={84.832001,11901.246};
				type="Hill";
			};
			class Hill_Koman
			{
				name="Koman";
				position[]={1660.652,7415.6611};
				type="Hill";
			};
			class Hill_Mayak
			{
				name="Mayak";
				position[]={12323.813,4553.1069};
				type="Hill";
			};
			class Local_MB_VMC
			{
				name="Military Base";
				position[]={4497,8291};
				type="Local";
			};
			class Local_MB_Tisy
			{
				name="Military Base";
				position[]={1570,14069};
				type="Local";
			};
			class Local_MB_Kamensk
			{
				name="Military Base";
				position[]={7838.812,14704.823};
				type="Local";
			};
			class Local_MB_Zeleno
			{
				name="Military Base";
				position[]={2410.375,5133.6812};
				type="Local";
			};
			class Local_MB_Pavlovo
			{
				name="Military Base";
				position[]={2016,3408};
				type="Local";
			};
			class Local_AF_Balota
			{
				name="Balota Airfield";
				position[]={5082.0308,2376.3279};
				type="Local";
			};
			class Local_AF_Krasno
			{
				name="Krasnostav Airfield";
				position[]={12100.681,12573.896};
				type="Local";
			};
			class Local_AF_Vybor
			{
				name="North West Airfield";
				position[]={4428,10179};
				type="Local";
			};
			class RailroadStation_Elektro
			{
				name="";
				position[]={10309.912,2092.2029};
				type="RailroadStation";
			};
			class RailroadStation_Prigorodki
			{
				name="";
				position[]={8056.0342,3264.908};
				type="RailroadStation";
			};
			class RailroadStation_Cherno
			{
				name="";
				position[]={6540.626,2638.7749};
				type="RailroadStation";
			};
			class RailroadStation_Balota
			{
				name="";
				position[]={4399.2588,2309.397};
				type="RailroadStation";
			};
			class RailroadStation_Komarovo
			{
				name="";
				position[]={3670.667,2397.5249};
				type="RailroadStation";
			};
			class RailroadStation_Kamenka
			{
				name="";
				position[]={1877.9351,2168.5591};
				type="RailroadStation";
			};
			class RailroadStation_Zeleno
			{
				name="";
				position[]={2485.2661,5214.6641};
				type="RailroadStation";
			};
			class RailroadStation_Vavilovo
			{
				name="";
				position[]={1991.5031,11289.488};
				type="RailroadStation";
			};
			class RailroadStation_Novaya
			{
				name="";
				position[]={3534.425,12537.095};
				type="RailroadStation";
			};
			class RailroadStation_Severograd
			{
				name="";
				position[]={7846.5811,12401.488};
				type="RailroadStation";
			};
			class RailroadStation_Novo
			{
				name="";
				position[]={11610.908,14133.702};
				type="RailroadStation";
			};
			class RailroadStation_Svetlo
			{
				name="";
				position[]={13970.473,13486.344};
				type="RailroadStation";
			};
			class RailroadStation_Berezino
			{
				name="";
				position[]={12989.844,10200.673};
				type="RailroadStation";
			};
			class RailroadStation_Solnich
			{
				name="";
				position[]={13146.109,7094.4722};
				type="RailroadStation";
			};
			class RailroadStation_Solnich2
			{
				name="";
				position[]={13356.109,6154.4722};
				type="RailroadStation";
			};
			class RailroadStation_Kamyshovo
			{
				name="";
				position[]={11970.969,3548.7229};
				type="RailroadStation";
			};
			class RailroadStation_Dobroye
			{
				name="";
				position[]={12860.658,15050.221};
				type="RailroadStation";
			};
			class LocalOffice_Novaya
			{
				name="";
				position[]={3469.1721,13135.165};
				type="LocalOffice";
			};
			class LocalOffice_Vybor
			{
				name="";
				position[]={3857.656,8946.1396};
				type="LocalOffice";
			};
			class LocalOffice_Sinys
			{
				name="";
				position[]={1439.08,12011.26};
				type="LocalOffice";
			};
			class LocalOffice_Vavil
			{
				name="";
				position[]={2220.8401,11075.83};
				type="LocalOffice";
			};
			class LocalOffice_Lopat
			{
				name="";
				position[]={2720.72,9978.1396};
				type="LocalOffice";
			};
			class LocalOffice_Pustosh
			{
				name="";
				position[]={3051.4399,7804.6802};
				type="LocalOffice";
			};
			class LocalOffice_Pogorev
			{
				name="";
				position[]={4480.71,6462};
				type="LocalOffice";
			};
			class LocalOffice_Kabanin
			{
				name="";
				position[]={5382,8589};
				type="LocalOffice";
			};
			class LocalOffice_Stary
			{
				name="";
				position[]={5971.0801,7743.3501};
				type="LocalOffice";
			};
			class LocalOffice_Novy
			{
				name="";
				position[]={7166.96,7526.0098};
				type="LocalOffice";
			};
			class LocalOffice_Grishino
			{
				name="";
				position[]={5983,10305};
				type="LocalOffice";
			};
			class LocalOffice_Severo
			{
				name="";
				position[]={7986,12732};
				type="LocalOffice";
			};
			class LocalOffice_Gorka
			{
				name="";
				position[]={9503,8805};
				type="LocalOffice";
			};
			class LocalOffice_Mogi
			{
				name="";
				position[]={7562,5149};
				type="LocalOffice";
			};
			class LocalOffice_Nadez
			{
				name="";
				position[]={5906,4844};
				type="LocalOffice";
			};
			class LocalOffice_Cherno
			{
				name="";
				position[]={6607,2418};
				type="LocalOffice";
			};
			class LocalOffice_Kozlov
			{
				name="";
				position[]={4394,4629};
				type="LocalOffice";
			};
			class LocalOffice_Komar
			{
				name="";
				position[]={3676,2501};
				type="LocalOffice";
			};
			class LocalOffice_Pavlovo
			{
				name="";
				position[]={1655,3851};
				type="LocalOffice";
			};
			class LocalOffice_Zeleno
			{
				name="";
				position[]={2854,5282};
				type="LocalOffice";
			};
			class LocalOffice_Elektro
			{
				name="";
				position[]={10212,2295};
				type="LocalOffice";
			};
			class LocalOffice_Kamys
			{
				name="";
				position[]={12155,3514};
				type="LocalOffice";
			};
			class LocalOffice_Staroye
			{
				name="";
				position[]={10106,5457};
				type="LocalOffice";
			};
			class LocalOffice_Dolina
			{
				name="";
				position[]={11217,6564};
				type="LocalOffice";
			};
			class LocalOffice_Solnich
			{
				name="";
				position[]={13383,6225};
				type="LocalOffice";
			};
			class LocalOffice_Polana
			{
				name="";
				position[]={10676,7989};
				type="LocalOffice";
			};
			class LocalOffice_Berezino
			{
				name="";
				position[]={12272,9474};
				type="LocalOffice";
			};
			class LocalOffice_Krasno
			{
				name="";
				position[]={11257,12211};
				type="LocalOffice";
			};
			class LocalOffice_Svetlo
			{
				name="";
				position[]={14013,13335};
				type="LocalOffice";
			};
			class LocalOffice_Cpol
			{
				name="";
				position[]={12110,13779};
				type="LocalOffice";
			};
			class LocalOffice_Novo
			{
				name="";
				position[]={11564,14742};
				type="LocalOffice";
			};
		};
	};
};
