# AdvancedGroups
###### [Wayback Archive](https://web.archive.org/web/20210524150111/https://github.com/Opticoom/AdvancedGroups)
###### [Archive.is ...waiting...](https://archive.is/wip/EmUTT)
###### [Github](https://github.com/Opticoom/AdvancedGroups)
###### [BitBucket](https://bitbucket.org/Opticoom/advancedgroups/src/main/)

## DayZ Addon 


Ever since the beginning of DayZ Standalone, Bohemia has been very strict on monitization. There are many restrictions that Bohemia put in place for both monitization on Addons and on servers. 

Some of the key things consist of:

##### Addon Monitization
- "Our games and tools are licensed for non-commercial use only."
- "You are allowed to put footage (screenshots and videos) of our games on websites (such as YouTube). You are only allowed to make money (e.g. advertisement revenues) from it if you add your own content to it that would be a sufficient amount of additional work/content to justify it."
- "Can I create/trade/sell my own items (for example clothing, posters, cards, plastic models.) based on content from Bohemia Interactive games?
No."

In the past Bohemia has taken down various addons. These addons such as the old Helicopter mod got taken down because under the scenes they were paid addons. They were on the steam workshop listed for free, but when you go to use the mod on any DayZ server instance you're server would crash and tell you to report to a discord server in order to purchace the server side addon. This mod got taken down early 2021 for this exact reason.

With that being said alot of addons have come out with this same concept. Bohemia may be slow, but these addons are illegaly monitizing its not a matter of if, it's a matter of when they get taken down. Due to this bad news, I will be releasing the source code of both the AdvancedGroups ServerMod and the client mod. Obviously the ServerMod was obfuscated at first but, let's be honest, there aren't that many obfuscation techniques for a programming language as obscure as Enforce Script. lol.

![alt text](https://raw.githubusercontent.com/Opticoom/AdvancedGroups/main/unknown.png)

**Free Software, Hell Yeah!**

