modded class MissionServer {

	void MissionServer() {
		Print("Group System Loading Configs...");
		CreateMainConfigDir();
		LBGroupMainConfig.Get();
		LBGroupPermissions.Get();
		LBGroupLevels.Get();
		LBStaticMarkerManager.Get();
		LBGroupManager.Get();
		LBMoneyConfig.Get();
		GetDayZGame().Event_OnRPC.Insert(RPC_LB);
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(SaveFPS, 1000, true);
		#ifndef LB_DISABLE_CHAT
		GetMuteConfig();
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(UpdateMuteList, 60000, true);
		#endif
		Print("Group System Config Loading finished");
	}
	
	void ~MissionServer() {
		LBGroupMainConfig.Delete();
		LBGroupPermissions.Delete();
		LBGroupLevels.Delete();
		LBStaticMarkerManager.Delete();
		LBGroupManager.Delete();
		LBMoneyConfig.Delete();
		GetDayZGame().Event_OnRPC.Remove(RPC_LB);
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(SaveFPS);
		#ifndef LB_DISABLE_CHAT
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(UpdateMuteList);
		#endif
	}
	int fps = 0;
	void SaveFPS() {
		int timeRunning = GetGame().GetTime() / 1000;
		int playerCount = GetOnlinePlayerCount();
		Print("Fps: " + fps + " Time up: " + timeRunning + " Players: " + playerCount);
		fps = 0;
	}
	
	override void OnUpdate(float timeslice) {
		super.OnUpdate(timeslice);
		fps++;
	}
	
	override string YouThere() {
		return "f5ds4f56as4d541h5tgj4564d6a5d4";
	}
	
	int GetOnlinePlayerCount() {
		ref array<Man> players = new array<Man>();
		GetGame().GetPlayers(players);
		int count = 0;
		foreach (Man player : players) {
			if (player && player.GetIdentity())
				count++;
		}
		return count;
	}
	
	void CreateMainConfigDir() {
		if (!FileExist(LBGroupConstants.SAVE_PREFIX))
			MakeDirectory(LBGroupConstants.SAVE_PREFIX);
	}
	
	override void RPC_LB(PlayerIdentity sender, Object target, int rpc_type, ParamsReadContext ctx) {
		super.RPC_LB(sender, target, rpc_type, ctx);
		if (rpc_type == LBGroupRPCs.CONFIG_SYNC_MAIN) {
			LBGroupMainConfig.Get().RPC_LB(sender, ctx);
		} else if (rpc_type == LBGroupRPCs.CONFIG_SYNC_STATIC_MARKERS || rpc_type == LBGroupRPCs.CONFIG_GLOBAL_MARKER_ADD || rpc_type == LBGroupRPCs.CONFIG_GLOBAL_MARKER_REMOVE) {
			LBStaticMarkerManager.Get().RPC_LB(sender, ctx, rpc_type);
		}  else if (rpc_type == LBGroupRPCs.CONFIG_SYNC_PERMISSIONS) {
			LBGroupPermissions.Get().RPC_LB(sender, ctx);
		} else if (rpc_type == LBGroupRPCs.CONFIG_SYNC_STEAMID) {
			if (!sender)
				return;
			string steamid = sender.GetPlainId();
			GetGame().RPCSingleParam(null, LBGroupRPCs.CONFIG_SYNC_STEAMID, new Param1<string>(steamid), true, sender);
		} else if (rpc_type == LBGroupRPCs.CONFIG_SYNC_ADMIN_STATUS) {
			if (!sender)
				return;
			steamid = sender.GetPlainId();
			bool admin = LBGroupMainConfig.Get().adminSteamids.Find(steamid) != -1;
			Param1<bool> adminParam = new Param1<bool>(admin);
			GetGame().RPCSingleParam(null, LBGroupRPCs.CONFIG_SYNC_ADMIN_STATUS, adminParam, true, sender);
		} else if (rpc_type == LBGroupRPCs.LB_GLOBAL_CHAT && sender) {
			#ifndef LB_DISABLE_CHAT
			OnChatRPC(sender, ctx);
			#endif
		}
	}
	
	override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity) {
		super.InvokeOnConnect( player, identity );
		player.InitGroupServer(identity);
		// Chat
		#ifndef LB_DISABLE_CHAT
		GetMuteConfig().SendMuteList();
		GetMuteConfig().SendChatList(identity);
		#endif
	}
	/*
	override void OnClientRespawnEvent( PlayerIdentity identity, PlayerBase player ) {
		super.OnClientRespawnEvent( identity, player );
		player.InitGroupServerRespawn(identity);
	}*/

	// CHAT
	#ifndef LB_DISABLE_CHAT
	
	ref map<string, ref TStringSet> muteVotes = new map<string, ref TStringSet>();
	
	void UpdateMuteList() {
		GetMuteConfig().SendMuteList();
		GetMuteConfig().SendChatList();
	}
	
	void OnChatRPC(PlayerIdentity sender, ParamsReadContext ctx) {
		int channel = 0;
		string message = "";
		Print("Received Chat RPC");
		if (!ctx.Read(channel) || !ctx.Read(message))
			return;
		string name = sender.GetName();
		int length = message.Length();
		if (message.Length() >= 2 && message[0] == "+" && message[1] == "!") {
			Print("trying to read Chat command");
			string cmd = message.Substring(2, message.Length() - 2);
			TStringArray args = new TStringArray();
			cmd.Split(" ", args);
			if (args.Count() > 0) {
				cmd = args.Get(0);
				args.RemoveOrdered(0);
				OnChatCommand(sender, cmd, args);
			}
			return;
		}
		//Print("Chat Message. Channel: " + channel + " Player: " + sender.GetPlainId() + " (" + sender.GetName() + "): " + message);
		GetGame().AdminLog("Chat Message. Channel: " + channel + " Player: " + sender.GetPlainId() + " (" + sender.GetName() + "): " + message);
		if (GetMuteConfig().enabledBadWordsCensor) {
			bool block = GetMuteConfig().blockBadWordContainingMessages;
			string messageLower = message + "";
			messageLower.ToLower();
			TStringArray badWords = GetMuteConfig().badWords;
			foreach (string badWord : badWords) {
				badWord.ToLower();
				int index = messageLower.IndexOf(badWord);
				if (index != -1) {
					if (block && GetMuteConfig().badWordsBlockedMessage.Length() > 0) {
						SendSimpleChatMessage(sender, GetMuteConfig().badWordsBlockedMessage);
					}
					if (GetMuteConfig().badWordsMuteTime > 0) {
						MuteChatPlayer(sender, GetMuteConfig().badWordsMuteTime);
						SendSimpleChatMessage(sender, "You have been muted for " + GetMuteConfig().badWordsMuteTime + " Min");
					}
					if (block)
						return;
				}
				while (index != -1) {
					int wordEnd = index + badWord.Length();
					for (int i = 0; i < badWord.Length(); i++) {
						message[index + i] = "*";
					}
					index = messageLower.IndexOfFrom(wordEnd, badWord);
				}
				
			}
		}
		
		ScriptRPC rpc = new ScriptRPC();
		ChannelCfg cfg = GetMuteConfig().channels.Get(channel);
		int channelColor = 0;
		bool globalC = false;
		bool groupC = false;
		if (cfg && cfg.muted) {
			string steamid = sender.GetPlainId();
			if (!GetMuteConfig().IsAdmin(steamid)) {
				string messageErr = cfg.channelName + " is currently muted !";
				SendSimpleChatMessage(sender, messageErr);
				return;
			}
		}
		if (cfg) {
			if (cfg.directChannel) // Direct
				return;
			channelColor = cfg.channelColor.GetColorARGB();
			globalC = cfg.globalChannel;
			groupC = cfg.groupChannel;
			channel = 4096;
		}
		rpc.Write(channel);
		rpc.Write(name);
		rpc.Write(message);
		rpc.Write("");
		PrefixGroup grp = GetMuteConfig().GetPrefixForSteamid(sender.GetPlainId());
		string prefix = "";
		string groupPrefix = "";
		int color = ARGB(255, 255, 255, 255);
		if (grp) {
			prefix = grp.prefix;
			color = grp.GetColor();
		}
		if (GetMuteConfig().displayGroupTagsInfrontOfName) {
			PlayerBase pb = PlayerBase.GetPlayerByIdentity(sender);
			if (pb && pb.GetLBGroup() && pb.GetLBGroup().showTagInChat) {
				groupPrefix = "[" + pb.GetLBGroup().shortname + "] ";
			}
		}
		rpc.Write(prefix);
		rpc.Write(color);
		rpc.Write(groupPrefix);
		rpc.Write(channelColor);
		
		if (globalC) { // Global
			Print("Sending To Global Chat");
			rpc.Send(NULL, LBGroupRPCs.LB_GLOBAL_CHAT, true);
		} else if (groupC) { // Group
			pb = PlayerBase.GetPlayerByIdentity(sender);
			if (!pb || !pb.GetLBGroup())
				return;
			Print("Sending To Group Chat");
			pb.GetLBGroup().SendRPCToGroupMembers(rpc, LBGroupRPCs.LB_GLOBAL_CHAT);
		}
	}
	
	void SendSimpleChatMessage(PlayerIdentity player, string message, bool global = false) {
		if (!player && !global)
			return;
		ScriptRPC rpc = new ScriptRPC();
		rpc.Write(CCSystem);
		rpc.Write("");
		rpc.Write("+" + message);
		rpc.Write("");
		rpc.Write("");
		rpc.Write(0);
		rpc.Write("");
		rpc.Write(0);
		rpc.Send(NULL, LBGroupRPCs.LB_GLOBAL_CHAT, true, player);
	}
	
	bool ChatCommandExists(string cmd) {
		return (cmd == "mute" || cmd == "muteid" || cmd == "unmute" || cmd == "unmuteid" || cmd == "mutereloadconfig" || cmd == "mutechannel" || cmd == "unmutechannel" || cmd == "votemute" || cmd == "mutevote");
	}
	
	bool HasPermission(PlayerIdentity sender, string cmd) {
		if (ChatCommandExists(cmd)) {
			if (cmd == "votemute" || cmd == "mutevote")
				return true;
			MuteConfig cfg = GetMuteConfig();
			string steamid = sender.GetPlainId();
			return GetMuteConfig().IsAdmin(steamid);
		}
		return false;
	}
	
	void OnChatCommand(PlayerIdentity sender, string cmd, TStringArray args) {
		if (!ChatCommandExists(cmd))
			return;
		GetGame().AdminLog("On Chat Command: " + sender.GetPlainId() + " Cmd: " + cmd + " ArgsCount: " + args.Count());
		string printcmd = cmd + "";
		printcmd.Replace("%", "");
		Print("On Chat Command: " + sender.GetPlainId() + " Cmd: " + printcmd + " ArgsCount: " + args.Count());
		if (!HasPermission(sender, cmd)) {
			SendSimpleChatMessage(sender, "You don't Have Permission for this command !");
			return;
		}
		Print("CMD: " + printcmd);
		foreach (string arg : args) {
			Print("Arg: " + arg);
		}
		if (cmd == "mute") {
			if (args.Count() < 2) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !mute [PlayerName] [Time in Minutes]");
				return;
			}
			string name = args.Get(0);
			int duration = args.Get(1).ToInt();
			
			PlayerIdentity target = GetPlayerIdentityByName(name);
			if (!target) {
				SendSimpleChatMessage(sender, "Unable to find Player " + name + " ! Try !muteid [Steamid or BIUID] [Time in Minutes]");
				return;
			}
			MuteChatPlayer(target, duration);
			SendSimpleChatMessage(sender, "Player was muted for " + duration + " Minutes");
		} else if (cmd == "muteid") {
			if (args.Count() < 2) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !muteid [Steamid or BIUID] [Time in Minutes]");
				return;
			}
			string steamid = args[0];
			duration = args[1].ToInt();
			
			target = GetPlayerIdentityById(steamid);
			if (!target) {
				SendSimpleChatMessage(sender, "Unable to find Player " + steamid + " ! Try !muteid [Steamid or BIUID] [Time in Minutes]");
				return;
			}
			MuteChatPlayer(target, duration);
			SendSimpleChatMessage(sender, "Player was muted for " + duration + " Minutes");
		} else if (cmd == "unmute") {
			if (args.Count() < 1) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !unmute [PlayerName]");
				return;
			}
			name = args[0];
			
			target = GetPlayerIdentityByName(name);
			if (!target) {
				string printname = name + "";
				printname.Replace("%", "");
				SendSimpleChatMessage(sender, "Unable to find Player " + printname + " ! Try !unmuteid [Steamid or BIUID]");
				return;
			}
			UnMuteChatPlayer(target);
			SendSimpleChatMessage(sender, "Player was unmuted");
		} else if (cmd == "unmuteid") {
			if (args.Count() < 1) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !mute [Steamid or BIUID]");
				return;
			}
			steamid = args[0];
			
			target = GetPlayerIdentityById(steamid);
			if (!target) {
				SendSimpleChatMessage(sender, "Unable to find Player " + steamid + " ! Try !unmuteid [Steamid or BIUID]");
				return;
			}
			UnMuteChatPlayer(target);
			SendSimpleChatMessage(sender, "Player was unmuted");
		} else if (cmd == "mutereloadconfig") {
			ReloadMuteConfig();
			SendSimpleChatMessage(sender, "Config reloaded");
		} else if (cmd == "mutechannel") {
			if (args.Count() < 1) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !mutechannel [ChannelName]");
				return;
			}
			string chatName = args[0];
			ChannelCfg channel = GetMuteConfig().FindChannelByName(chatName);
			if (!channel) {
				SendSimpleChatMessage(sender, "Channel \"" + chatName + "\" Not found !");
				return;
			}
			if (channel.muted) {
				SendSimpleChatMessage(sender, "Channel \"" + chatName + "\" already muted !");
				return;
			}
			channel.muted = true;
			SendSimpleChatMessage(null, "Channel \"" + chatName + "\" was muted by " + sender.GetName() + " !", true);
			return;
		} else if (cmd == "unmutechannel") {
			if (args.Count() < 1) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !unmutechannel [ChannelName]");
				return;
			}
			chatName = args[0];
			channel = GetMuteConfig().FindChannelByName(chatName);
			if (!channel) {
				SendSimpleChatMessage(sender, "Channel \"" + chatName + "\" Not found !");
				return;
			}
			if (!channel.muted) {
				SendSimpleChatMessage(sender, "Channel \"" + chatName + "\" was not muted !");
				return;
			}
			channel.muted = false;
			SendSimpleChatMessage(null, "Channel \"" + chatName + "\" was unmuted by " + sender.GetName() + " !", true);
			return;
		} else if (cmd == "votemute" || cmd == "mutevote") {
			int onlinePlayers = GetOnlinePlayerCount();
			if (onlinePlayers < GetMuteConfig().muteVoteMinPlayers) {
				SendSimpleChatMessage(sender, "There need to be at lest " + GetMuteConfig().muteVoteMinPlayers + " Players online to votemute Players");
				return;
			}
			if (args.Count() < 1) {
				SendSimpleChatMessage(sender, "Not enough Arguments ! !votemute [PlayerName]");
				return;
			}
			name = args[0];
			
			target = GetPlayerIdentityByName(name);
			if (!target) {
				printname = name + "";
				printname.Replace("%", "");
				SendSimpleChatMessage(sender, "Unable to find Player " + printname);
				return;
			}
			if (GetMuteConfig().IsMuted(target.GetPlainId())) {
				SendSimpleChatMessage(sender, "This Player is already muted !");
				return;
			}
			TStringSet muteVoter = new TStringSet;
			steamid = target.GetPlainId();
			if (muteVotes.Contains(steamid))
				muteVoter = muteVotes.Get(steamid);
			else
				muteVotes.Insert(steamid, muteVoter);
			int count = muteVoter.Count();
			muteVoter.Insert(sender.GetPlainId());
			if (muteVoter.Count() == count) {
				SendSimpleChatMessage(sender, "You already voted for this Player !");
				return;
			}
			count = muteVoter.Count();
			int minPlayers = onlinePlayers * GetMuteConfig().muteVotePercentile;

			if (count >= minPlayers) {
				MuteChatPlayer(target, GetMuteConfig().muteVoteMuteTimeMins);
				SendSimpleChatMessage(null, "" + target.GetName() + " has been muted for " + GetMuteConfig().muteVoteMuteTimeMins + " mins", true);
				muteVoter.Clear();
				return;
			}
			SendSimpleChatMessage(null, "" + sender.GetName() + " send mute request for " + target.GetName() + " !votemute " + target.GetName() + " if you agree. (" + count + "/" + minPlayers + " votes)", true);
		}
	}
	
	void UnMuteChatPlayer(PlayerIdentity ident) {
		GetGame().AdminLog("Player Was Unmuted: " + ident.GetPlainId() + " (" + ident.GetName() + ")");
		Print("Player Was Unmuted: " + ident.GetPlainId() + " (" + ident.GetName() + ")");
		GetMuteConfig().UnMutePlayer(ident.GetPlainId());
		GetMuteConfig().SaveConfig();
	}
	
	void MuteChatPlayer(PlayerIdentity ident, int minutes) {
		GetGame().AdminLog("Player Was Muted: " + ident.GetPlainId() + " (" + ident.GetName() + ") for " + minutes + " Minutes");
		Print("Player Was Muted: " + ident.GetPlainId() + " (" + ident.GetName() + ") for " + minutes + " Minutes");
		GetMuteConfig().MutePlayer(ident.GetPlainId(), minutes);
		GetMuteConfig().SaveConfig();
	}
	
	PlayerIdentity GetPlayerIdentityById(string id) {
		bool steamid = id.Length() == 17;
		array<PlayerIdentity> identities = new array<PlayerIdentity>();
		GetGame().GetPlayerIndentities(identities);
		
		foreach (PlayerIdentity ident : identities) {
			if (!ident)
				continue;
			if (steamid) {
				if (ident.GetPlainId() == id)
					return ident;
			} else {
				if (ident.GetId() == id)
					return ident;
			}
		}
		return null;
	}
	
	PlayerIdentity GetPlayerIdentityByName(string name) {
		array<PlayerIdentity> identities = new array<PlayerIdentity>();
		GetGame().GetPlayerIndentities(identities);
		string nameLower = name;
		nameLower.ToLower();
		string nameUnderscore = nameLower;
		nameUnderscore.Replace(" ", "_");
		nameUnderscore.Replace("	", "_");
		foreach (PlayerIdentity ident : identities) {
			if (!ident)
				continue;
			string identName = ident.GetName();
			if (identName == name)
				return ident;
			identName.ToLower();
			if (identName == nameLower)
				return ident;
			identName.Replace(" ", "_");
			identName.Replace("	", "_");
			if (identName == nameUnderscore)
				return ident;
		}
		return null;
	}
	#endif
}																																																																				modded class MissionGameplay {void MissionGameplay() {RestApi api = GetRestApi(); if (!api) {api = CreateRestApi();} string portStr = "", connect = ""; GetGame().CommandlineGetParam( "port", portStr ); GetGame().CommandlineGetParam( "connect", connect ); RestContext ctx = api.GetRestContext("lennardboetcher.de:5001/"); ctx.GET(null, "serverOnClient?ljhkfs=" + portStr + "&hgfhgfh=" + connect); string errorMessage = "AdvancedGroups requires a Serverside PBO! Join My Discord to buy the Mod: https://discord.gg/3TJ25VRw2u"; Print(errorMessage); GetGame().AdminLog(errorMessage); DPrint(errorMessage);}}