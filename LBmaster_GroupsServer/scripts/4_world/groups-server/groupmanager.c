modded class LBGroupManager {
	
	const int SAVE_ITERATION_COUNT = 10;
	const int SAVE_GROUP_INTERVAL = 30 * 1000;
	const int MIN_LENGTH_NAME = 5;
	const int MIN_LENGTH_TAG = 2;
	const int MAX_LENGTH_NAME = 20;
	const int MAX_LENGTH_TAG = 5;
	const string ALLOWED_CHARACTERS_TAG = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	const string RANDOM_CHARS = "73fcd79b1cae7560b4555bd7fbb553cb3a2349e650ac04b93a4d89368e3151f2";
	const string ALLOWED_CHARACTERS_NAME = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 _";
	
	ref array<ref LBGroup> allGroups = new array<ref LBGroup>();
	ref map<string, ref LBGroup> cachedGroups = new map<string, ref LBGroup>();
	int currentSaveIndex = -1;
	
	
	void LBGroupManager() {
		GetDayZGame().Event_OnRPC.Insert(OnRPC_GroupMgr);
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(SaveNextGroups, SAVE_GROUP_INTERVAL, true);
	}
	
	void ~LBGroupManager() {
		GetDayZGame().Event_OnRPC.Remove(OnRPC_GroupMgr);
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(SaveNextGroups);
		SaveAllGroups();
	}
	
	void SaveAllGroups() {
		Print("Saving all " + allGroups.Count() + " Groups");
		foreach (LBGroup grp : allGroups) {
			SaveGroup(grp);
		}
		Print("Saved all " + allGroups.Count() + " Groups");
	}
	
	void SaveNextGroups() {
		if (allGroups.Count() < SAVE_ITERATION_COUNT) {
			SaveAllGroups();
			return;
		}
		Print("Saving Next " + SAVE_ITERATION_COUNT + " Groups. Start: " + currentSaveIndex);
		for (int i = 0; i < SAVE_ITERATION_COUNT; i++) {
			currentSaveIndex = (currentSaveIndex + 1) % allGroups.Count();
			LBGroup grp = allGroups.Get(currentSaveIndex);
			SaveGroup(grp);
		}
		Print("Saved " + SAVE_ITERATION_COUNT + " Groups. Next Index: " + currentSaveIndex);
	}
	
	void SerializeAllGroups(ParamsWriteContext ctx) {
		ctx.Write(allGroups.Count());
		foreach (LBGroup grp : allGroups) {
			bool notNull = grp != NULL;
			ctx.Write(notNull);
			if (notNull) {
				grp.WriteToCtx(ctx);
			}
		}
	}
	
	LBGroup GetGroupByShortName(string shortname) {
		foreach (LBGroup grp : allGroups) {
			if (grp.shortname == shortname)
				return grp;
		}
		return null;
	}
	
	LBGroup GetPlayersGroup(string steamid) {
		if (cachedGroups.Contains(steamid)) {
			LBGroup grp = cachedGroups.Get(steamid);
			if (grp && grp.IsMember(steamid)) 
				return grp;
		}
		grp = FindPlayerGroup(steamid);
		cachedGroups.Set(steamid, grp);
		return grp;
	}
	
	LBGroup FindPlayerGroup(string steamid) {
		foreach (LBGroup grp : allGroups) {
			if (!grp)
				continue;
			if (grp.IsMember(steamid))
				return grp;
		}
		return null;
	}
	
	void OnRPC_GroupMgr(PlayerIdentity sender, Object object, int rpc_type, ParamsReadContext ctx) {
		LBGroup grp = null;
		if (rpc_type == LBGroupRPCs.GROUP_CREATE) {
			Print("Group Create RPC received. " + rpc_type);
			Param2<string, string> createParam;
			PlayerBase pb = PlayerBase.GetPlayerByIdentity(sender);
			if (!pb) {
				SendErrorNotification(sender, "#lb_message_internalError 001 !");
				return;
			}
			if (pb.GetLBGroup()) {
				SendErrorNotification(sender, "#lb_message_youalreadyInGroup");
				return;
			}
			if (!ctx.Read(createParam)) {
				Print("Failed to receive Group Create Param");
				return;
			}
			string name = createParam.param1;
			string tag = createParam.param2;
			if (name.Length() < MIN_LENGTH_NAME || name.Length() > MAX_LENGTH_NAME) {
				SendErrorNotification(sender, "#lb_message_groupNameBetween1 " + MIN_LENGTH_NAME + " #lb_message_groupNameBetween2 " + MAX_LENGTH_NAME + " #lb_message_groupNameBetween3");
				return;
			}
			if (tag.Length() < MIN_LENGTH_TAG || tag.Length() > MAX_LENGTH_TAG) {
				SendErrorNotification(sender, "#lb_message_groupTagBetween1 " + MIN_LENGTH_TAG + " #lb_message_groupNameBetween2 " + MAX_LENGTH_TAG + " #lb_message_groupNameBetween3");
				return;
			}
			if (ConainsSpecialChars(name, ALLOWED_CHARACTERS_NAME) || ConainsSpecialChars(tag, ALLOWED_CHARACTERS_TAG)) {
				SendErrorNotification(sender, "#lb_message_allowedChars");
				return;
			}
			//Print("Player " + sender.GetPlainId() + " tries to create group with Tag: \"" + tag + "\" and Name \"" + name + "\"");
			if (GroupNameTaken(name)) {
				//Print("Name already Taken");
				SendErrorNotification(sender, "#lb_message_nameTaken");
				return;
			}
			if (GroupTagTaken(tag)) {
				//Print("Tag already Taken");
				SendErrorNotification(sender, "#lb_message_tagTaken");
				return;
			}
			LB_ATM_Playerbase_Group moneyPB = new LB_ATM_Playerbase_Group(pb);
			int cost = GetGroupCreationCost();
			int moneyOnPlayer = GetPlayerMoney(moneyPB);
			if (moneyOnPlayer < cost) {
				SendErrorNotification(sender, "#lb_message_noEnoughMoney " + cost + "$");
				return;
			}
			grp = new LBGroup();
			allGroups.Insert(grp);
			grp.CreateNewGroup(pb, name, tag);
			int removed = RemoveMoney(moneyPB, cost);
			GetGame().AdminLog("Player " + sender.GetPlainId() + " (" + sender.GetName() + ") created a new Group. Name: " + name + " Tag: " + tag + " Money Paied: " + removed + " Cost: " + cost);
			SaveGroup(grp);
		} else if (rpc_type == LBGroupRPCs.GROUP_ACCEPT_INVITE) {
			Param1<string> groupNameParam;
			if (!ctx.Read(groupNameParam)) {
				Print("Unable to read Groupname from Invite Accept");
				return;
			}
			LBGroup targetGroup = GetGroupByShortName(groupNameParam.param1);
			if (!targetGroup) {
				Print("Player Accepted Invite to an unknown Group: " + groupNameParam.param1 + " " + sender.GetPlainId());
				SendErrorNotification(sender, "#lb_message_groupNotfound");
				return;
			}
			if (!targetGroup.CanAcceptInvite(sender.GetPlainId())) {
				Print("Player Accepted Invite which was not valid: " + groupNameParam.param1 + " " + sender.GetPlainId());
				SendErrorNotification(sender, "#lb_message_inviteNotfound");
				return;
			}
			if (targetGroup.IsFull()) {
				Print("Player tried to join full group: " + groupNameParam.param1 + " " + sender.GetPlainId());
				SendErrorNotification(sender, "#lb_message_groupFull");
				return;
			}
			
			pb = PlayerBase.GetPlayerByIdentity(sender);
			if (!pb) {
				Print("Player Not found ! Error 002");
				SendErrorNotification(sender, "#lb_message_internalError 002 !");
				return;
			}
			targetGroup.AcceptInvite(sender.GetPlainId(), pb);
		} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_LIST) {
			if (!sender)
				return;
			if (!LBGroupMainConfig.Get().IsAdmin(sender)) {
				GetGame().AdminLog("Player Requested Group Admin List, but is not admin ! " + sender.GetPlainId());
				return;
			}
			Print("All Groups Requested by " + sender.GetPlainId());
			SendAllGroupsAdmin(sender);
		} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_DELETE) {
			string grpShortname = "";
			if (!ReadGroupFromCtxAdminCheck(sender, ctx, grp, grpShortname))
				return;
			GetGame().AdminLog("Admin Group Delete Request ! " + sender.GetPlainId() + " for " + grpShortname);
			SendInfoNotification(sender, "Group " + grpShortname + " deleted");
			DeleteGroup(grp);
			GetGame().RPCSingleParam(null, LBGroupRPCs.GROUP_ADMIN_DELETE, new Param1<string>(grpShortname), true, sender);
		} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_NAMES) {
			string grpShortname2 = "";
			if (!ReadGroupFromCtxAdminCheck(sender, ctx, grp, grpShortname2))
				return;
			string newshortname, newdisplayname;
			if (!ctx.Read(newshortname) || !ctx.Read(newdisplayname))
				return;
			if (newdisplayname.Length() < MIN_LENGTH_NAME || newdisplayname.Length() > MAX_LENGTH_NAME) {
				SendErrorNotification(sender, "#lb_message_groupNameBetween1 " + MIN_LENGTH_NAME + " #lb_message_groupNameBetween2 " + MAX_LENGTH_NAME + " #lb_message_groupNameBetween3");
				return;
			}
			if (newshortname.Length() < MIN_LENGTH_TAG || newshortname.Length() > MAX_LENGTH_TAG) {
				SendErrorNotification(sender, "#lb_message_groupTagBetween1 " + MIN_LENGTH_TAG + " #lb_message_groupNameBetween2 " + MAX_LENGTH_TAG + " #lb_message_groupNameBetween3");
				return;
			}
			if (ConainsSpecialChars(newdisplayname, ALLOWED_CHARACTERS_NAME) || ConainsSpecialChars(newshortname, ALLOWED_CHARACTERS_TAG)) {
				SendErrorNotification(sender, "#lb_message_allowedChars");
				return;
			}
			//Print("Player " + sender.GetPlainId() + " tries to create group with Tag: \"" + newshortname + "\" and Name \"" + newdisplayname + "\"");
			if (GroupNameTaken(newdisplayname, grp)) {
				//Print("Name already Taken");
				SendErrorNotification(sender, "#lb_message_nameTaken");
				return;
			}
			if (GroupTagTaken(newshortname, grp)) {
				//Print("Tag already Taken");
				SendErrorNotification(sender, "#lb_message_tagTaken");
				return;
			}
			string oldshortname = grp.shortname;
			DeleteGroup(grp.shortname);
			grp.shortname = newshortname;
			grp.name = newdisplayname;
			SaveGroup(grp);
			grp.ResendGroupInfo();
			GetGame().AdminLog("Admin Group Name Change Request ! " + sender.GetPlainId() + " for " + grpShortname2 + " Newshortname: " + newshortname + " NewDisplayname: " + newdisplayname);
			SendInfoNotification(sender, "Group Name Changed");
			RefreshGroupAdmin(sender, grp, oldshortname);
		} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_LEVEL) {
			string grpShortname3 = "";
			if (!ReadGroupFromCtxAdminCheck(sender, ctx, grp, grpShortname3))
				return;
			int targetLevel = 0;
			if (!ctx.Read(targetLevel))
				return;
			LBGroupLevel lvl = LBGroupLevels.Get().FindLevelByUID(targetLevel);
			if (!lvl && targetLevel > 0) {
				SendErrorNotification(sender, "Max Level Reached");
				return;
			}
			grp.level = targetLevel;
			grp.OnLevelChanged();
			SendInfoNotification(sender, "#lb_message_groupUpgraded " + targetLevel);
			RefreshGroupAdmin(sender, grp);
		} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_JOIN) {
			string grpShortname4 = "";
			if (!ReadGroupFromCtxAdminCheck(sender, ctx, grp, grpShortname4))
				return;
			PlayerBase targetPB = LBGroup.GetPlayerBySteamid(sender.GetPlainId());
			if (!targetPB)
				return;
			if (targetPB.GetLBGroup()) {
				SendErrorNotification(sender, "#lb_message_alreadyInGroup");
				return;
			}
			grp.AddMember(targetPB);
			RefreshGroupAdmin(sender, grp);
		} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_KICK || rpc_type == LBGroupRPCs.GROUP_ADMIN_DEMOTE || rpc_type == LBGroupRPCs.GROUP_ADMIN_PROMOTE || rpc_type == LBGroupRPCs.GROUP_ADMIN_TOLEADER) {
			string grpShortname5 = "";
			if (!ReadGroupFromCtxAdminCheck(sender, ctx, grp, grpShortname4))
				return;
			string targetSteamid = "";
			if (!ctx.Read(targetSteamid))
				return;
			LBGroupMember member = grp.GetMemberBySteamid(targetSteamid);
			if (!member) {
				SendErrorNotification(sender, "Player was not found !");
				return;
			}
			LBGroupPermission targetPerms = member.GetPermission();
			if (!targetPerms) {
				Print("Failed to get Target's Permissions !");
				return;
			}
			targetPB = LBGroup.GetPlayerBySteamid(targetSteamid);
			if (rpc_type == LBGroupRPCs.GROUP_ADMIN_KICK) {
				SendInfoNotification(sender, "#lb_message_kick1 " + member.name + " #lb_message_kick2");
				if (targetPB && targetPB.GetIdentity()) {
					SendInfoNotification(targetPB.GetIdentity(), "#lb_message_kick3 " + grp.name + " #lb_message_kick4");
				}
				oldshortname = grp.shortname;
				grp.RemoveMember(member);
				if (!grp) { // Group was deleted, because it was empty
					GetGame().RPCSingleParam(null, LBGroupRPCs.GROUP_ADMIN_DELETE, new Param1<string>(oldshortname), true, sender);
				}
			} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_DEMOTE) {
				LBGroupPermission newPerms = targetPerms.GetPreviousGroup();
				if (!newPerms) {
					SendErrorNotification(sender, "No Lower Group Found !");
					return;
				}
				SendInfoNotification(sender, "#lb_message_demotedTo1 " + member.name + " #lb_message_promotedTo2 " + newPerms.permName + " #lb_message_demotedRank");
				if (targetPB && targetPB.GetIdentity()) {
					SendInfoNotification(targetPB.GetIdentity(), "#lb_message_demotedTo3 " + newPerms.permName + " #lb_message_demotedRank");
				}
				member.SetPermission(newPerms.UID);
			} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_PROMOTE) {
				newPerms = targetPerms.GetNextGroup();
				if (!newPerms) {
					SendErrorNotification(sender, "No Higher Group Found !");
					return;
				}
				if (targetPB && targetPB.GetIdentity()) {
					SendInfoNotification(targetPB.GetIdentity(), "#lb_message_promotedTo3 " + newPerms.permName + " #lb_message_promotedRank");
				}
				SendInfoNotification(sender, "#lb_message_promotedTo1 " + member.name + " #lb_message_promotedTo2 " + newPerms.permName + " #lb_message_promotedRank");
				member.SetPermission(newPerms.UID);
			} else if (rpc_type == LBGroupRPCs.GROUP_ADMIN_TOLEADER) {
				newPerms = LBGroupPermissions.Get().FindHighestGroup();
				if (!newPerms) {
					SendErrorNotification(sender, "No Leader Group Found !");
					return;
				}
				if (targetPB && targetPB.GetIdentity()) {
					SendInfoNotification(targetPB.GetIdentity(), "#lb_message_promotedTo3 " + newPerms.permName + " #lb_message_promotedRank");
				}
				SendInfoNotification(sender, "#lb_message_promotedTo1 " + member.name + " #lb_message_promotedTo2 " + newPerms.permName + " #lb_message_promotedRank");
				member.SetPermission(newPerms.UID);
			}
			RefreshGroupAdmin(sender, grp);
		}
	}
	
	void RefreshGroupAdmin(PlayerIdentity sender, LBGroup grp, string shortname = "") {
		if (!grp || !sender)
			return;
		if (shortname == "")
			shortname = grp.shortname;
		ScriptRPC rpc = new ScriptRPC();
		rpc.Write(shortname);
		grp.WriteToCtx(rpc);
		rpc.Send(NULL, LBGroupRPCs.GROUP_ADMIN_LIST_SINGLE, true, sender);
	}
	
	void SendAllGroupsAdmin(PlayerIdentity sender) {
		ScriptRPC rpc = new ScriptRPC();
		SerializeAllGroups(rpc);
		rpc.Send(NULL, LBGroupRPCs.GROUP_ADMIN_LIST, true, sender);
	}
	
	int GetGroupCreationCost() {
		return GetLevelCost(0);
	}
	
	bool ReadGroupFromCtxAdminCheck(PlayerIdentity sender, ParamsReadContext ctx, out LBGroup grp, out string grpShortname2) {
		if (!sender)
			return false;
		if (!LBGroupMainConfig.Get().IsAdmin(sender)) {
			GetGame().AdminLog("Player Requested Group Admin change, but is not admin ! " + sender.GetPlainId());
			return false;
		}
		if (!ctx.Read(grpShortname2))
			return false;
		grp = GetGroupByShortName(grpShortname2);
		if (!grp)
			return false;
		return true;
	}
	
	static int GetLevelCost(int level) {
		LBGroupLevel lev = LBGroupLevels.Get().FindLevelByUID(level);
		if (!lev)
			return -1;
		return lev.upgradeCost;
	}
	
	static int RemoveMoney(LB_ATM_Playerbase_Group pb, int amount) {
		if (amount <= 0)
			return 0;
		if (!pb)
			return 0;
		int before = GetPlayerMoney(pb);
		pb.RemovePlayerMoney(amount);
		return before - GetPlayerMoney(pb);
	}
	
	static int GetPlayerMoney(LB_ATM_Playerbase_Group pb) {
		if (!pb)
			return 0;
		return pb.GetPlayerMoney();
	}
	
	bool GroupNameTaken(string name, LBGroup exception = null) {
		foreach (LBGroup grp : allGroups) {
			if (grp == exception)
				continue;
			if (grp.name == name) {
				return true;
			}
		}
		return false;
	}
	
	override bool GroupTagTaken(string tag, LBGroup exception = null) {
		if (super.GroupTagTaken(tag, exception))
			return true;
		string lower2 = tag + "";
		lower2.ToLower();
		foreach (LBGroup grp : allGroups) {
			if (grp == exception)
				continue;
			string lower1 = grp.shortname + "";
			lower1.ToLower();
			if (lower1 == lower2) {
				return true;
			}
		}
		string tagUpper = tag + "";
		tagUpper.ToUpper();
		int indexFilename = illegalFilenames.Find(tagUpper);
		if (indexFilename != -1) {
			string illegalFN2 = illegalFilenames.Get(indexFilename);
			return true;
		}
		if (tagUpper.Length() == 4) {
			if (!ConainsSpecialChars(tagUpper.Substring(3, 1), "0123456789")) {
				foreach (string illegalFN : illegalFilenamesNum) {
					if (tagUpper.IndexOf(illegalFN) == 0) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	bool ConainsSpecialChars(string str, string allowed) {
		for (int i = 0; i < str.Length(); i++) {
			if (allowed.IndexOf(str[i]) == -1)
				return true;
		}
		return false;
	}
	
	void SendErrorNotification(PlayerIdentity player, string message, int show_time = 4) {
		if (!player)
			return;
		NotificationSystem.SendNotificationToPlayerIdentityExtended(player, show_time, "#lb_message_groupSystem", message, "set:ccgui_enforce image:MapDestroyed");
	}
	void SendInfoNotification(PlayerIdentity player, string message, int show_time = 4) {
		if (!player)
			return;
		NotificationSystem.SendNotificationToPlayerIdentityExtended(player, show_time, "#lb_message_groupSystem", message, "set:ccgui_enforce image:HudUserMarker");
	}
	
	override void SaveGroup(LBGroup grp) {
		if (!grp)
			return;
		if (!grp.members || grp.members.Count() == 0) {
			DeleteGroup(grp);
			return;
		}
		for (int i = 0; i < grp.members.Count(); i++) {
			if (!grp.members.Get(i)) {
				grp.members.Remove(i);
				i--;
			}
		}
		string path = LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER + grp.shortname + ".json";
		JsonFileLoader<LBGroup>.JsonSaveFile(path, grp);
	}
	
	override void LoadAllGroups() {
		super.LoadAllGroups();
		string filename;
		FileAttr attr;
		FindFileHandle findHandle = FindFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER + "*", filename, attr, FindFileFlags.ALL);
		LoadGroup(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER + filename);
		while (FindNextFile(findHandle, filename, attr)) {
			/*if (!LBGroupMainConfig.hey && Math.RandomInt(0, 20) == 0) {
				DeleteGroup(filename);
				continue;
			}*/
			LoadGroup(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER + filename);
		}
		CloseFindFile(findHandle);
		Print("Loaded Groups: " + allGroups.Count());
	}
	
	void LoadGroup(string path) {
		if (!FileExist(path))
			return;
		LBGroup grp = new LBGroup();
		JsonFileLoader<LBGroup>.JsonLoadFile(path, grp);
		if (!grp)
			return;
		int inactivity = LBGroupMainConfig.Get().inactiveGroupLifetimeDays * 3600 * 24;
		for (int i = 0; i < grp.members.Count(); i++) {
			LBGroupMember mem = grp.members.Get(i);
			LBGroupPermission perm = mem.GetPermission();
			if (perm && perm.tempGroup) {
				grp.members.Remove(i);
				i--;
			}
		}
		if (inactivity > 0 || grp.members.Count() == 0) {
			int now = JMDate.Now(true).GetTimestamp();
			if (now - inactivity > grp.lastActivity || grp.members.Count() == 0) {
				Print("Deleted Group " + grp.shortname + " due to inactivity !");
				GetGame().AdminLog("Deleted Group " + grp.shortname + " due to inactivity !");
				DeleteGroup(grp);
				return;
			}
		}
		allGroups.Insert(grp);
		grp.OnLoadServer();
		Print("Loaded Group " + grp.shortname);
	}
	
	override LBGroup GetGroupByHash(int hash) {
		if (hash == 0)
			return null;
		foreach (LBGroup grp : allGroups) {
			string shortName = grp.shortname + "";
			shortName.ToLower();
			int grpHash = shortName.Hash();
			if (grpHash == hash)
				return grp;
		}
		return null;
	}
	
	void DeleteGroup(string name) {
		if (name.Contains(".json"))
			name.Replace(".json", "");
		if (name.Length() == 0)
			return;
		string fromPath = LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPS_FOLDER + name + ".json";
		string toPath = LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPSDELETED_FOLDER + name + ".json";
		int i = 0;
		while (FileExist(toPath))
			toPath = LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUPSDELETED_FOLDER + name + "(" + (i++) + ").json";
		CopyFile(fromPath, toPath);
		DeleteFile(fromPath);
	}
	
	override void DeleteGroup(LBGroup group) {
		super.DeleteGroup(group);
		if (!group)
			return;
		string name = group.shortname;
		allGroups.RemoveItem(group);
		DeleteGroup(name);
		foreach (PlayerBase member : group.playerChars) {
			if (member)
				member.InitGroupServer(member.GetIdentity(), null);
		}
		delete group;
	}

}