modded class LBStaticMarkerManager {
	
	override void RPC_LB(PlayerIdentity sender, ParamsReadContext ctx, int type) {
		if (type == LBGroupRPCs.CONFIG_SYNC_STATIC_MARKERS) {
			ScriptRPC rpc = new ScriptRPC();
			rpc.Write(staticMarkers.Count() + tempStaticMarkers.Count());
			foreach (LBServerMarker marker : staticMarkers) {
				marker.WriteToCtx(rpc);
			}
			foreach (LBServerMarker marker2 : tempStaticMarkers) {
				marker2.WriteToCtx(rpc);
			}
			rpc.Send(null, LBGroupRPCs.CONFIG_SYNC_STATIC_MARKERS, true, sender);
		} else if (type == LBGroupRPCs.CONFIG_GLOBAL_MARKER_ADD) {
			if (!sender)
				return;
			string steamid = sender.GetPlainId();
			if (LBGroupMainConfig.Get().adminSteamids.Find(steamid) == -1) {
				Print("Player " + steamid + " tried to add Global Marker without Permission !");
				return;
			}
			LBMarker mark = new LBMarker();
			if (!mark.ReadFromCtx(ctx)) {
				Print("Failed to read Global Marker from " + steamid);
				return;
			}
			LBServerMarker serverMark = new LBServerMarker();
			serverMark.InitFromLBMarker(mark);
			if (serverMark.type == LBMarkerType.SERVER_DYNAMIC) {
				tempStaticMarkers.Insert(serverMark);
			} else {
				serverMark.type = LBMarkerType.SERVER_STATIC;
				staticMarkers.Insert(serverMark);
				SaveMarkers();
			}
			LBGroupManager.Get().SendInfoNotification(sender, "#lb_message_globalMarkerAdded");
			GetGame().AdminLog("Global Marker " + serverMark.name + " (" + serverMark.uid + ") was added by " + steamid + " " + sender.GetName());
			SendMarkerRefreshRPC();
		} else if (type == LBGroupRPCs.CONFIG_GLOBAL_MARKER_REMOVE) {
			if (!sender)
				return;
			steamid = sender.GetPlainId();
			if (LBGroupMainConfig.Get().adminSteamids.Find(steamid) == -1) {
				Print("Player " + steamid + " tried to remove Global Marker without Permission !");
				return;
			}
			int uid = 0;
			if (!ctx.Read(uid)) {
				Print("Failed to read UID for Global Marker Remove from " + steamid);
			}
			LBServerMarker marker3 = FindMarker(uid, staticMarkers);
			if (!marker3)
				marker3 = FindMarker(uid, tempStaticMarkers);
			else
				staticMarkers.RemoveItem(marker3);
			if (!marker3) {
				LBGroupManager.Get().SendInfoNotification(sender, "#lb_message_markerNotFound");
				return;
			} else
				tempStaticMarkers.RemoveItem(marker3);
			LBGroupManager.Get().SendInfoNotification(sender, "#lb_message_globalMarkerRemoved");
			GetGame().AdminLog("Global Marker " + marker3.name + " (" + marker3.uid + ") was removed by " + steamid + " " + sender.GetName());
			SendMarkerRefreshRPC();
			SaveMarkers();
		}
	}
	
	void SaveMarkers() {
		JsonFileLoader<array<ref LBServerMarker>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_STATIC_MARKER, staticMarkers);
	}
	
	LBServerMarker FindMarker(int uid, array<ref LBServerMarker> lst) {
		foreach (LBServerMarker marker : lst) {
			if (marker && marker.uid == uid)
				return marker;
		}
		return null;
	}
}