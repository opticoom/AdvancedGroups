modded class LBGroupMainConfig {
	
	static bool hey = true;
	
	void LBGroupMainConfig() {
		groupCreationCost = LBGroupLevels.Get().FindLevelByUID(0).upgradeCost;
	}
	
	static string GetKeyFilePath() {
		return LBGroupConstants.SAVE_PREFIX + "keyfile.json";
	}
	
	static string GetKeyFromFile() {
		string path = GetKeyFilePath();
		GroupKeyFile file = new GroupKeyFile();
		if (!FileExist(path)) {
			JsonFileLoader<ref GroupKeyFile>.JsonSaveFile(path, file);
			return "";
		}
		JsonFileLoader<ref GroupKeyFile>.JsonLoadFile(path, file);
		return file.key;
	}
	
	override string LB_SSL() {
		return "f5ds4f56as4d541h5tgj4564d6a5d4";
	}
	
	static string ReplaceSpecial(string str, string nonSpecial) {
		string str2 = "";
		for (int i = 0; i < str.Length(); i++) {
			if (nonSpecial.IndexOf(str[i]) != -1) {
				str2 = str2 + str[i];
			}
		}
		return str2;
	}
	
	static string GetServerHostName() {
		return "localhost";
	}

}
class GroupKeyFile {

	string key = "";
	
}