modded class LBGroup {

	[NonSerialized()]
	int lastCostInfoSent = 0;
	[NonSerialized()]
	static int currentGroup = 0;
	[NonSerialized()]
	const int GROUP_UPDATE_TIMER = 4000;
	[NonSerialized()]
	ref array<PlayerBase> playerChars = new array<PlayerBase>();
	[NonSerialized()]
	ref array<ref Param2<int, string>> lastInvites = new array<ref Param2<int, string>>();
	
	override void OnLoadServer() {
		super.OnLoadServer();
		foreach (LBGroupMember member : members) {
			member.online = false;
			member.parentGroup = this;
		}
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(StartUpdateDelayed, (((currentGroup++) * 733) % GROUP_UPDATE_TIMER), false);
		InitNumbers();
		if (GetFreeMemberSlots() < 0) {
			GetGame().AdminLog("Loaded Group has more players, than allowed: " + shortname + " (" + name + ") MemberCount: " + members.Count() + " Max: " + maxPlayers);
		}
	}
	
	void ~LBGroup() {
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(StartUpdateDelayed);
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(UpdateGroup);
		Print("Delete Group: " + shortname);
	}
	
	void StartUpdateDelayed() {
		Print("Starting Update Delayed " + shortname);
		GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(UpdateGroup, GROUP_UPDATE_TIMER, true);
	}
	
	void OnPlayerOnline(PlayerBase player, PlayerIdentity identity) {
		lastActivity = JMDate.Now(true).GetTimestamp();
		Print("On Player Online: " + (identity != null) + " " + (player != null));
		playerChars.Insert(player);
		if (identity)
			player.GetMyGroupMarker().hashedId = identity.GetId();
	}
	
	void OnPlayerOffline(PlayerBase player, string steamid) {
		if (player)
			playerChars.RemoveItem(player);
		lastActivity = JMDate.Now(true).GetTimestamp();
		bool identOk = (player && player.GetIdentity());
		Print("On Player Offline: " + identOk + " " + (player != null));
		RebuildCache();
	}
	
	void OnPlayerRespawn(PlayerBase player, PlayerIdentity ident) {
		if (player)
			playerChars.RemoveItem(player);
		Print("On Player Respawn: " + (ident != null) + " " + (player != null));
			
		RebuildCache();
	}
	
	bool CanInvite(string steamid) {
		Param2<int, string> inv = GetInvite(steamid);
		if (!inv)
			return true;
		int lastTime = inv.param1;
		int cooldown = LBGroupMainConfig.Get().inviteCooldownSeconds * 1000;
		if (cooldown <= 0)
			return true;
		int now = GetGame().GetTime();
		return now - lastTime > cooldown;
	}
	
	bool CanAcceptInvite(string steamid) {
		Param2<int, string> inv = GetInvite(steamid);
		return inv != null;
	}
	
	Param2<int, string> GetInvite(string steamid) {
		Print("Searching for invite in " + lastInvites.Count() + " invites");
		foreach (Param2<int, string> invite : lastInvites) {
			if (invite && invite.param2 == steamid) {
				Print("Found invite for " + steamid + " : " + invite.param1);
				return invite;
			}
		}
		return null;
	}
	
	void AcceptInvite(string steamid, PlayerBase pb) {
		Param2<int, string> inv = GetInvite(steamid);
		if (!inv)
			return;
		lastInvites.RemoveItem(inv);
		AddMember(pb);
	}
	
	void OnRPCServer(PlayerIdentity sender, int type, ParamsReadContext ctx) {
		if (type == LBGroupRPCs.ADD) {
			LBMarker marker = new LBMarker();
			if (!marker.ReadFromCtx(ctx)) {
				Print("Failed to read Marker to add !");
				return;
			}
			PlayerBase senderPB = PlayerBase.GetPlayerByIdentity(sender);
			if (!senderPB) {
				Print("Failed to get Sender's Player Object !");
				return;
			}
			LBGroupPermission perms = senderPB.GetPermission();
			if (!perms) {
				Print("Failed to get Sender's Permissions !");
				return;
			}
			if (!perms.CanSeeMarkerType(marker.type)) {
				SendErrorNotification(sender, "#lb_message_noPermissionsMarker");
				return;
			}
			if (markers.Count() >= markerLimit && marker.type != LBMarkerType.GROUP_PING) {
				SendErrorNotification(sender, "#lb_message_markerLimitReached");
				return;
			}
			AddMarkerServer(marker);
		} else if (type == LBGroupRPCs.REMOVE) {
			int uid;
			if (!ctx.Read(uid)) {
				Print("Failed to receive new Marker ID from Client !");
				return;
			}
			senderPB = PlayerBase.GetPlayerByIdentity(sender);
			if (!senderPB) {
				Print("Failed to get Sender's Player Object !");
				return;
			}
			perms = senderPB.GetPermission();
			if (!perms) {
				Print("Failed to get Sender's Permissions !");
				return;
			}
			LBMarker mark = FindAnyMarkerByUID(uid);
			if (!mark) {
				Print("Failed to find Any Marker with uid: " + uid);
				return;
			}
			if (!perms.CanSeeMarkerType(mark.type)) {
				SendErrorNotification(sender, "#lb_message_noPermissionsRemove");
				return;
			}
			RemoveMarkerServer(mark);
		} else if (type == LBGroupRPCs.INVITE) {
			string invtedSteamid;
			if (!ctx.Read(invtedSteamid)) {
				Print("Failed to Receive Invite Steamid !");
				return;
			}
			senderPB = PlayerBase.GetPlayerByIdentity(sender);
			if (!senderPB) {
				Print("Failed to get Sender's Player Object !");
				return;
			}
			perms = senderPB.GetPermission();
			if (!perms) {
				Print("Failed to get Sender's Permissions !");
				return;
			}
			if (!perms.canInvite) {
				SendErrorNotification(sender, "#lb_message_noPermissionsInvite");
				return;
			}
			PlayerBase targetPB = GetPlayerBySteamid(invtedSteamid);
			if (!targetPB) {
				SendErrorNotification(sender, "#lb_message_playerNotFound");
				return;
			}
			if (targetPB.GetLBGroup()) {
				SendErrorNotification(sender, "#lb_message_alreadyInGroup");
				return;
			}
			if (IsFull()) {
				SendErrorNotification(sender, "#lb_message_groupFull");
				return;
			}
			InvitePlayer(targetPB, sender.GetName());
			SendInfoNotification(sender, "#lb_message_invited");
		} else if (type == LBGroupRPCs.JOIN_SUBGROUP) {
			if (!LBGroupMainConfig.Get().enableSubGroups)
				return;
			int groupId = 0;
			if (!ctx.Read(groupId)) {
				Print("Failed to read requested Subgroup ID !");
				return;
			}
			if (groupId < 0 || groupId >= subGroupCount) {
				Print("Invalid Group ID: " + groupId + " GroupCount: " + subGroupCount);
				SendErrorNotification(sender, "#lb_message_internalError 003");
				return;
			}
			int current = GetSubgroupMemberCount(groupId);
			if (current >= subGroupSize) {
				SendErrorNotification(sender, "#lb_message_groupFull");
				return;
			}
			PlayerBase pb = PlayerBase.GetPlayerByIdentity(sender);
			if (!pb) {
				Print("Unable to find Player Object of " + sender.GetPlainId());
				return;
			}
			LBGroupMember memMarker = pb.GetMyGroupMarker();
			if (!memMarker) {
				Print("Unable to find Marker of " + sender.GetPlainId());
				return;
			}
			memMarker.SetSubGroup(groupId);
			SendInfoNotification(sender, "#lb_message_subgroupJoined " + LBGroupMainConfig.Get().subGroupNames.Get(groupId), 1.0);
		} else if (type == LBGroupRPCs.LEAVE) {
			pb = PlayerBase.GetPlayerByIdentity(sender);
			if (!pb) {
				Print("Unable to find Player Object of " + sender.GetPlainId());
				return;
			}
			memMarker = pb.GetMyGroupMarker();
			if (!memMarker) {
				Print("Unable to find Marker of " + sender.GetPlainId());
				return;
			}
			int permGroup = memMarker.permissionGroup;
			RemoveMarkerServer(memMarker);
			pb.SetLBGroup(null);
			FindNewLeaderIfNeeded(permGroup);
			SendInfoNotification(sender, "#lb_message_groupLeft " + name);
		} else if (type == LBGroupRPCs.PROMOTE || type == LBGroupRPCs.DEMOTE || type == LBGroupRPCs.KICK) {
			string steamid;
			if (!ctx.Read(steamid))
				return;
			senderPB = PlayerBase.GetPlayerByIdentity(sender);
			if (!senderPB) {
				Print("Failed to get Sender's Player Object !");
				return;
			}
			targetPB = GetPlayerBySteamid(steamid);
			perms = senderPB.GetPermission();
			if (!perms) {
				Print("Failed to get Sender's Permissions !");
				return;
			}
			LBGroupMember targetMember = GetMemberBySteamid(steamid);
			if (!targetMember) {
				SendErrorNotification(sender, "#lb_message_playerNotFound");
				return;
			}
			LBGroupPermission targetPerms = targetMember.GetPermission();
			if (!targetPerms) {
				Print("Failed to get Target's Permissions !");
				return;
			}
			if (type == LBGroupRPCs.PROMOTE) {
				if (!perms.CanPromote(targetPerms)) {
					SendErrorNotification(sender, "#lb_message_noPermissionsPromote");
					return;
				} else {
					string newGroupName = targetPerms.GetNextGroup().permName;
					SendInfoNotification(sender, "#lb_message_promotedTo1 " + targetMember.name + " #lb_message_promotedTo2 " + newGroupName + " #lb_message_promotedRank");
					if (targetPB && targetPB.GetIdentity()) {
						SendInfoNotification(targetPB.GetIdentity(), "#lb_message_promotedTo3 " + newGroupName + " #lb_message_promotedRank");
					}
					targetMember.SetPermission(targetPerms.GetNextGroup().UID);
					return;
				}
			}
			if (type == LBGroupRPCs.DEMOTE) {
				if (!perms.CanDemote(targetPerms)) {
					SendErrorNotification(sender, "#lb_message_noPermissionsDemote");
					return;
				} else {
					newGroupName = targetPerms.GetPreviousGroup().permName;
					SendInfoNotification(sender, "#lb_message_demotedTo1 " + targetMember.name + " #lb_message_promotedTo2 " + newGroupName + " #lb_message_demotedRank");
					if (targetPB && targetPB.GetIdentity()) {
						SendInfoNotification(targetPB.GetIdentity(), "#lb_message_demotedTo3 " + newGroupName + " #lb_message_demotedRank");
					}
					targetMember.SetPermission(targetPerms.GetPreviousGroup().UID);
					return;
				}
			}
			if (type == LBGroupRPCs.KICK) {
				if (!perms.CanKick(targetPerms)) {
					SendErrorNotification(sender, "#lb_message_noPermissionsKick");
					return;
				} else {
					SendInfoNotification(sender, "#lb_message_kick1 " + targetMember.name + " #lb_message_kick2");
					if (targetPB && targetPB.GetIdentity()) {
						SendInfoNotification(targetPB.GetIdentity(), "#lb_message_kick3 " + name + " #lb_message_kick4");
					}
					RemoveMarkerServer(targetMember);
				}
			}
		} else if (type == LBGroupRPCs.UPGRADE) {
			senderPB = PlayerBase.GetPlayerByIdentity(sender);
			if (!senderPB) {
				Print("Failed to get Sender's Player Object !");
				return;
			}
			perms = senderPB.GetPermission();
			if (!perms) {
				Print("Failed to get Sender's Permissions !");
				return;
			}
			if (!perms.canUpgrade) {
				SendErrorNotification(sender, "#lb_message_noPermissionsUpgrade");
				return;
			}
			int cost = LBGroupManager.GetLevelCost(level + 1);
			if (cost < 0) {
				SendErrorNotification(sender, "#lb_message_maxLevelReached");
				return;
			}
			if (GetGame().GetTime() - lastCostInfoSent > 15000) {
				SendInfoNotification(sender, "Upgrading The Group Costs " + cost + "$ Press Upgrade again to confim.");
				lastCostInfoSent = GetGame().GetTime();
				return;
			}
			lastCostInfoSent = 0;
			pb = PlayerBase.GetPlayerByIdentity(sender);
			if (!pb) {
				Print("Unable to find Player Object of " + sender.GetPlainId());
				return;
			}
			LB_ATM_Playerbase_Group moneyPB = new LB_ATM_Playerbase_Group(pb);
			int moneyOnPlayer = LBGroupManager.GetPlayerMoney(moneyPB);
			if (moneyOnPlayer < cost) {
				SendErrorNotification(sender, "Not enough money ! Group Upgrade costs " + cost + "$");
				return;
			}
			int removed = LBGroupManager.RemoveMoney(moneyPB, cost);
			GetGame().AdminLog("Player " + sender.GetPlainId() + " (" + sender.GetName() + ") upgraded Group. Name: " + name + " Tag: " + shortname + " Money Paied: " + removed + " Cost: " + cost);
			LBGroupManager.Get().SaveGroup(this);
			level++;
			OnLevelChanged();
			SendInfoNotification(sender, "#lb_message_groupUpgraded " + level);
		} else if (type == LBGroupRPCs.CHANGE_TAG_VISIBILITY) {
			bool enabled;
			if (!ctx.Read(enabled)) {
				Print("Unable to read enabled From Client !");
				return;
			}
			showTagInChat = enabled;
			SyncGroupTagVisiblity();
		} else if (type > LBGroupRPCs.START_MARKER_RPC) {
			int uid2;
			if (!ctx.Read(uid2))
				return;
			LBMarker marker2 = FindAnyMarkerByUID(uid2);
			if (marker2)
				marker2.OnMarkerRPCServer(type, ctx);
		}
	}
	
	void OnLevelChanged() {
		InitNumbers();
		ScriptRPC upgradeRPC = CreateRPCCall(LBGroupRPCs.UPGRADE);
		upgradeRPC.Write(level);
		upgradeRPC.Write(maxPlayers);
		upgradeRPC.Write(subGroupCount);
		upgradeRPC.Write(subGroupSize);
		upgradeRPC.Write(markerLimit);
		upgradeRPC.Write(plotpoleLimit);
		SendRPCToGroupMembers(upgradeRPC);
	}
	
	void FindNewLeaderIfNeeded(int leftPermissionGroup) {
		LBGroupPermission leaderGroup = LBGroupPermissions.Get().FindHighestGroup();
		if (leaderGroup.UID != leftPermissionGroup)
			return;
		if (!members || members.Count() == 0)
			return;
		LBGroupPermission lowerPerm = leaderGroup.GetPreviousGroup();
		bool found = false;
		LBGroupMember member = null;
		while (lowerPerm) {
			member = GetFirstMembersOfGroup(lowerPerm.UID);
			if (member) {
				found = true;
				break;
			}
			lowerPerm = lowerPerm.GetPreviousGroup();
		}
		if (!found)
			member = members.Get(0);
		member.SetPermission(leaderGroup.UID);
	}
	
	array<ref LBGroupMember> GetMembersOfGroup(int group) {
		array<ref LBGroupMember> arr = new array<ref LBGroupMember>();
		foreach (LBGroupMember mem : members) {
			if (mem.permissionGroup == group)
				arr.Insert(mem);
		}
		return arr;
	}
	
	LBGroupMember GetFirstMembersOfGroup(int group) {
		foreach (LBGroupMember mem : members) {
			if (mem.permissionGroup == group)
				return mem;
		}
		return null;
	}
	
	void InvitePlayer(PlayerBase player, string invitername = "") {
		PlayerIdentity ident = player.GetIdentity();
		GetGame().RPCSingleParam(player, LBGroupRPCs.GROUP_INVITE, new Param1<string>(shortname), true, ident);
		SendInfoNotification(ident, invitername + " #lb_message_invite1 " + name + " (" + shortname + ") #lb_message_invite2");
		lastInvites.Insert(new Param2<int, string>(GetGame().GetTime(), ident.GetPlainId()));
	}
	
	void InvitePlayer(PlayerIdentity ident, string invitername = "") {
		if (!ident)
			return;
		PlayerBase pb = GetPlayerBySteamid(ident.GetPlainId());
		if (!pb)
			return;
		InvitePlayer(pb, invitername);
	}
	
	void AddMarkerServer(LBMarker marker) {
		Print("Adding Marker Server: " + marker + " Group: " + shortname);
		if (marker) {
			string printname = marker.name + "";
			printname.Replace("%", "");
			Print("Marker Info: " + printname + " " + marker.icon + " " + marker.type);
		}
		AddMarkerLocal(marker);
		if (marker.type == LBMarkerType.GROUP_PING && LBGroupMainConfig.Get().tacticalPingLifetimeSeconds >= 0)
			GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(RemovePingMarkerServer, LBGroupMainConfig.Get().tacticalPingLifetimeSeconds * 1000, false, marker.uid);
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.ADD);
		marker.WriteToCtx(rpc);
		SendRPCToGroupMembers(rpc);
	}
	
	void AddPlayerMarkerServer(LBMarker marker) {
		Print("Adding Player Marker Server: " + marker);
		if (marker) {
			string printname = marker.name + "";
			printname.Replace("%", "");
			Print("Marker Info: " + printname + " " + marker.icon + " " + marker.type);
		}
		AddMarkerLocal(marker);
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.ADD_CLIENT);
		marker.WriteToCtx(rpc);
		SendRPCToGroupMembers(rpc);
	}
	
	void RemoveMarkerServer(int uid) {
		LBMarker marker = FindAnyMarkerByUID(uid);
		if (marker)
			RemoveMarkerServer(marker);
	}
	
	void RemovePingMarkerServer(int uid) {
		LBMarker marker = FindPingMarkerByUID(uid);
		if (marker)
			RemoveMarkerServer(marker);
	}
	
	void RemoveMarkerServer(LBMarker marker) {
		Print("Removing Marker Server: " + marker);
		if (marker) {
			string printname = marker.name + "";
			printname.Replace("%", "");
			Print("Marker Info: " + printname + " " + marker.icon + " " + marker.type);
		}
		int uid = marker.uid;
		RemoveMarkerLocal(marker);
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.REMOVE);
		rpc.Write(uid);
		SendRPCToGroupMembers(rpc);
	}
	
	void RebuildCache() {
		/*onlinePlayers.Clear();
		playerChars.Clear();
		ref array<Man> players = new array<Man>();
		GetGame().GetPlayers(players);
		foreach (Man player : players) {
			PlayerBase pb = PlayerBase.Cast(player);
			if (pb && pb.GetIdentity() && IsMember(pb.GetIdentity().GetPlainId())) {
				onlinePlayers.Insert(pb.GetIdentity());
				playerChars.Insert(pb);
			}
		}*/
	}
	
	void CreateNewGroup(PlayerBase owner, string nam, string tag) {
		OnLoadServer();
		lastActivity = JMDate.Now(true).GetTimestamp();
		creationDate = lastActivity;
		level = 0;
		InitNumbers();
		name = nam;
		shortname = tag;
		int leaderGroup = LBGroupPermissions.Get().FindHighestGroup().UID;
		AddMember(owner, leaderGroup);
	}
	
	void AddMember(PlayerBase pb) {
		int group = LBGroupPermissions.Get().FindLowestGroup().UID;
		AddMember(pb, group);
	}
	
	void AddMember(PlayerBase pb, int permissionGroup) {
		LBGroupMember member = LBGroupMember.CreateMember(pb);
		member.permissionGroup = permissionGroup;
		member.currentSubgroup = GetNextFreeSubgroup();
		member.online = true;
		AddPlayerMarkerServer(member);
		pb.InitGroupServer(pb.GetIdentity(), this);
	}
	
	override void RemoveMember(LBGroupMember member) {
		string steamid = member.steamid;
		//RemoveMarkerServer(member);
		members.RemoveItem(member);
		delete member;
		PlayerBase pb = GetPlayerBySteamid(steamid);
		if (pb) {
			OnPlayerOffline(pb, steamid);
			pb.lbgroup = null;
			pb.SendGroupInfo();
		}
		LBGroupManager.Get().SaveGroup(this);
	}
	
	static PlayerBase GetPlayerBySteamid(string steamid) {
		ref array<Man> players = new array<Man>();
		GetGame().GetPlayers(players);
		foreach (Man player : players) {
			PlayerBase pb = PlayerBase.Cast(player);
			if (!pb || !pb.GetIdentity())
				continue;
			if (pb.GetIdentity().GetPlainId() == steamid)
				return pb;
		}
		return null;
	}
	
	int GetNextFreeSubgroup() {
		if (!LBGroupMainConfig.Get().enableSubGroups)
			return 0;
		LBGroupLevel lev = LBGroupLevels.Get().FindLevelByUID(level);
		if (!lev)
			return 0;
		int max = lev.subgroupSize;
		if (max <= 0)
			return 0;
		int i = 0;
		while (GetSubgroupMemberCount(i) >= max && i <= lev.subgroupCount)
			i++;
		return i;
	}
	
	override void InitNumbers() {
		super.InitNumbers();
		LBGroupLevel lvl = LBGroupLevels.Get().FindLevelByUID(level);
		if (!lvl)
			lvl = LBGroupLevels.Get().GetHighestLevel();
		maxPlayers = lvl.maxPlayerCount;
		subGroupSize = lvl.subgroupSize;
		subGroupCount = lvl.subgroupCount;
		markerLimit = LBGroupMainConfig.Get().groupMarkerLimit + lvl.groupMarkerLimitAdded;
		plotpoleLimit = lvl.groupPlotpolesLimitAdded;
	}
	
	int GetFreeMemberSlots() {
		return maxPlayers - members.Count();
	}
	
	bool IsFull() {
		return GetFreeMemberSlots() <= 0;
	}
	
	void UpdatePlayerList() {
		for (int i = 0; i < playerChars.Count(); i++) {
			if (!playerChars.Get(i) || !playerChars.Get(i).IsAlive() || !playerChars.Get(i).GetIdentity())
				playerChars.Remove(i--);
		}
	}
	
	void UpdateGroup() {
		if (playerChars && playerChars.Count() > 0) { // Reduce Log Spam. There is nothing to update if no player is online
			Print("Update Group: " + shortname);
			UpdatePlayerList();
			UpdatePlayerPositionsAndHealth();
			Print("Finished Update Group: " + shortname);
		}
	}
	
	void UpdatePlayerPositionsAndHealth() {
		foreach (PlayerBase player : playerChars) {
			LBGroupMember member = GetMemberBySteamid(player.GetIdentity().GetPlainId());
			if (member) {
				member.SetPosition(player.GetPosition());
				member.SetHealth(player.GetHealth());
			}
		}
	}
	
	void SendErrorNotification(PlayerIdentity player, string message, float show_time = 4) {
		if (!player)
			return;
		string printmessage = message + "";
		printmessage.Replace("%", "");
		Print("Sending Error Notification to " + player.GetPlainId() + ": " + printmessage);
		NotificationSystem.SendNotificationToPlayerIdentityExtended(player, show_time, "#lb_message_groupSystem", message, "set:ccgui_enforce image:MapDestroyed");
	}
	
	void SendInfoNotification(PlayerIdentity player, string message, float show_time = 4) {
		if (!player)
			return;
		string printmessage = message + "";
		printmessage.Replace("%", "");
		Print("Sending Info Notification to " + player.GetPlainId() + ": " + printmessage);
		NotificationSystem.SendNotificationToPlayerIdentityExtended(player, show_time, "#lb_message_groupSystem", message, "set:ccgui_enforce image:HudUserMarker");
	}
	
	void SyncGroupTagVisiblity() {
		ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.CHANGE_TAG_VISIBILITY);
		rpc.Write(showTagInChat);
		SendRPCToGroupMembers(rpc);
	}
	
	override void SendRPCToGroupMembers(ScriptRPC rpc, int otherRPCType = -1) {
		if (!playerChars)
			playerChars = new array<PlayerBase>();
		Print("Sending RPC To Group Members: " + playerChars.Count());
		int type = otherRPCType;
		if (type == -1)
			type = LBGroupRPCs.GROUP_RPC;
		int count = 0;
		foreach (PlayerBase pb : playerChars) {
			if (!pb)
				continue;
			PlayerIdentity ident = pb.GetIdentity();
			if (ident) {
				rpc.Send(null, type, true, ident);
				count++;
				string printname = ident.GetName() + "";
				printname.Replace("%", "");
				Print("RPC Tpye: " + type + " Sent to " + printname + " " + ident.GetPlainId());
			}
		}
		Print("RPC was sent to " + count + " Players");
	}
	
	void ResendGroupInfo() {
		foreach (PlayerBase player : playerChars) {
			if (!player)
				continue;
			player.SendGroupInfo();
		}
	}
}