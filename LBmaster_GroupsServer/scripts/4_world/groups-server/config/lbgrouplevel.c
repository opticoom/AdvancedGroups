class LBGroupLevel {

	int level;
	int maxPlayerCount;
	int upgradeCost;
	int subgroupCount;
	int subgroupSize;
	int groupMarkerLimitAdded;
	int groupPlotpolesLimitAdded = 0;
	
	static LBGroupLevel InitLevel(int lvl, int maxPlayers, int cost, int subCount, int subSize, int markerAdded, int plotpoleAdded) {
		LBGroupLevel lev = new LBGroupLevel();
		lev.level = lvl;
		lev.maxPlayerCount = maxPlayers;
		lev.upgradeCost = cost;
		lev.subgroupCount = subCount;
		lev.subgroupSize = subSize;
		lev.groupMarkerLimitAdded = markerAdded;
		lev.groupPlotpolesLimitAdded = plotpoleAdded;
		return lev;
	}
	
	bool HasNextLevel() {
		return GetNextLevel() != null;
	}
	
	LBGroupLevel GetNextLevel() {
		return LBGroupLevels.Get().GetNextLevel(this);
	}
	
	/*static LBGroupLevel ReadFromCtx(ParamsReadContext ctx) {
		LBGroupLevel lvl = new LBGroupLevel;
		if (!ctx.Read(lvl.level))
			return null;
		if (!ctx.Read(lvl.maxPlayerCount))
			return null;
		if (!ctx.Read(lvl.upgradeCost))
			return null;
		return lvl;
	}
	
	void WriteToCtx(ParamsWriteContext ctx) {
		ctx.Write(level);
		ctx.Write(maxPlayerCount);
		ctx.Write(upgradeCost);
	}//*/
	
}
class LBGroupLevels {

	ref array<ref LBGroupLevel> allLevels = new array<ref LBGroupLevel>();
	
	static ref LBGroupLevels g_LBGroupGroups;
	
	static LBGroupLevels Get() {
		if (!g_LBGroupGroups) {
			//if (GetGame().IsServer() || !GetGame().IsMultiplayer()) {
				g_LBGroupGroups = Load();
			/*} else if (GetGame().IsClient()) {
				g_LBGroupGroups = new LBGroupLevels();
				GetGame().RPCSingleParam(null, LBGroupRPCs.CONFIG_SYNC_LEVELS, new Param1<bool>(true), true);
			}*/
		}
		return g_LBGroupGroups;
	}

	static void Delete() {
		if (g_LBGroupGroups)
			delete g_LBGroupGroups;
	}
	
	static LBGroupLevels Load() {
		LBGroupLevels levels;
		if (!FileExist(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUP_LEVELS)) {
			levels = LoadDefault();
			JsonFileLoader<array<ref LBGroupLevel>>.JsonSaveFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUP_LEVELS, levels.allLevels);
			return levels;
		}
		levels = new LBGroupLevels();
		JsonFileLoader<array<ref LBGroupLevel>>.JsonLoadFile(LBGroupConstants.SAVE_PREFIX + LBGroupConstants.SAVE_SUFFIX_GROUP_LEVELS, levels.allLevels);
		return levels;
	}
	
	static LBGroupLevels LoadDefault() {
		LBGroupLevels def = new LBGroupLevels;
		def.allLevels.Insert(LBGroupLevel.InitLevel(0, 3, 0, 2, 3, 0, 0));
		def.allLevels.Insert(LBGroupLevel.InitLevel(1, 5, 8000, 3, 3, 5, 0)); // +2
		def.allLevels.Insert(LBGroupLevel.InitLevel(2, 8, 20000, 4, 4, 10, 0)); // +3
		def.allLevels.Insert(LBGroupLevel.InitLevel(3, 12, 40000, 5, 4, 15, 1)); // +4
		def.allLevels.Insert(LBGroupLevel.InitLevel(4, 16, 60000, 5, 5, 20, 1)); // +4
		def.allLevels.Insert(LBGroupLevel.InitLevel(5, 20, 100000, 6, 5, 30, 2)); // +4
		return def;
	}
	
	/*void RPC_LB(PlayerIdentity sender, ParamsReadContext ctx) {
		if (GetGame().IsServer()) {
			ScriptRPC rpc = new ScriptRPC();
			rpc.Write(allLevels.Count());
			foreach (LBGroupLevel lvl : allLevels) {
				lvl.WriteToCtx(rpc);
			}
			rpc.Send(null, LBGroupRPCs.CONFIG_SYNC_LEVELS, true);
		} else {
			int count = 0;
			if (!ctx.Red(count)) {
				Print("Failed to receive Group Level Config !");
				return;
			}
		}
	}*/
	
	LBGroupLevel FindLevelByUID(int level) {
		if (level == -1)
			return null;
		foreach (LBGroupLevel level2 : allLevels) {
			if (level2.level == level)
				return level2;
		}
		return null;
	}

	LBGroupLevel GetHighestLevel() {
		LBGroupLevel highest = null;
		foreach (LBGroupLevel level2 : allLevels) {
			if (!highest || level2.level > highest.level)
				highest = level2;
		}
		return highest;
	}
	
	LBGroupLevel GetNextLevel(LBGroupLevel level) {
		if (!level)
			return null;
		return FindLevelByUID(level.level + 1);
	}
		
	LBGroupLevel GetNextLevel(int level) {
		return GetNextLevel(FindLevelByUID(level));
	}
	
}