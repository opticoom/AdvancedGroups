modded class LBGroupMember {
	
	override void SetHealth(float health) {
		if (GetGame().IsServer()) {
			if (Math.AbsFloat(health - this.health) > 1 || (health == 0 && this.health != 0) || (health == 100 && this.health != 100)) {
				ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.HEALTH);
				rpc.Write(health);
				SendMarkerRPC(rpc);
				super.SetHealth(health);
			}
		} else {
			super.SetHealth(health);
		}
	}
	
	override void SetPermission(int perm) {
		if (GetGame().IsServer()) {
			if (perm != permissionGroup) {
				ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.PERMISSION);
				rpc.Write(perm);
				SendMarkerRPC(rpc);
				super.SetPermission(perm);
			}
		} else {
			super.SetPermission(perm);
		}
	}
	
	override void SetOnline(bool on, string hashedId) {
		if (GetGame().IsServer()) {
			if (on != online) {
				ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.ONLINE);
				rpc.Write(on);
				rpc.Write(hashedId);
				SendMarkerRPC(rpc);
			}
		}
		super.SetOnline(on, hashedId);
	}

}