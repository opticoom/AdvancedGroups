modded class LBMarker {

	override void SetPosition(vector pos) {
		if (GetGame().IsServer()) {
			if (vector.Distance(pos, position) > 1) {
				ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.POSITION);
				rpc.Write(pos[0]);
				rpc.Write(pos[1]);
				rpc.Write(pos[2]);
				SendMarkerRPC(rpc);
				super.SetPosition(pos);
			}
		} else {
			super.SetPosition(pos);
		}
	}
	override void SetName(string name) {
		if (GetGame().IsServer()) {
			if (this.name != name) {
				ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.NAME);
				rpc.Write(name);
				SendMarkerRPC(rpc);
				super.SetName(name);
			}
		} else {
			super.SetName(name);
		}
	}
	
	override void SetSubGroup(int grp) {
		if (GetGame().IsServer()) {
			if (grp != currentSubgroup) {
				ScriptRPC rpc = CreateRPCCall(LBGroupRPCs.SUBGRUOP);
				rpc.Write(grp);
				SendMarkerRPC(rpc);
				super.SetSubGroup(grp);
			}
		} else {
			super.SetSubGroup(grp);
		}
	}
	
	void OnMarkerRPCServer(int type, ParamsReadContext ctx) {
		if (type == LBGroupRPCs.POSITION) {
			float x,y,z;
			if (!ctx.Read(x) || !ctx.Read(y) || !ctx.Read(z))
				return;
			vector vec = Vector(x,y,z);
			SetPosition(vec);
		}
	}
}