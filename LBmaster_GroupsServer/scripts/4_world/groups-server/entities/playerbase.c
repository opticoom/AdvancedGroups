modded class PlayerBase {
	
	string steamid;
	
	void InitGroupServer(PlayerIdentity identity, LBGroup lgroup = null) {
		if (identity)
			steamid = identity.GetPlainId();
		if (!lgroup)
			lbgroup = LBGroupManager.Get().GetPlayersGroup(steamid);
		else
			lbgroup = lgroup;
		if (lbgroup) {
			lbgroup.OnPlayerOnline(this, identity);
		}
		LBGroupMember member = GetMyGroupMarker();
		if (member) {
			member.SetHealth(GetHealth());
			member.SetName(GetIdentity().GetName());
			member.SetOnline(true, GetIdentity().GetId());
		}
		SendGroupInfo();
	}
	
	override void SetLBGroup(LBGroup grp) {
		super.SetLBGroup(grp);
		InitGroupServer(GetIdentity(), grp);
	}
	
	void InitGroupServerRespawn(PlayerIdentity identity) {
		steamid = identity.GetPlainId();
		lbgroup = LBGroupManager.Get().GetPlayersGroup(steamid);
		if (lbgroup) {
			lbgroup.OnPlayerRespawn(this, identity);
			LBGroupMember member = GetMyGroupMarker();
			if (member) {
				member.SetHealth(GetHealth());
				member.SetOnline(true, identity.GetId());
			}
		}
	}
	
	override void OnDisconnect() {
		super.OnDisconnect();
		if (lbgroup) {
			lbgroup.OnPlayerOffline(this, steamid);
			LBGroupMember member = GetMyGroupMarker();
			if (member) {
				member.SetOnline(false, "");
			}
		}
	}
	
	void SendGroupInfo() {
		PlayerIdentity ident = GetIdentity();
		if (!ident)
			return;
		string id = ident.GetId();
		int hash = id.Hash();
		identityIdHash = hash;
		SetSynchDirty();
		ScriptRPC rpc = new ScriptRPC();
		bool present = lbgroup != null;
		rpc.Write(present);
		if (lbgroup) {
			lbgroup.WriteToCtx(rpc);
		}
		rpc.Send(this, LBGroupRPCs.GROUP_SYNC, true, ident);
		memberCache = null;
	}
	
	override void OnRPC(PlayerIdentity sender, int rpc_type, ParamsReadContext ctx) {
		super.OnRPC(sender, rpc_type, ctx);
		if (rpc_type == LBGroupRPCs.GROUP_RPC) {
			Print("Received Group RPC");
			int type = 0;
			if (!ctx.Read(type))
				return;
			LBGroup grp = GetLBGroup();
			Print("Received Group RPC Type: " + type + " Group: " + (grp != null));
			if (grp)
				grp.OnRPCServer(sender, type, ctx);
			Print("Hello4");
		}
	}

	override void EEKilled(Object killer) {
		super.EEKilled(killer);
		LBGroupMember member = GetMyGroupMarker();
		if (member)
			member.SetHealth(0.0);
	}
	
	override void EEHitBy(TotalDamageResult damageResult, int damageType, EntityAI source, int component, string dmgZone, string ammo, vector modelPos, float speedCoef) {
		super.EEHitBy(damageResult, damageType, source, component, dmgZone, ammo, modelPos, speedCoef);
		LBGroupMember member = GetMyGroupMarker();
		if (member)
			member.SetHealth(GetHealth());
	}
}