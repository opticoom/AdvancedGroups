class CfgPatches
{
	class LBmaster_GroupsServer
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]={"DZ_Data", "LBmaster_Groups"};
	};
};
class CfgMods
{
	class LBmaster_GroupsServer
	{
		dir="LBmaster_GroupsServer";
		picture="";
		action="";
		hideName=0;
		hidePicture=1;
		name="Groups";
		author="LBmaster";
		credits="LBmaster";
		authorID="0";
		version="1.0";
		type="mod";
		dependencies[]=
		{
			"Game",
			"World",
			"Mission"
		};
		class defs
		{
			class gameScriptModule
			{
				value="";
				files[]=
				{
					"LBmaster_GroupsServer/scripts/3_Game"
				};
			};
			class worldScriptModule
			{
				value="";
				files[]=
				{
					"LBmaster_GroupsServer/scripts/4_World"
				};
			};
			class missionScriptModule
			{
				value="";
				files[]=
				{
					"LBmaster_GroupsServer/scripts/5_Mission"
				};
			};
		};
	};
};
